
draw_set_font(fntM)
draw_set_halign(fa_center)
draw_set_valign(fa_top)

///POPUP TEXT
with PopupText
{
if visible = 1
{
draw_set_color(c_black)
draw_text(median(view_xview+view_wview-10-string_width(mytext)/2,view_xview+10+string_width(mytext)/2,x),median(view_yview+view_hview-30+1,view_yview+20+1,y)+1,string(mytext))
draw_text(median(view_xview+view_wview-10-string_width(mytext)/2,view_xview+10+string_width(mytext)/2,x)+1,median(view_yview+view_hview-30+1,view_yview+20+1,y)+1,string(mytext))
draw_text(median(view_xview+view_wview-10-string_width(mytext)/2,view_xview+10+string_width(mytext)/2,x)+1,median(view_yview+view_hview-30+1,view_yview+20+1,y),string(mytext))
draw_set_color(c_white)
draw_text(median(view_xview+view_wview-10-string_width(mytext)/2,view_xview+10+string_width(mytext)/2,x),median(view_yview+view_hview-30,view_yview+20,y),string(mytext))
}
}

with LevelUp
draw_sprite(sprite_index,-1,x,y)

if instance_exists(Player)
{
//DRAW THE HUD HERE


//HEALTH BAR
draw_sprite(sprHealthBar,2,view_xview+20,view_yview+4)

if Player.maxhealth != 0 draw_sprite_ext(sprHealthFill,2,view_xview+22,view_yview+7,max(0,84*(Player.lsthealth/Player.maxhealth)),1,0,c_white,1)
if Player.maxhealth != 0 draw_sprite_ext(sprHealthFill,1,view_xview+22,view_yview+7,max(0,84*(Player.my_health/Player.maxhealth)),1,0,c_white,1)
if ((Player.sprite_index = Player.spr_hurt and Player.image_index < 1 and !instance_exists(Portal)) or Player.lsthealth < Player.my_health) and !instance_exists(GenCont) and !instance_exists(LevCont)
if Player.maxhealth != 0 draw_sprite_ext(sprHealthFill,0,view_xview+22,view_yview+7,max(0,84*(Player.my_health/Player.maxhealth)),1,0,c_white,1)

draw_set_font(fntM)

//CROWNS

draw_set_halign(fa_right)

if Player.crown > 1 and !instance_exists(LevCont)
{
draw_set_color(c_black)
draw_text(view_xview+view_wview-2+1,view_yview+view_hview-string_height("C")-2,string(Player.crown_name[Player.crown])/*+"#"+string(Player.crown_text[Player.crown])*/)
draw_text(view_xview+view_wview-2+1,view_yview+view_hview-string_height("C")-2+1,string(Player.crown_name[Player.crown])/*+"#"+string(Player.crown_text[Player.crown])*/)
draw_text(view_xview+view_wview-2,view_yview+view_hview-string_height("C")-2+1,string(Player.crown_name[Player.crown])/*+"#"+string(Player.crown_text[Player.crown])*/)
draw_set_color(c_white)
draw_text(view_xview+view_wview-2,view_yview+view_hview-string_height("C")-2,string(Player.crown_name[Player.crown])/*+"#"+string(Player.crown_text[Player.crown])*/)
draw_set_valign(fa_top)
}

//HEALTH TEXT
draw_set_halign(fa_center)
if (!((Player.sprite_index = Player.spr_hurt and Player.image_index < 1 and !instance_exists(Portal)) or Player.lsthealth < Player.my_health) or sin(wave) > 0) or instance_exists(GenCont) or instance_exists(LevCont)
{
draw_set_color(c_black)
draw_text(view_xview+23+44,view_yview+8,string(Player.my_health)+"/"+string(Player.maxhealth))
draw_text(view_xview+23+45,view_yview+8,string(Player.my_health)+"/"+string(Player.maxhealth))
draw_text(view_xview+23+45,view_yview+7,string(Player.my_health)+"/"+string(Player.maxhealth))
draw_set_color(c_white)
draw_text(view_xview+23+44,view_yview+7,string(Player.my_health)+"/"+string(Player.maxhealth))
}

//HARD MODE ICON
//HARD MODE ICON
//HARD MODE ICON
if global.hardmode = 1 then draw_sprite(sprHardModeHUD,-1,view_xview+103,view_yview-2,)


//SECONDARY WEAPON
if Player.bwep != 0
{
var spr, col, wid;
spr = Player.wep_sprt[Player.bwep]
wid = 16
if Player.wep_type[Player.bwep] = 0
wid = 32
col = c_dkgray
if Player.race = 7
col = c_white
if round(Player.area/2) = Player.area/2 or col = c_white or instance_exists(GenCont) or instance_exists(LevCont)
{
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+67,round(view_yview)+16,col,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+69,round(view_yview)+16,col,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+68,round(view_yview)+15,col,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+68,round(view_yview)+17,col,1)
}

draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+68,round(view_yview)+16,c_black,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),max(0,round(wid)*min(Player.wep_load[Player.bwep],Player.breload/Player.wep_load[Player.bwep])),14,round(view_xview)+68,round(view_yview)+16,c_white,0.2)

if Player.wep_type[Player.bwep] != 0
{
draw_set_halign(fa_left)
draw_set_color(c_black)
draw_text(view_xview+86,view_yview+22,string(Player.ammo[Player.wep_type[Player.bwep]]))
draw_text(view_xview+87,view_yview+22,string(Player.ammo[Player.wep_type[Player.bwep]]))
draw_text(view_xview+87,view_yview+21,string(Player.ammo[Player.wep_type[Player.bwep]]))
if Player.race = 7 or Player.wep_type[Player.wep] = Player.wep_type[Player.bwep]
draw_set_color(c_white)
else
draw_set_color(c_silver)
if Player.ammo[Player.wep_type[Player.bwep]] <= Player.typ_ammo[Player.wep_type[Player.bwep]]
{
if Player.race = 7 or Player.wep_type[Player.wep] = Player.wep_type[Player.bwep]
draw_set_color(c_red)
else
draw_set_color(c_gray)
}
if Player.ammo[Player.wep_type[Player.bwep]] = 0
draw_set_color(c_dkgray)
draw_text(view_xview+86,view_yview+21,string(Player.ammo[Player.wep_type[Player.bwep]]))
}
}


//PRIMARY WEAPON
var spr, col, wid, act;
spr = Player.wep_sprt[Player.wep]
wid = 16
col = c_white
act = 0

if Player.wep_type[Player.wep] = 0
wid = 32

col = c_white

if string_copy(Player.wep_name[Player.wep],0,5) = "ULTRA"
col = c_lime;

if string_copy(Player.wep_name[Player.wep],0,4) = "GOLD"
col = make_color_rgb(223,201,134);

if Player.curse = 1
col = make_color_rgb(136,36,174);

draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+25,round(view_yview)+16,col,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+23,round(view_yview)+16,col,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+24,round(view_yview)+17,col,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+24,round(view_yview)+15,col,1)

draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),round(wid),14,round(view_xview)+24,round(view_yview)+16,c_black,1)
draw_sprite_part_smart(spr,1,round(sprite_get_xoffset(spr)),round(sprite_get_yoffset(spr)-8),max(0,round(wid)*min(Player.wep_load[Player.wep],Player.reload/Player.wep_load[Player.wep])),14,round(view_xview)+24,round(view_yview)+16,c_white,0.2)

if Player.wep_type[Player.wep] != 0
{
draw_set_halign(fa_left)
draw_set_color(c_black)
draw_text(view_xview+42,view_yview+22,string(Player.ammo[Player.wep_type[Player.wep]]))
draw_text(view_xview+43,view_yview+22,string(Player.ammo[Player.wep_type[Player.wep]]))
draw_text(view_xview+43,view_yview+21,string(Player.ammo[Player.wep_type[Player.wep]]))

draw_set_color(c_white)

if Player.ammo[Player.wep_type[Player.wep]] <= Player.typ_ammo[Player.wep_type[Player.wep]]
draw_set_color(c_red)
if Player.ammo[Player.wep_type[Player.wep]] = 0
draw_set_color(c_dkgray)
draw_text(view_xview+42,view_yview+21,string(Player.ammo[Player.wep_type[Player.wep]]))
}

//EXPERIENCE BAR
draw_set_halign(fa_center)
if Player.skillpoints > 0
draw_sprite(sprExpBarLevel,0,view_xview+4,view_yview+4)
draw_sprite(sprExpBar,(Player.rad/((Player.level*60)*max(1,(Player.ultra_got[59]*2))))*16,view_xview+4,view_yview+4)
if Player.level < 10
{
    draw_set_color(c_black)
    draw_text(view_xview+11,view_yview+17-string_height("A")/2,string(Player.level))
    draw_text(view_xview+12,view_yview+17-string_height("A")/2,string(Player.level))
    draw_text(view_xview+12,view_yview+16-string_height("A")/2,string(Player.level))
    draw_set_color(c_white)
    draw_text(view_xview+11,view_yview+16-string_height("A")/2,string(Player.level))
}
else
{
    draw_sprite(sprUltraLevel,0,view_xview+11,view_yview+17);
}

//AMMO ICONS
img = 0
if Player.wep_type[Player.bwep] = 1 img = 1
if Player.wep_type[Player.wep] = 1 or (Player.race = 7 and Player.wep_type[Player.bwep] = 1) img = 2
draw_sprite(sprBulletIconBG,img,view_xview+2,view_yview+32)
draw_sprite(sprBulletIcon,7-ceil((Player.ammo[1]/Player.typ_amax[1])*7),view_xview+2,view_yview+32)

img = 0
if Player.wep_type[Player.bwep] = 2 img = 1
if Player.wep_type[Player.wep] = 2 or (Player.race = 7 and Player.wep_type[Player.bwep] = 2) img = 2
draw_sprite(sprShotIconBG,img,view_xview+12,view_yview+32)
draw_sprite(sprShotIcon,7-ceil((Player.ammo[2]/Player.typ_amax[2])*7),view_xview+12,view_yview+32)

img = 0
if Player.wep_type[Player.bwep] = 3 img = 1
if Player.wep_type[Player.wep] = 3 or (Player.race = 7 and Player.wep_type[Player.bwep] = 3) img = 2
draw_sprite(sprBoltIconBG,img,view_xview+22,view_yview+32)
draw_sprite(sprBoltIcon,7-ceil((Player.ammo[3]/Player.typ_amax[3])*7),view_xview+22,view_yview+32)

img = 0
if Player.wep_type[Player.bwep] = 4 img = 1
if Player.wep_type[Player.wep] = 4 or (Player.race = 7 and Player.wep_type[Player.bwep] = 4) img = 2
draw_sprite(sprExploIconBG,img,view_xview+32,view_yview+32)
draw_sprite(sprExploIcon,7-ceil((Player.ammo[4]/Player.typ_amax[4])*7),view_xview+32,view_yview+32)

img = 0
if Player.wep_type[Player.bwep] = 5 img = 1
if Player.wep_type[Player.wep] = 5 or (Player.race = 7 and Player.wep_type[Player.bwep] = 5) img = 2
draw_sprite(sprEnergyIconBG,img,view_xview+42,view_yview+32)
draw_sprite(sprEnergyIcon,7-ceil((Player.ammo[5]/Player.typ_amax[5])*7),view_xview+42,view_yview+32)


//LOW AMMO WARNING

if Player.ammo[Player.wep_type[Player.wep]] <= Player.typ_ammo[Player.wep_type[Player.wep]] and sin(wave) > 0 and Player.drawempty > 0
{
if Player.drawempty = 10 and Player.ammo[Player.wep_type[Player.wep]] > Player.typ_ammo[Player.wep_type[Player.wep]]-Player.wep_cost[Player.wep]
snd_play(Player.snd_lowa)
Player.drawempty -= 1

txt = "LOW "+string(Player.typ_name[Player.wep_type[Player.wep]])
if Player.ammo[Player.wep_type[Player.wep]] < Player.wep_cost[Player.wep]
{
if Player.ammo[Player.wep_type[Player.wep]] > 0
txt = "NOT ENOUGH "+string(Player.typ_name[Player.wep_type[Player.wep]])
else
txt = "EMPTY"
}
draw_set_halign(fa_left)
draw_set_color(c_black)
draw_text(view_xview+55,view_yview+34,string(txt))
draw_text(view_xview+55,view_yview+35,string(txt))
draw_text(view_xview+54,view_yview+35,string(txt))
draw_set_color(c_red)
draw_text(view_xview+54,view_yview+34,string(txt))
}


//LOW HP

if Player.my_health <= 4 and Player.my_health != Player.maxhealth and sin(wave) > 0 and Player.drawlowhp > 0
{
Player.drawlowhp -= 1
txt = "LOW HP"
draw_set_halign(fa_left)
draw_set_color(c_black)
draw_text(view_xview+111,view_yview+7,string(txt))
draw_text(view_xview+111,view_yview+8,string(txt))
draw_text(view_xview+110,view_yview+8,string(txt))
draw_set_color(c_red)
draw_text(view_xview+110,view_yview+7,string(txt))
}

//SKILL ICONS
dir = 1
dis = 1
dix = 0
diy = 0
uxoff = 0

repeat(Player.maxultra)
{
    if Player.ultra_got[dis] = 1
    {
        draw_sprite_ext(sprUltraIconHUD,dis,view_xview+view_wview-12,view_yview+13,1,1,0,c_black,1)
        draw_sprite_ext(sprUltraIconHUD,dis,view_xview+view_wview-12,view_yview+12,1,1,0,c_white,1)
        diy += 1
        uxoff = -18
    }
    dis += 1
}

/*repeat(Player.maxskill)
{
    if uxoff != 0
    {
        if Player.skill_got[dir] = 1
        {
            draw_sprite_ext(sprSkillIconHUD,dir,uxoff+view_xview+view_wview-12-16*dix,view_yview+13,1,1,0,c_black,1)
            draw_sprite_ext(sprSkillIconHUD,dir,uxoff+view_xview+view_wview-12-16*dix,view_yview+12,1,1,0,c_white,1)
            dix += 1
        }
    }
    
    dir += 1
}*/

for (var i=0;i<ds_list_size(Player.mutationslist);i++) {
    dir=ds_list_find_value(Player.mutationslist,i)
    draw_sprite_ext(sprSkillIconHUD,dir,uxoff+view_xview+view_wview-12-16*dix,view_yview+13,1,1,0,c_black,1)
    draw_sprite_ext(sprSkillIconHUD,dir,uxoff+view_xview+view_wview-12-16*dix,view_yview+12,1,1,0,c_white,1)
    dix += 1

}

}
else if !instance_exists(GenCont)
{
scrDrawGameOver()
}


//PICKUP STUFF
draw_set_halign(fa_center)
draw_set_font(fntM)
with WepPickup
{
if place_meeting(x,y,Player) and instance_nearest(Player.x,Player.y,WepPickup) = self and wep != 0
{
draw_sprite(sprEPickup,UberCont.opt_gamepad,x,y-7)
if type = 1{
draw_sprite(sprBulletIconBG,2,x+7,y-21)
draw_sprite(sprBulletIcon,7-ceil((Player.ammo[type]/Player.typ_amax[type])*7),x+7,y-21)}
if type = 2{
draw_sprite(sprShotIconBG,2,x+7,y-21)
draw_sprite(sprShotIcon,7-ceil((Player.ammo[type]/Player.typ_amax[type])*7),x+7,y-21)}
if type = 3{
draw_sprite(sprBoltIconBG,2,x+7,y-21)
draw_sprite(sprBoltIcon,7-ceil((Player.ammo[type]/Player.typ_amax[type])*7),x+7,y-21)}
if type = 4{
draw_sprite(sprExploIconBG,2,x+7,y-21)
draw_sprite(sprExploIcon,7-ceil((Player.ammo[type]/Player.typ_amax[type])*7),x+7,y-21)}
if type = 5{
draw_sprite(sprEnergyIconBG,2,x+7,y-21)
draw_sprite(sprEnergyIcon,7-ceil((Player.ammo[type]/Player.typ_amax[type])*7),x+7,y-21)}

draw_set_color(c_black)
draw_text(x,y-30,string(name))
draw_text(x+1,y-30,string(name))
draw_text(x+1,y-31,string(name))
draw_set_color(c_white)
draw_text(x,y-31,string(name))
//draw_sprite(sprAmmoPointer,0,view_xview+5-10+type*10,view_yview+32+12)
}
}

//VENUS CAR
with CarVenusFixed
{
if place_meeting(x,y,Player)
{
draw_sprite(sprEPickup,UberCont.opt_gamepad,x,y-7)

draw_set_color(c_black)
draw_text(x,y-30,string(name))
draw_text(x+1,y-30,string(name))
draw_text(x+1,y-31,string(name))
draw_set_color(c_white)
draw_text(x,y-31,string(name))
//draw_sprite(sprAmmoPointer,0,view_xview+5-10+type*10,view_yview+32+12)
}
}

//VENUS CAR
with CarVenusExit
{
if place_meeting(x,y,Player)
{
draw_sprite(sprEPickup,UberCont.opt_gamepad,x,y-7)

draw_set_color(c_black)
draw_text(x,y-30,string(name))
draw_text(x+1,y-30,string(name))
draw_text(x+1,y-31,string(name))
draw_set_color(c_white)
draw_text(x,y-31,string(name))
//draw_sprite(sprAmmoPointer,0,view_xview+5-10+type*10,view_yview+32+12)
}
}

//CORPSE FLOWER
with CorpseFlower
{
if place_meeting(x,y,Player)
{
draw_sprite(sprEPickup,UberCont.opt_gamepad,x,y-7)

draw_set_color(c_black)
draw_text(x,y-30,string(name))
draw_text(x+1,y-30,string(name))
draw_text(x+1,y-31,string(name))
draw_set_color(c_white)
draw_text(x,y-31,string(name))
//draw_sprite(sprAmmoPointer,0,view_xview+5-10+type*10,view_yview+32+12)
}
}

//grid
//with Floor
//draw_rectangle(x,y,x+32,y+32,1)
