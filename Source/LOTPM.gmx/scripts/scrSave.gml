scrRaces() var dir; dir = 1
repeat(racemax){
ini_write_real("CHARACTERS","Total Kills: Mutant"+string(dir),ctot_kill[dir])
ini_write_real("CHARACTERS","Total Time: Mutant"+string(dir),ctot_time[dir])
ini_write_real("CHARACTERS","Total Loops: Mutant"+string(dir),ctot_loop[dir])
ini_write_real("CHARACTERS","Total Deaths: Mutant"+string(dir),ctot_dead[dir])

ini_write_real("CHARACTERS","Most Kills: Mutant"+string(dir),cbst_kill[dir])
ini_write_real("CHARACTERS","Best Time: Mutant"+string(dir),cbst_time[dir])
ini_write_real("CHARACTERS","Best Loop: Mutant"+string(dir),cbst_loop[dir])
ini_write_real("CHARACTERS","Best Difficulty: Mutant"+string(dir),cbst_diff[dir])

ini_write_real("CHARACTERS","Start Wep: Mutant"+string(dir),cwep[dir])
ini_write_real("CHARACTERS","Unlocked: Mutant"+string(dir),cgot[dir])
dir += 1}

ini_write_real("MISC","Protowep",protowep)
ini_write_real("MISC","Total Time",tot_time)

ini_write_real("OPTIONS","Sound Effects Volume",opt_sfxvol)
ini_write_real("OPTIONS","Music Volume",opt_musvol)
ini_write_real("OPTIONS","Ambient Volume",opt_ambvol)

ini_write_real("OPTIONS","Fullscreen",opt_fulscrn)
ini_write_real("OPTIONS","Scaling",opt_scaling)
ini_write_real("OPTIONS","Nice Shadows",opt_niceshd)
ini_write_real("OPTIONS","Nice Dark",opt_nicedrk)

ini_write_real("OPTIONS","Use Gamepad",opt_gamepad)
ini_write_real("OPTIONS","Gamepad Autoaim",opt_autoaim)

ini_write_real("OPTIONS","Screenshake",opt_shake)
ini_write_real("OPTIONS","Capture Mouse",opt_mousecp)

ini_write_real("OPTIONS","Fit Screen",opt_fitscrn)

ini_write_real("OPTIONS","View Scaling",opt_scaling)
