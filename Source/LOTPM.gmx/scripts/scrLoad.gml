scrRaces() var dir; dir = 1
repeat(racemax){
ctot_kill[dir] = ini_read_real("CHARACTERS","Total Kills: Mutant"+string(dir),0)
ctot_time[dir] = ini_read_real("CHARACTERS","Total Time: Mutant"+string(dir),0)
ctot_loop[dir] = ini_read_real("CHARACTERS","Total Loops: Mutant"+string(dir),0)
ctot_dead[dir] = ini_read_real("CHARACTERS","Total Deaths: Mutant"+string(dir),0)

cbst_kill[dir] = ini_read_real("CHARACTERS","Most Kills: Mutant"+string(dir),0)
cbst_time[dir] = ini_read_real("CHARACTERS","Best Time: Mutant"+string(dir),0)
cbst_loop[dir] = ini_read_real("CHARACTERS","Best Loop: Mutant"+string(dir),0)
cbst_diff[dir] = ini_read_real("CHARACTERS","Best Difficulty: Mutant"+string(dir),0)

cwep[dir] = ini_read_real("CHARACTERS","Start Wep: Mutant"+string(dir),1)
cgot[dir] = ini_read_real("CHARACTERS","Unlocked: Mutant"+string(dir),0)
dir += 1}

protowep = ini_read_real("MISC","Protowep",58)
tot_time = ini_read_real("MISC","Total Time",0)

opt_sfxvol = ini_read_real("OPTIONS","Sound Effects Volume",1)
opt_musvol = ini_read_real("OPTIONS","Music Volume",1)
opt_ambvol = ini_read_real("OPTIONS","Ambient Volume",1)

opt_fulscrn = ini_read_real("OPTIONS","Fullscreen",1)
opt_scaling = ini_read_real("OPTIONS","Scaling",4)
opt_niceshd = ini_read_real("OPTIONS","Nice Shadows",1)
opt_nicedrk = ini_read_real("OPTIONS","Nice Dark",1)

opt_gamepad = ini_read_real("OPTIONS","Use Gamepad",0)
opt_autoaim = ini_read_real("OPTIONS","Gamepad Autoaim",0)

opt_shake = ini_read_real("OPTIONS","Screenshake",1)
opt_mousecp = ini_read_real("OPTIONS","Capture Mouse",0)

opt_fitscrn = ini_read_real("OPTIONS","Fit Screen",0)

opt_scaling = ini_read_real("OPTIONS","View Scaling",min(floor(display_get_width()/320),floor(display_get_height()/240)))
