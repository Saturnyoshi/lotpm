draw_set_blend_mode(bm_add)
//draw_set_blend_mode(bm_add)

with DevastatorImpact
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*1.3,image_xscale*1.3,image_angle,c_white,0.1)
with FlameFlakBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with SuperFlameFlakBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with EnemyFlak
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with HeavyBullet2
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with AestheticFlame
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with BFGBall
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with HomingBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with ShrapnelBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with ProtoBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with EnemyBulletRot
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with BlasterBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with DevourerBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with ElderScorpionBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with ElderLaser if sprite_index = sprElderLaser or sprite_index = sprElderLaserStart or sprite_index = sprElderLaserEnd
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with ElderThroneBall
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with BruteProjectile
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with HunterBouncerBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with RuinsSargeBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with UltraBullet1
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with UltraBullet2
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with HeavySlug
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with BouncyBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with FlamePellet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with SuperFlakBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with HeavyBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_xscale*2,image_angle,c_white,0.1)
with Bullet22
draw_sprite_ext(sprite_index,-1,x,y,image_xscale+.5,image_xscale+.5,image_angle,c_white,0.1)
with ThroneBall3
draw_sprite_ext(sprite_index,-1,x,y,image_xscale+.5,image_xscale+.5,image_angle,c_white,0.1)
with MushProp1
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with VenomCannon
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_yscale*2,image_angle,c_white,0.1)
with VenomBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_yscale*2,image_angle,c_white,0.1)
with EmperorBullet1
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with WormGuardBody
draw_sprite_ext(sprite_index,-1,x,y,1.3,1.3,image_angle,c_white,0.1)
with WormGuardHead
draw_sprite_ext(sprite_index,-1,x,y,1.3,1.3,direction,c_white,0.1)
with ThroneBall2
draw_sprite_ext(sprite_index,-1,x,y,image_xscale+.5,image_xscale+.5,image_angle,c_white,0.1)
with ThroneBall
draw_sprite_ext(sprite_index,-1,x,y,image_xscale+.5,image_xscale+.5,image_angle,c_white,0.1)
with VenomBullet2
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_yscale*2,image_angle,c_white,0.1)
with VenomBullet
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_yscale*2,image_angle,c_white,0.1)
with MiniFlakBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with RicochetBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with RicochetCannonBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with EmperorProjectile1
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with BratataProjectile
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,choose(c_red,c_yellow,c_aqua,c_lime,c_purple),0.1)
with PlasmaBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with BruteProjectile
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with HorrorBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Bullet1
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with ToxicGas
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Deflect
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Bullet2
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with PlasmaBall
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with LaserCannon
draw_sprite_ext(sprite_index,-1,x,y,image_xscale*2,image_yscale*2,image_angle,c_white,0.1)
with Lightning
draw_sprite_ext(sprite_index,-1,x,y,image_xscale,image_yscale*2,image_angle,c_white,0.1)
with FlakBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with PlasmaBig
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with PlasmaImpact
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with EnemyBullet3
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Slug
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Disc
draw_sprite_ext(sprite_index,-1,xprevious,yprevious,1,1,image_angle,c_white,0.1)
with Laser
{
draw_sprite_ext(sprite_index,-1,x,y,image_xscale,2,image_angle,c_white,0.1)
draw_sprite_ext(sprLaserStart,img,xstart,ystart,2,2,image_angle,c_white,0.1)
draw_sprite_ext(sprLaserEnd,img,x,y,2,2,image_angle,c_white,0.1)
}
with EnemyLaser
{
draw_sprite_ext(sprite_index,-1,x,y,image_xscale,2,image_angle,c_white,0.1)
draw_sprite_ext(sprEnemyLaserStart,img,xstart,ystart,2,2,image_angle,c_white,0.1)
draw_sprite_ext(sprEnemyLaserEnd,img,x,y,2,2,image_angle,c_white,0.1)
}

with Floor
{
if sprite_index = sprFloor2B
draw_sprite_ext(sprFloor2BBloom,image_index,x,y,1,1,image_angle,c_white,0.1)

if sprite_index = sprFloor102B
draw_sprite_ext(sprFloor102BBloom,image_index,x,y,1,1,image_angle,c_white,0.1)
}

with LaserCharge
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with EnemyBullet1
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with AllyBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with RadChest
draw_sprite_ext(sprRadChestGlow,-1,x,y,2,2,image_angle,c_white,0.1)
with EnemyBullet2
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with EnemyBullet3
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with EnemyBullet4
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with IDPDBullet
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Explosion
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Bolt
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with MeatExplosion
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with BulletHit
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with EBulletHit
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with ScorpionBulletHit
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with TrapFire
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Flame
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with Portal
draw_sprite_ext(sprite_index,-1,x,y,2*image_xscale,2*image_yscale,image_angle,c_white,0.1)
with IDPDSpawn
draw_sprite_ext(sprite_index,-1,x,y,2*image_xscale,2*image_yscale,image_angle,c_white,0.1)

with Rad
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
with BigRad
draw_sprite_ext(sprite_index,-1,x,y,2,2,image_angle,c_white,0.1)
draw_set_blend_mode(bm_normal)
