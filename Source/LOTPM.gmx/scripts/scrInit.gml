//INIT DATA
if !file_exists("lotpm"+string(version)+".sav"){
    ini_open("lotpm"+string(version)+".sav")
    scrInitStats()
    scrInitOptions()
    scrInitData()
    ini_close()
} else {
    ini_open("lotpm"+string(version)+".sav")
    scrLoad()
    ini_close()
}

if opt_gamepad = 1 and !joystick_exists(1)
opt_gamepad = 0

window_set_fullscreen(opt_fulscrn)

//window_set_region_scale(1-opt_fitscrn*2,0);
