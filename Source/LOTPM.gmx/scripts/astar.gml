/// astar(speed, goalx, goaly);

// init
var astar_grid, astar_path, astar_cellSize, astar_xTop, astar_yTop, astar_xBot, astar_yBot, astar_offset;
astar_cellSize = 16;
astar_xTop = 20032;
astar_yTop = 20032;
astar_xBot = 0;
astar_yBot = 0;
with(Wall)
{
    if(x < astar_xTop)
    {
        astar_xTop = x;
    }
    if(y < astar_yTop)
    {
        astar_yTop = y;
    }
    if(x > astar_xBot)
    {
        astar_xBot = x;
    }
    if(y > astar_yBot)
    {
        astar_yBot = y;
    }
}
astar_offset = 64;
astar_grid = mp_grid_create(astar_xTop,astar_yTop,astar_xBot/astar_cellSize,astar_yBot/astar_cellSize,astar_cellSize,astar_cellSize);
astar_path = path_add();
mp_grid_add_instances(astar_grid,Wall,1);
mp_grid_add_instances(astar_grid,Barrel,1);
mp_grid_add_instances(astar_grid,ToxicBarrel,1);
mp_grid_add_instances(astar_grid,ToxicGas,1);
mp_grid_add_instances(astar_grid,Explosion,1);
mp_grid_path(astar_grid,astar_path,x,y,argument1,argument2,1);

// move

_X = x;
_Y = y;
path_start(astar_path,argument0,0,0);
x = _X;
y = _Y;
motion_add(point_direction(x,y,path_get_point_x(astar_path,1),path_get_point_y(astar_path,1)),argument0);

// dilit
Pastar_path = path_duplicate(astar_path);
path_delete(astar_path);
mp_grid_destroy(astar_grid);
