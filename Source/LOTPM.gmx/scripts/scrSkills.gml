skill_name[0] = ""
skill_text[0] = ""
skill_msnd[0] = sndMut
skill_tips[0] = ""

skill_name[1] = "RHINO SKIN"
skill_text[1] = "+4 MAX @rHP@s"
skill_msnd[1] = sndMutRhinoSkin
skill_tips[1] = "thick skin"
skill_rare[1] = 1; //DECIDES RARITY. HIGHER = MORE RARE

skill_name[2] = "EXTRA FEET"
skill_text[2] = "MORE @wSPEED@s"
skill_msnd[2] = sndMutExtraFeet
skill_tips[2] = "run forever"
skill_rare[2] = 1;

skill_name[3] = "PLUTONIUM HUNGER"
skill_text[3] = "STRONGER @gATTRACT@s POWER"
skill_msnd[3] = sndMutPlutoniumHunger
skill_tips[3] = "need those rads"
skill_rare[3] = 1;

skill_name[4] = "RABBIT PAW"
skill_text[4] = "MORE @rHP@s AND @yAMMO@s DROPS"
skill_msnd[4] = sndMutRabbitPaw
skill_tips[4] = "feeling lucky"
skill_rare[4] = 1;

skill_name[5] = "THRONE BUTT"
skill_text[5] = "UPGRADES YOUR SPECIAL ABILITY"
if instance_exists(Player)
skill_text[5] = string(Player.race_name[Player.race])+" "+string(Player.race_butt[Player.race])
skill_msnd[5] = sndMutThroneButt
skill_tips[5] = "sit on the throne"
skill_rare[5] = 1;

skill_name[6] = "LUCKY SHOT"
skill_text[6] = "SOME KILLS REGENERATE @yAMMO@s"
skill_msnd[6] = sndMutLuckyShot
skill_tips[6] = "ammo everywhere"
skill_rare[6] = 1;

skill_name[7] = "BLOODLUST"
skill_text[7] = "SOME KILLS REGENERATE @rHP@s"
skill_msnd[7] = sndMutBloodLust
skill_tips[7] = "drink blood"
skill_rare[7] = 1;

skill_name[8] = "GAMMA GUTS"
skill_text[8] = "@wENEMIES@s TOUCHING YOU TAKE DAMAGE"
skill_msnd[8] = sndMutGammaGuts
skill_tips[8] = "skin glows"
skill_rare[8] = 1;

skill_name[9] = "SECOND STOMACH"
skill_text[9] = "MORE @rHP@s FROM MEDKITS"
skill_msnd[9] = sndMutSecondStomach
skill_tips[9] = "stomach rumbles"
skill_rare[9] = 1;

skill_name[10] = "BACK MUSCLE"
skill_text[10] = "HIGHER @yAMMO@s MAX"
skill_msnd[10] = sndMutBackMuscle
skill_tips[10] = "great strength"
skill_rare[10] = 1;

skill_name[11] = "SCARIER FACE"
skill_text[11] = "LESS @wENEMY @rHP@s"
skill_msnd[11] = sndMutScarierFace
skill_tips[11] = "mirrors will break"
skill_rare[11] = 1;

skill_name[12] = "EUPHORIA"
skill_text[12] = "SLOWER @wENEMY BULLETS@s"
skill_msnd[12] = sndMutEuphoria
skill_tips[12] = "time passes slowly"
skill_rare[12] = 1;

skill_name[13] = "LONG ARMS"
skill_text[13] = "LONGER @wMELEE@s RANGE"
skill_msnd[13] = sndMutLongArms
skill_tips[13] = "more reach"
skill_rare[13] = 1;

skill_name[14] = "BOILING VEINS"
skill_text[14] = "@wNO DAMAGE@s FROM EXPLOSIONS AND FIRE#WHEN UNDER 4 @rHP@s"
skill_msnd[14] = sndMutBoilingVeins
skill_tips[14] = "temperature is rising"
skill_rare[14] = 1;

skill_name[15] = "SHOTGUN SHOULDERS"
skill_text[15] = "@wSHELLS@s BOUNCE FURTHER"
skill_msnd[15] = sndMutShotgunFingers
skill_tips[15] = "shells are friends"
skill_rare[15] = 1;

skill_name[16] = "RECYCLE GLAND"
skill_text[16] = "MOST HIT @wBULLETS@s BECOME @yAMMO@s"
skill_msnd[16] = sndMutRecycleGland
skill_tips[16] = "no need to aim"
skill_rare[16] = 1;

skill_name[17] = "LASER BRAIN"
skill_text[17] = "@wENERGY WEAPONS@s DEAL MORE DAMAGE"
skill_msnd[17] = sndMutLaserBrain
skill_tips[17] = "neurons everywhere"
skill_rare[17] = 1;

skill_name[18] = "LAST WISH"
skill_text[18] = "GET @rFULL HP@s AND SOME @yAMMO@s"
skill_msnd[18] = sndMutLastWish
skill_tips[18] = "listen"
skill_rare[18] = 1;

skill_name[19] = "EAGLE EYES"
skill_text[19] = "BETTER @wACCURACY@s"
skill_msnd[19] = sndMutEagleEyes
skill_tips[19] = "every shot connects"
skill_rare[19] = 1;

skill_name[20] = "IMPACT WRISTS"
skill_text[20] = "@wCORPSES@s FLY & HIT HARDER"
skill_msnd[20] = sndMutImpactWrists
skill_tips[20] = "see them fly"
skill_rare[20] = 1;

skill_name[21] = "BOLT MARROW"
skill_text[21] = "HOMING @wBOLTS@s"
skill_msnd[21] = sndMutBoltMarrow
skill_tips[21] = "bolts everywhere"
skill_rare[21] = 1;

skill_name[22] = "STRESS"
skill_text[22] = "HIGHER @wRATE OF FIRE@s#AS @rHP@s GETS LOWER"
skill_msnd[22] = sndMutStress
skill_tips[22] = "shaking"
skill_rare[22] = 1;

// LOTPM MUTATIONS
// LOTPM MUTATIONS
// LOTPM MUTATIONS
// LOTPM MUTATIONS
skill_name[23] = "BURSTING APPENDIX" // name
skill_text[23] = "@wENEMIES@s EXPLODE#INTO @wBULLETS@s" // description
skill_msnd[23] = sndMut // sound when picking it
skill_tips[23] = "boom" // tip
skill_rare[23] = 1;

skill_name[24] = "STEAMY LUNGS" // name
skill_text[24] = "LEAVES A TRAIL OF#@dDAMAGING SMOKE@s" // description
skill_msnd[24] = sndMut // stund when picking it
skill_tips[24] = "cough cough" // tip
skill_rare[24] = 1;

skill_name[25] = "BRAWNY KNUCKLES" // name
skill_text[25] = "LARGER, SLOWER @wMELEE SWINGS@s" // description
skill_msnd[25] = sndMut // sound when picking it
skill_tips[25] = "long arms won't save you" // tip
skill_rare[25] = 1;

skill_name[26] = "ERUPTING ROOTS" // name
skill_text[26] = "PROJECTILES CREATES#@rBLOOD@s EXPLOSIONS" // description
skill_msnd[26] = sndMut // sound when picking it
skill_tips[26] = "bloodshot" // tip
skill_rare[26] = 1;

skill_name[27] = "SLOW DAY" // name
skill_text[27] = "DOES NOTHING" // description
skill_msnd[27] = sndMut // sound when picking it
skill_tips[27] = "what is it good for" // tip
skill_rare[27] = 1;

skill_name[28] = "DEFENSIVE CLAWS" // name
skill_text[28] = "CREATES A @wMELEE SWING@s#WHEN HIT" // description
skill_msnd[28] = sndMut // sound when picking it
skill_tips[28] = "scratch em" // tip
skill_rare[28] = 1;

skill_name[29] = "PATIENCE" // name
skill_text[29] = "@gMUTATE@s LATER" // description
skill_msnd[29] = sndMutPatience // sound when picking it
skill_tips[29] = "wait a second" // tip
skill_rare[29] = 1;

skill_name[30] = "TRIGGER FINGERS" // name
skill_text[30] = "@wKILLS@s LOWER @wRELOAD TIME@s" // description
skill_msnd[30] = sndMutTriggerFingers // sound when picking it
skill_tips[30] = "wait a second" // tip
skill_rare[30] = 1;

skill_name[31] = "STRONG SPIRIT" // name
skill_text[31] = "PREVENTS @wDEATH@s ONCE#RECHARGES AT @rFULL HP@s IN NEXT AREA" // description
skill_msnd[31] = sndMutStrongSpirit // sound when picking it
skill_tips[31] = "the best mutation" // tip
skill_rare[31] = 1;

skill_name[32] = "HAMMERHEAD" // name
skill_text[32] = "BREAK THROUGH @wWALLS@s" // description
skill_msnd[32] = sndMutHammerhead // sound when picking it
skill_tips[32] = "you won't regret it" // tip
skill_rare[32] = 1;

skill_name[33] = "NAPALM LIVER" // name
skill_text[33] = "FIREY EXPLOSIONS#MORE FIRE DAMAGE#MORE HAZARDS" // description
skill_msnd[33] = sndMut // sound when picking it
skill_tips[33] = "don't drink molotovs" // tip
skill_rare[33] = 1;

maxskill = 33

dir = 0
repeat(maxskill+1)
{skill_got[dir] = 0
dir += 1}
//skill_got[20] = 1

//SLOW DAY
skill_got[27] = -1;

//HEAVY HEART MUTATIONS
skill_got[25] = -1;
skill_got[33] = -1;
