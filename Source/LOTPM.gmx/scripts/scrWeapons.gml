//WEAPONS
wep_name[0] = ""
wep_type[0] = 0
wep_auto[0] = 0
wep_load[0] = 1
wep_cost[0] = 1
wep_sprt[0] = sprfuckingnothing
wep_area[0] = -1
wep_text[0] = ""
wep_swap[0] = sndSwapPistol

wep_name[1] = "REVOLVER"
wep_type[1] = 1
wep_auto[1] = 0
wep_load[1] = 6
wep_cost[1] = 1
wep_sprt[1] = sprRevolver
wep_area[1] = -1
wep_text[1] = "trusty old revolver"
wep_swap[1] = sndSwapPistol

wep_name[2] = "TRIPLE MACHINEGUN"
wep_type[2] = 1
wep_auto[2] = 1
wep_load[2] = 4
wep_cost[2] = 3
wep_sprt[2] = sprTripleMachinegun
wep_area[2] = 4
wep_text[2] = "triple machinegun, so much fun"
wep_swap[2] = sndSwapMachinegun

wep_name[3] = "SLEDGEHAMMER"
wep_type[3] = 0
wep_auto[3] = 0
wep_load[3] = 22
wep_cost[3] = 0
wep_sprt[3] = sprHammer
wep_area[3] = 1
wep_text[3] = "hammer time"
wep_swap[3] = sndSwapHammer

wep_name[4] = "MACHINEGUN"
wep_type[4] = 1
wep_auto[4] = 1
wep_load[4] = 5
wep_cost[4] = 1
wep_sprt[4] = sprMachinegun
wep_area[4] = 0
wep_text[4] = ""
wep_swap[4] = sndSwapMachinegun

wep_name[5] = "SHOTGUN"
wep_type[5] = 2
wep_auto[5] = 0
wep_load[5] = 20
wep_cost[5] = 1
wep_sprt[5] = sprShotgun
wep_area[5] = 0
wep_text[5] = ""
wep_swap[5] = sndSwapShotgun

wep_name[6] = "CROSSBOW"
wep_type[6] = 3
wep_auto[6] = 0
wep_load[6] = 25
wep_cost[6] = 1
wep_sprt[6] = sprCrossbow
wep_area[6] = 1
wep_text[6] = ""
wep_swap[6] = sndSwapBow

wep_name[7] = "GRENADE LAUNCHER"
wep_type[7] = 4
wep_auto[7] = 0
wep_load[7] = 20
wep_cost[7] = 1
wep_sprt[7] = sprNader
wep_area[7] = 1
wep_text[7] = "be careful with those grenades"
wep_swap[7] = sndSwapExplosive

wep_name[8] = "DOUBLE SHOTGUN"
wep_type[8] = 2
wep_auto[8] = 0
wep_load[8] = 35
wep_cost[8] = 2
wep_sprt[8] = sprSuperShotgun
wep_area[8] = 5
wep_text[8] = "double shotgun, double fun"
wep_swap[8] = sndSwapShotgun

wep_name[9] = "MINIGUN"
wep_type[9] = 1
wep_auto[9] = 1
wep_load[9] = 1
wep_cost[9] = 1
wep_sprt[9] = sprMinigun
wep_area[9] = 6
wep_text[9] = "time to rain bullets"
wep_swap[9] = sndSwapMachinegun

wep_name[10] = "AUTO SHOTGUN"
wep_type[10] = 2
wep_auto[10] = 1
wep_load[10] = 4
wep_cost[10] = 1
wep_sprt[10] = sprAutoShotgun
wep_area[10] = 5
wep_text[10] = "just hold down the trigger"
wep_swap[10] = sndSwapShotgun

wep_name[11] = "AUTO CROSSBOW"
wep_type[11] = 3
wep_auto[11] = 1
wep_load[11] = 8
wep_cost[11] = 1
wep_sprt[11] = sprAutoCrossbow
wep_area[11] = 7
wep_text[11] = "225 bolts per minute"
wep_swap[11] = sndSwapBow

wep_name[12] = "SUPER CROSSBOW"
wep_type[12] = 3
wep_auto[12] = 0
wep_load[12] = 30
wep_cost[12] = 5
wep_sprt[12] = sprSuperCrossbow
wep_area[12] = 7
wep_text[12] = "5 bolts per shot"
wep_swap[12] = sndSwapBow

wep_name[13] = "SHOVEL"
wep_type[13] = 0
wep_auto[13] = 0
wep_load[13] = 35
wep_cost[13] = 0
wep_sprt[13] = sprShovel
wep_area[13] = 4
wep_text[13] = "dig"
wep_swap[13] = sndSwapHammer

wep_name[14] = "BAZOOKA"
wep_type[14] = 4
wep_auto[14] = 0
wep_load[14] = 30
wep_cost[14] = 1
wep_sprt[14] = sprBazooka
wep_area[14] = 5
wep_text[14] = ""
wep_swap[14] = sndSwapExplosive

wep_name[15] = "STICKY LAUNCHER"
wep_type[15] = 4
wep_auto[15] = 0
wep_load[15] = 25
wep_cost[15] = 1
wep_sprt[15] = sprStickyNader
wep_area[15] = 7
wep_text[15] = "dont touch sticky nades"
wep_swap[15] = sndSwapExplosive

wep_name[16] = "SMG"
wep_type[16] = 1
wep_auto[16] = 1
wep_load[16] = 3
wep_cost[16] = 1
wep_sprt[16] = sprSmg
wep_area[16] = 3
wep_text[16] = ""
wep_swap[16] = sndSwapPistol

wep_name[17] = "ASSAULT RIFLE"
wep_type[17] = 1
wep_auto[17] = 0
wep_load[17] = 11
wep_cost[17] = 3
wep_sprt[17] = sprARifle
wep_area[17] = 2
wep_text[17] = ""
wep_swap[17] = sndSwapMachinegun

wep_name[18] = "DISC GUN"
wep_type[18] = 3
wep_auto[18] = 1
wep_load[18] = 8
wep_cost[18] = 1
wep_sprt[18] = sprDiscGun
wep_area[18] = 3
wep_text[18] = "we hereby sincerely apologize"
wep_swap[18] = sndSwapBow

wep_name[19] = "LASER PISTOL"
wep_type[19] = 5
wep_auto[19] = 0
wep_load[19] = 7
wep_cost[19] = 1
wep_sprt[19] = sprLaserGun
wep_area[19] = 2
wep_text[19] = "futuristic weaponry"
wep_swap[19] = sndSwapEnergy

wep_name[20] = "LASER RIFLE"
wep_type[20] = 5
wep_auto[20] = 1
wep_load[20] = 5
wep_cost[20] = 1
wep_sprt[20] = sprLaserRifle
wep_area[20] = 5
wep_text[20] = ""
wep_swap[20] = sndSwapEnergy

wep_name[21] = "SLUGGER"
wep_type[21] = 2
wep_auto[21] = 0
wep_load[21] = 22
wep_cost[21] = 1
wep_sprt[21] = sprSlugger
wep_area[21] = 2
wep_text[21] = ""
wep_swap[21] = sndSwapShotgun

wep_name[22] = "GATLING SLUGGER"
wep_type[22] = 2
wep_auto[22] = 1
wep_load[22] = 7
wep_cost[22] = 1
wep_sprt[22] = sprGatlingSlugger
wep_area[22] = 8
wep_text[22] = "time to gatle"
wep_swap[22] = sndSwapShotgun

wep_name[23] = "ASSAULT SLUGGER"
wep_type[23] = 2
wep_auto[23] = 0
wep_load[23] = 20
wep_cost[23] = 3
wep_sprt[23] = sprAssaultSlugger
wep_area[23] = 5
wep_text[23] = ""
wep_swap[23] = sndSwapShotgun

wep_name[24] = "ENERGY SWORD"
wep_type[24] = 5
wep_auto[24] = 0
wep_load[24] = 8
wep_cost[24] = 2
wep_sprt[24] = sprEnergySword
wep_area[24] = 7
wep_text[24] = "zzzwwoonggg"
wep_swap[24] = sndSwapEnergy

wep_name[25] = "SUPER SLUGGER"
wep_type[25] = 2
wep_auto[25] = 0
wep_load[25] = 40
wep_cost[25] = 5
wep_sprt[25] = sprSuperSlugger
wep_area[25] = 10
wep_text[25] = "no need to aim"
wep_swap[25] = sndSwapShotgun

wep_name[26] = "HYPER RIFLE"
wep_type[26] = 1
wep_auto[26] = 0
wep_load[26] = 3
wep_cost[26] = 6
wep_sprt[26] = sprHyperRifle
wep_area[26] = 8
wep_text[26] = "hyper time"
wep_swap[26] = sndSwapMachinegun

wep_name[27] = "SCREWDRIVER"
wep_type[27] = 0
wep_auto[27] = 0
wep_load[27] = 11
wep_cost[27] = 0
wep_sprt[27] = sprScrewDriver
wep_area[27] = 2
wep_text[27] = "screwdriver will fix it"
wep_swap[27] = sndSwapSword

wep_name[28] = "LASER MINIGUN"
wep_type[28] = 5
wep_auto[28] = 1
wep_load[28] = 2
wep_cost[28] = 1
wep_sprt[28] = sprLaserMinigun
wep_area[28] = 8
wep_text[28] = "energy bill off the charts"
wep_swap[28] = sndSwapEnergy

wep_name[29] = "BLOOD LAUNCHER"
wep_type[29] = 4
wep_auto[29] = 1
wep_load[29] = 12
wep_cost[29] = 1
wep_sprt[29] = sprBloodNader
wep_area[29] = 10
wep_text[29] = "built with spare parts"
wep_swap[29] = sndSwapExplosive

wep_name[30] = "SPLINTER GUN"
wep_type[30] = 3
wep_auto[30] = 0
wep_load[30] = 26
wep_cost[30] = 1
wep_sprt[30] = sprSplinterGun
wep_area[30] = 5
wep_text[30] = "this will hurt"
wep_swap[30] = sndSwapBow

wep_name[31] = "TOXIC BOW"
wep_type[31] = 3
wep_auto[31] = 0
wep_load[31] = 29
wep_cost[31] = 1
wep_sprt[31] = sprToxicBow
wep_area[31] = 5
wep_text[31] = "hold breath while firing"
wep_swap[31] = sndSwapBow

wep_name[32] = "SENTRY GUN"
wep_type[32] = 1
wep_auto[32] = 1
wep_load[32] = 60
wep_cost[32] = 24
wep_sprt[32] = sprSentryGun
wep_area[32] = 8
wep_text[32] = ""
wep_swap[32] = sndSwapMachinegun

wep_name[33] = "WAVE GUN"
wep_type[33] = 2
wep_auto[33] = 0
wep_load[33] = 20
wep_cost[33] = 2
wep_sprt[33] = sprWaveGun
wep_area[33] = 12
wep_text[33] = "shoot em up"
wep_swap[33] = sndSwapShotgun

wep_name[34] = "PLASMA GUN"
wep_type[34] = 5
wep_auto[34] = 1
wep_load[34] = 16
wep_cost[34] = 2
wep_sprt[34] = sprPlasmaGun
wep_area[34] = 5
wep_text[34] = "fun fun"
wep_swap[34] = sndSwapEnergy

wep_name[35] = "PLASMA CANNON"
wep_type[35] = 5
wep_auto[35] = 0
wep_load[35] = 45
wep_cost[35] = 8
wep_sprt[35] = sprPlasmaCannon
wep_area[35] = 8
wep_text[35] = "fun fun fun"
wep_swap[35] = sndSwapEnergy

wep_name[36] = "ENERGY HAMMER"
wep_type[36] = 5
wep_auto[36] = 0
wep_load[36] = 20
wep_cost[36] = 4
wep_sprt[36] = sprEnergyHammer
wep_area[36] = 7
wep_text[36] = "break a leg"
wep_swap[36] = sndSwapHammer

wep_name[37] = "JACKHAMMER"
wep_type[37] = 4
wep_auto[37] = 1
wep_load[37] = 12
wep_cost[37] = 1
wep_sprt[37] = sprJackHammer
wep_area[37] = 5
wep_text[37] = "break some legs"
wep_swap[37] = sndSwapMotorized

wep_name[38] = "FLAK CANNON"
wep_type[38] = 2
wep_auto[38] = 0
wep_load[38] = 26
wep_cost[38] = 2
wep_sprt[38] = sprFlakCannon
wep_area[38] = 6
wep_text[38] = "10/10"
wep_swap[38] = sndSwapExplosive

wep_name[39] = "GOLDEN REVOLVER"
wep_type[39] = 1
wep_auto[39] = 0
wep_load[39] = 5
wep_cost[39] = 1
wep_sprt[39] = sprGoldRevolver
wep_area[39] = -1
wep_text[39] = "B-)"
wep_swap[39] = sndSwapPistol

wep_name[40] = "GOLDEN HAMMER"
wep_type[40] = 0
wep_auto[40] = 0
wep_load[40] = 18
wep_cost[40] = 0
wep_sprt[40] = sprGoldHammer
wep_area[40] = 19
wep_text[40] = "shiny hammer"
wep_swap[40] = sndSwapHammer

wep_name[41] = "GOLDEN MACHINEGUN"
wep_type[41] = 1
wep_auto[41] = 1
wep_load[41] = 5
wep_cost[41] = 1
wep_sprt[41] = sprGoldMachinegun
wep_area[41] = 19
wep_text[41] = "expensive machinegun"
wep_swap[41] = sndSwapMachinegun

wep_name[42] = "GOLDEN SHOTGUN"
wep_type[42] = 2
wep_auto[42] = 0
wep_load[42] = 24
wep_cost[42] = 1
wep_sprt[42] = sprGoldShotgun
wep_area[42] = 19
wep_text[42] = "beautiful shotgun"
wep_swap[42] = sndSwapShotgun

wep_name[43] = "GOLDEN CROSSBOW"
wep_type[43] = 3
wep_auto[43] = 0
wep_load[43] = 23
wep_cost[43] = 1
wep_sprt[43] = sprGoldCrossbow
wep_area[43] = 19
wep_text[43] = "velvet handles"
wep_swap[43] = sndSwapBow

wep_name[44] = "GOLDEN GRENADE LAUNCHER"
wep_type[44] = 4
wep_auto[44] = 0
wep_load[44] = 20
wep_cost[44] = 1
wep_sprt[44] = sprGoldNader
wep_area[44] = 19
wep_text[44] = "even the grenades are gold"
wep_swap[44] = sndSwapExplosive

wep_name[45] = "GOLDEN LASER PISTOL"
wep_type[45] = 5
wep_auto[45] = 0
wep_load[45] = 6
wep_cost[45] = 1
wep_sprt[45] = sprGoldLaserGun
wep_area[45] = 19
wep_text[45] = "this thing gets hot"
wep_swap[45] = sndSwapEnergy

wep_name[46] = "CHICKEN SWORD"
wep_type[46] = 0
wep_auto[46] = 0
wep_load[46] = 18
wep_cost[46] = 0
wep_sprt[46] = sprSword
wep_area[46] = -1
wep_text[46] = "chicken loves her sword"
wep_swap[46] = sndSwapSword

wep_name[47] = "NUKE LAUNCHER"
wep_type[47] = 4
wep_auto[47] = 0
wep_load[47] = 50
wep_cost[47] = 3
wep_sprt[47] = sprNukeLauncher
wep_area[47] = 10
wep_text[47] = "this is what started it all"
wep_swap[47] = sndSwapExplosive

wep_name[48] = "ION CANNON"
wep_type[48] = 5
wep_auto[48] = 1
wep_load[48] = 10
wep_cost[48] = 6
wep_sprt[48] = sprIonCannon
wep_area[48] = 10
wep_text[48] = "death from above"
wep_swap[48] = sndSwapEnergy


wep_name[49] = "QUADRUPLE MACHINEGUN"
wep_type[49] = 1
wep_auto[49] = 1
wep_load[49] = 4
wep_cost[49] = 4
wep_sprt[49] = sprQuadrupleMachinegun
wep_area[49] = 12
wep_text[49] = "the future is here"
wep_swap[49] = sndSwapMachinegun

wep_name[50] = "FLAMETHROWER"
wep_type[50] = 4
wep_auto[50] = 1
wep_load[50] = 6
wep_cost[50] = 1
wep_sprt[50] = sprFlameThrower
wep_area[50] = 6
wep_text[50] = "burn burn burn"
wep_swap[50] = sndSwapFlame

wep_name[51] = "DRAGON"
wep_type[51] = 4
wep_auto[51] = 1
wep_load[51] = 3
wep_cost[51] = 1
wep_sprt[51] = sprDragon
wep_area[51] = 13
wep_text[51] = "hot breath"
wep_swap[51] = sndSwapDragon


wep_name[52] = "FLARE GUN"
wep_type[52] = 4
wep_auto[52] = 0
wep_load[52] = 25
wep_cost[52] = 1
wep_sprt[52] = sprFlareGun
wep_area[52] = 8
wep_text[52] = "signal for help"
wep_swap[52] = sndSwapFlame


wep_name[53] = "ENERGY SCREWDRIVER"
wep_type[53] = 5
wep_auto[53] = 0
wep_load[53] = 5
wep_cost[53] = 1
wep_sprt[53] = sprEnergyScrewDriver
wep_area[53] = 9
wep_text[53] = "future fixing"
wep_swap[53] = sndSwapEnergy


wep_name[54] = "HYPER LAUNCHER"
wep_type[54] = 4
wep_auto[54] = 0
wep_load[54] = 7
wep_cost[54] = 1
wep_sprt[54] = sprHyperLauncher
wep_area[54] = 14
wep_text[54] = "point and click"
wep_swap[54] = sndSwapExplosive

wep_name[55] = "LASER CANNON"
wep_type[55] = 5
wep_auto[55] = 1
wep_load[55] = 30
wep_cost[55] = 3
wep_sprt[55] = sprLaserCannon
wep_area[55] = 7
wep_text[55] = "oh laser cannon"
wep_swap[55] = sndSwapEnergy

wep_name[56] = "RUSTY REVOLVER"
wep_type[56] = 1
wep_auto[56] = 0
wep_load[56] = 7
wep_cost[56] = 1
wep_sprt[56] = sprRustyRevolver
wep_area[56] = -1
wep_text[56] = "this revolver is ancient"
wep_swap[56] = sndSwapPistol

wep_name[57] = "LIGHTNING PISTOL"
wep_type[57] = 5
wep_auto[57] = 0
wep_load[57] = 11
wep_cost[57] = 1
wep_sprt[57] = sprLightningPistol
wep_area[57] = 7
wep_text[57] = "thunder"
wep_swap[57] = sndSwapEnergy

wep_name[58] = "LIGHTNING RIFLE"
wep_type[58] = 5
wep_auto[58] = 0
wep_load[58] = 28
wep_cost[58] = 2
wep_sprt[58] = sprLightningRifle
wep_area[58] = 9
wep_text[58] = "a storm is coming"
wep_swap[58] = sndSwapEnergy


wep_name[59] = "LIGHTNING SHOTGUN"
wep_type[59] = 5
wep_auto[59] = 0
wep_load[59] = 24
wep_cost[59] = 2
wep_sprt[59] = sprLightningShotgun
wep_area[59] = 9
wep_text[59] = "hurricane"
wep_swap[59] = sndSwapEnergy

//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////
//////////**** LOTPM WEAPONS***********////

wep_name[60] = "TOASTER"
wep_type[60] = 4 // 0 = melee, 1 = bullets, 2 = shells, 3 = bolts, 4 = explosives, 5 = energy
wep_auto[60] = 0
wep_load[60] = 45
wep_cost[60] = 4
wep_sprt[60] = sprWepToaster
wep_area[60] = -1
wep_text[60] = "BURN"
wep_swap[60] = sndSwapExplosive

wep_name[61] = "DISC MINIGUN"
wep_type[61] = 3 // 0 = melee, 1 = bullets, 2 = shells, 3 = bolts, 4 = explosives, 5 = energy
wep_auto[61] = 1
wep_load[61] = 1
wep_cost[61] = 1
wep_sprt[61] = sprWepDiscMinigun
wep_area[61] = 8
wep_text[61] = "IT'S GOING TO KILL YOU"
wep_swap[61] = sndSwapBow

wep_name[62] = "LIGHTNING LAUNCHER"
wep_type[62] = 5 // 0 = melee, 1 = bullets, 2 = shells, 3 = bolts, 4 = explosives, 5 = energy
wep_auto[62] = 0
wep_load[62] = 40
wep_cost[62] = 3
wep_sprt[62] = sprWepLightningLauncher
wep_area[62] = 8
wep_text[62] = "BOOM N ZAP"
wep_swap[62] = sndSwapEnergy

wep_name[63] = "FLARE BOW"
wep_type[63] = 4 // 0 = melee, 1 = bullets, 2 = shells, 3 = bolts, 4 = explosives, 5 = energy
wep_auto[63] = 0
wep_load[63] = 20
wep_cost[63] = 1
wep_sprt[63] = sprWepFlareBow
wep_area[63] = 6
wep_text[63] = "feed the fire"
wep_swap[63] = sndSwapBow

wep_name[64] = "CARBINE"
wep_type[64] = 2
wep_auto[64] = 0
wep_load[64] = 45
wep_cost[64] = 2
wep_sprt[64] = sprWepCarbine
wep_area[64] = 5
wep_text[64] = "SHOOT EM UP"
wep_swap[64] = sndSwapShotgun

wep_name[65] = "BULLET SHOTGUN"
wep_type[65] = 1
wep_auto[65] = 0
wep_load[65] = 17
wep_cost[65] = 3
wep_sprt[65] = sprWepBulletShotgun
wep_area[65] = 6
wep_text[65] = "SHOOT EM UP"
wep_swap[65] = sndSwapShotgun

wep_name[66] = "NAIL GUN"
wep_type[66] = 3
wep_auto[66] = 1
wep_load[66] = 3
wep_cost[66] = 1
wep_sprt[66] = sprWepNailgun
wep_area[66] = 5
wep_text[66] = "8 INCH NAILS"
wep_swap[66] = sndSwapMachinegun

wep_name[67] = "LASER SHOTGUN"
wep_type[67] = 5
wep_auto[67] = 0
wep_load[67] = 30
wep_cost[67] = 6
wep_sprt[67] = sprWepLaserShotgun
wep_area[67] = 8
wep_text[67] = "ZAP"
wep_swap[67] = sndSwapMachinegun

wep_name[68] = "DISC CANNON"
wep_type[68] = 3
wep_auto[68] = 0
wep_load[68] = 30
wep_cost[68] = 6
wep_sprt[68] = sprWepDiscCannon
wep_area[68] = 9
wep_text[68] = "..."
wep_swap[68] = sndSwapMachinegun

wep_name[69] = "SCYTHE"
wep_type[69] = 0
wep_auto[69] = 0
wep_load[69] = 15
wep_cost[69] = 0
wep_sprt[69] = sprWepScythe
wep_area[69] = -1
wep_text[69] = "dark harvest"
wep_swap[69] = sndSwapHammer

wep_name[70] = "ENERGY SCYTHE"
wep_type[70] = 5
wep_auto[70] = 0
wep_load[70] = 15
wep_cost[70] = 2
wep_sprt[70] = sprWepEnergyScythe
wep_area[70] = 9
wep_text[70] = "grim"
wep_swap[70] = sndSwapHammer

wep_name[71] = "SUPER DISC CANNON"
wep_type[71] = 3
wep_auto[71] = 0
wep_load[71] = 45
wep_cost[71] = 10
wep_sprt[71] = sprWepSuperDiscCannon
wep_area[71] = 15
wep_text[71] = "die"
wep_swap[71] = sndSwapBow

wep_name[72] = "PLASMA PISTOL"
wep_type[72] = 5
wep_auto[72] = 0
wep_load[72] = 10
wep_cost[72] = 1
wep_sprt[72] = sprWepPlasmaPistol
wep_area[72] = 5
wep_text[72] = ""
wep_swap[72] = sndSwapEnergy

wep_name[73] = "DOUBLE MACHINEGUN"
wep_type[73] = 1
wep_auto[73] = 1
wep_load[73] = 4
wep_cost[73] = 2
wep_sprt[73] = sprWepSawedOffMachineGun
wep_area[73] = 4
wep_text[73] = "where's your accuracy?"
wep_swap[73] = sndSwapPistol

wep_name[74] = "GRENADE PISTOL"
wep_type[74] = 4
wep_auto[74] = 0
wep_load[74] = 10
wep_cost[74] = 1
wep_sprt[74] = sprWepGrenadePistol
wep_area[74] = 1
wep_text[74] = "use with caution"
wep_swap[74] = sndSwapExplosive

wep_name[75] = "CAR LAUNCHER"
wep_type[75] = 4
wep_auto[75] = 0
wep_load[75] = 25
wep_cost[75] = 3
wep_sprt[75] = sprWepCarLauncher
wep_area[75] = 9
wep_text[75] = "automotive destruction"
wep_swap[75] = sndSwapExplosive

wep_name[76] = "SUPER SHOTGUN"
wep_type[76] = 2
wep_auto[76] = 0
wep_load[76] = 40
wep_cost[76] = 3
wep_sprt[76] = sprWepSuperShotgun
wep_area[76] = 10
wep_text[76] = "super shotgun, thrice the fun"
wep_swap[76] = sndSwapShotgun

wep_name[77] = "PLASMA SWORD"
wep_type[77] = 5
wep_auto[77] = 0
wep_load[77] = 8
wep_cost[77] = 2
wep_sprt[77] = sprWepPlasmaSword
wep_area[77] = 10
wep_text[77] = ""
wep_swap[77] = sndSwapEnergy

wep_name[78] = "NAIL CANNON"
wep_type[78] = 3
wep_auto[78] = 0
wep_load[78] = 30
wep_cost[78] = 7
wep_sprt[78] = sprWepNailCannon
wep_area[78] = 10
wep_text[78] = ""
wep_swap[78] = sndSwapPistol

wep_name[79] = "SUPER PLASMA CANNON"
wep_type[79] = 5
wep_auto[79] = 0
wep_load[79] = 240
wep_cost[79] = 24
wep_sprt[79] = sprWepSPC
wep_area[79] = 15
wep_text[79] = "FUN FUN FUN#fun fun fun"
wep_swap[79] = sndSwapEnergy

wep_name[80] = "BROOM"
wep_type[80] = 2
wep_auto[80] = 1
wep_load[80] = 5
wep_cost[80] = 1
wep_sprt[80] = sprWepBroom
wep_area[80] = 5
wep_text[80] = "clean"
wep_swap[80] = sndSwapEnergy

wep_name[81] = "UNUSED - METEORITE LAUNCHER"
wep_type[81] = 4
wep_auto[81] = 0
wep_load[81] = 90
wep_cost[81] = 5
wep_sprt[81] = sprMeteoriteLauncher
wep_area[81] = -1
wep_text[81] = "spiders from mars"
wep_swap[81] = sndSwapExplosive

wep_name[82] = "GRENADE MACHINEGUN"
wep_type[82] = 4
wep_auto[82] = 1
wep_load[82] = 8
wep_cost[82] = 1
wep_sprt[82] = sprWepGrenadeMachinegun
wep_area[82] = 6
wep_text[82] = "use with extra caution"
wep_swap[82] = sndSwapExplosive

wep_name[83] = "GRENADE RIFLE"
wep_type[83] = 4
wep_auto[83] = 0
wep_load[83] = 25
wep_cost[83] = 3
wep_sprt[83] = sprWepGrenadeRifle
wep_area[83] = 7
wep_text[83] = "go berserk"
wep_swap[83] = sndSwapExplosive

wep_name[84] = "SUPER SHUFFLE GUN"
wep_type[84] = irandom(4)+1
wep_auto[84] = 0
wep_load[84] = 40
wep_cost[84] = 1
wep_sprt[84] = sprWepShuffleGun
wep_area[84] = 7
wep_text[84] = "praise RNGESUS"
wep_swap[84] = sndSwapHammer

wep_name[85] = "SUPER ASSAULT RIFLE"
wep_type[85] = 1
wep_auto[85] = 0
wep_load[85] = 60
wep_cost[85] = 10
wep_sprt[85] = sprWepSuperAssaultRifle
wep_area[85] = 8
wep_text[85] = "shiny"
wep_swap[85] = sndSwapExplosive

wep_name[86] = "SUPER WAVE GUN"
wep_type[86] = 2
wep_auto[86] = 0
wep_load[86] = 130
wep_cost[86] = 25
wep_sprt[86] = sprWepSuperWaveGun
wep_area[86] = 11
wep_text[86] = "bratata?"
wep_swap[86] = sndSwapShotgun

wep_name[87] = "TSUNAMI GUN"
wep_type[87] = 2
wep_auto[87] = 0
wep_load[87] = 400
wep_cost[87] = 40
wep_sprt[87] = sprWepTsunamiGun
wep_area[87] = 15
wep_text[87] = "welcome to my personal (bullet) hell"
wep_swap[87] = sndSwapShotgun

wep_name[88] = "ROCKET LAUNCHER"
wep_type[88] = 4
wep_auto[88] = 0
wep_load[88] = 30
wep_cost[88] = 2
wep_sprt[88] = sprWepRocketLauncher
wep_area[88] = 5
wep_text[88] = "ka boom ka boom"
wep_swap[88] = sndSwapExplosive

wep_name[89] = "ASSAULT GRENADE LAUNCHER"
wep_type[89] = 4
wep_auto[89] = 0
wep_load[89] = 80
wep_cost[89] = 3
wep_sprt[89] = sprWepAssaultGrenadeLauncher
wep_area[89] = 12
wep_text[89] = "ka boom ka boom"
wep_swap[89] = sndSwapExplosive

wep_name[90] = "BRATATATATA"
wep_type[90] = 1
wep_auto[90] = 1
wep_load[90] = 2
wep_cost[90] = 5
wep_sprt[90] = sprWepBratata
wep_area[90] = 9
wep_text[90] = "bratatat"
wep_swap[90] = sndSwapShotgun

wep_name[91] = "AUTO TOXIC BOW"
wep_type[91] = 3
wep_auto[91] = 1
wep_load[91] = 8
wep_cost[91] = 1
wep_sprt[91] = sprWepAutoToxicBow
wep_area[91] = 7
wep_text[91] = "225 TOXIC bolts per minute"
wep_swap[91] = sndSwapBow

wep_name[92] = "SUPER TOXIC BOW"
wep_type[92] = 3
wep_auto[92] = 0
wep_load[92] = 30
wep_cost[92] = 5
wep_sprt[92] = sprWepSuperToxicBow
wep_area[92] = 10
wep_text[92] = "5 bolts per shot"
wep_swap[92] = sndSwapBow

wep_name[93] = "PLASMA MACHINEGUN"
wep_type[93] = 5
wep_auto[93] = 1
wep_load[93] = 7
wep_cost[93] = 1
wep_sprt[93] = sprWepPlasmaMachineGun
wep_area[93] = 6
wep_text[93] = ""
wep_swap[93] = sndSwapMachinegun

wep_name[94] = "PLASMA ASSAULT RIFLE"
wep_type[94] = 5
wep_auto[94] = 0
wep_load[94] = 45
wep_cost[94] = 3
wep_sprt[94] = sprWepPlasmaAssaultRifle
wep_area[94] = 4
wep_text[94] = ""
wep_swap[94] = sndSwapMachinegun

wep_name[95] = "UZI"
wep_type[95] = 1
wep_auto[95] = 1
wep_load[95] = 2
wep_cost[95] = 2
wep_sprt[95] = sprWepUzi
wep_area[95] = 4
wep_text[95] = ""
wep_swap[95] = sndSwapMachinegun

wep_name[96] = "UNUSED - EVISCERATOR"
wep_type[96] = 4
wep_auto[96] = 0
wep_load[96] = 170
wep_cost[96] = 15
wep_sprt[96] = sprWepEviscerator
wep_area[96] = -1
wep_text[96] = "having trouble aiming?"
wep_swap[96] = sndSwapMachinegun

wep_name[97] = "BONE BREAKER"
wep_type[97] = 0
wep_auto[97] = 0
wep_load[97] = 30
wep_cost[97] = 0
wep_sprt[97] = sprWepBoneBreaker
wep_area[97] = 4
wep_text[97] = "hammer time"
wep_swap[97] = sndSwapHammer

wep_name[98] = "CLEAVER"
wep_type[98] = 0
wep_auto[98] = 0
wep_load[98] = 10
wep_cost[98] = 0
wep_sprt[98] = sprWepMace
wep_area[98] = -1
wep_text[98] = ""
wep_swap[98] = sndSwapHammer

wep_name[99] = "PLASMA SHOVEL"
wep_type[99] = 5
wep_auto[99] = 0
wep_load[99] = 12
wep_cost[99] = 5
wep_sprt[99] = sprWepPlasmaShovel
wep_area[99] = 8
wep_text[99] = ""
wep_swap[99] = sndSwapEnergy

wep_name[100] = "PLASMA HAMMER"
wep_type[100] = 5
wep_auto[100] = 0
wep_load[100] = 12
wep_cost[100] = 5
wep_sprt[100] = sprWepPlasmaHammer
wep_area[100] = 14
wep_text[100] = ""
wep_swap[100] = sndSwapEnergy

wep_name[101] = "PLASMA SCREWDRIVER"
wep_type[101] = 5
wep_auto[101] = 0
wep_load[101] = 10
wep_cost[101] = 2
wep_sprt[101] = sprWepPlasmaScrewdriver
wep_area[101] = 11
wep_text[101] = ""
wep_swap[101] = sndSwapEnergy

wep_name[102] = "RICOCHET GUN"
wep_type[102] = 1
wep_auto[102] = 1
wep_load[102] = 1
wep_cost[102] = 2
wep_sprt[102] = sprWepRicochetGun
wep_area[102] = 6
wep_text[102] = "pop pop pop pop"
wep_swap[102] = sndSwapPistol

wep_name[103] = "TRIPLE RICOCHET GUN"
wep_type[103] = 1
wep_auto[103] = 1
wep_load[103] = 1
wep_cost[103] = 6
wep_sprt[103] = sprWepTripleRicochetGun
wep_area[103] = 8
wep_text[103] = "pop pop pop pop#pop pop pop pop pop#pop pop pop pop"
wep_swap[103] = sndSwapPistol

wep_name[104] = "RICOCHET CANNON"
wep_type[104] = 1
wep_auto[104] = 1
wep_load[104] = 10
wep_cost[104] = 12
wep_sprt[104] = sprWepRicochetCannon
wep_area[104] = 10
wep_text[104] = "pop."
wep_swap[104] = sndSwapPistol

wep_name[105] = "WRECKER"
wep_type[105] = 4
wep_auto[105] = 0
wep_load[105] = 50
wep_cost[105] = 5
wep_sprt[105] = sprWepWrecker
wep_area[105] = 6
wep_text[105] = "pew pew pew"
wep_swap[105] = sndSwapPistol

wep_name[106] = "AUTO FLARE BOW"
wep_type[106] = 4
wep_auto[106] = 1
wep_load[106] = 12
wep_cost[106] = 1
wep_sprt[106] = sprWepAutoFlareBow
wep_area[106] = 8
wep_text[106] = "fight fire with fire"
wep_swap[106] = sndSwapBow

wep_name[107] = "SUPER FLARE BOW"
wep_type[107] = 4
wep_auto[107] = 0
wep_load[107] = 30
wep_cost[107] = 5
wep_sprt[107] = sprWepSuperFlareBow
wep_area[107] = 10
wep_text[107] = "that dps though"
wep_swap[107] = sndSwapBow

wep_name[108] = "BUG ZAPPER"
wep_type[108] = 5
wep_auto[108] = 0
wep_load[108] = 12
wep_cost[108] = 3
wep_sprt[108] = sprWepBugZapper
wep_area[108] = 6
wep_text[108] = ""
wep_swap[108] = sndSwapEnergy

///ULTRA WEAPONS
///ULTRA WEAPONS
///ULTRA WEAPONS
///ULTRA WEAPONS
///ULTRA WEAPONS
///ULTRA WEAPONS
///ULTRA WEAPONS
wep_name[109] = "ULTRA DISC GUN"
wep_type[109] = 3
wep_auto[109] = 1
wep_load[109] = 3
wep_cost[109] = 1
wep_sprt[109] = sprWepUltraDiscGun
wep_area[109] = 23
wep_text[109] = "b-but its risk vs reward"
wep_swap[109] = sndSwapPistol


///SHIELD WEAPONS
///SHIELD WEAPONS
///SHIELD WEAPONS
///SHIELD WEAPONS
///SHIELD WEAPONS
///SHIELD WEAPONS
///SHIELD WEAPONS
wep_name[110] = "CROSSBOW SHIELD"
wep_type[110] = 3
wep_auto[110] = 0
wep_load[110] = 30
wep_cost[110] = 10
wep_sprt[110] = sprWepShieldedCrossBow
wep_area[110] = 19
wep_text[110] = "you feel protected"
wep_swap[110] = sndSwapBow

wep_name[111] = "REVOLVER SHIELD"
wep_type[111] = 1
wep_auto[111] = 0
wep_load[111] = 20
wep_cost[111] = 20
wep_sprt[111] = sprWepRevolverShield
wep_area[111] = 20
wep_text[111] = "you feel protected"
wep_swap[111] = sndSwapPistol

wep_name[112] = "BAZOOKA SHIELD"
wep_type[112] = 4
wep_auto[112] = 0
wep_load[112] = 60
wep_cost[112] = 10
wep_sprt[112] = sprWepBazookaShield
wep_area[112] = 21
wep_text[112] = "you feel protected?"
wep_swap[112] = sndSwapExplosive

wep_name[113] = "PLASMA GUN SHIELD"
wep_type[113] = 5
wep_auto[113] = 0
wep_load[113] = 60
wep_cost[113] = 15
wep_sprt[113] = sprWepPlasmaShield
wep_area[113] = 20
wep_text[113] = "you feel protected"
wep_swap[113] = sndSwapEnergy

wep_name[114] = "DISC SHIELD"
wep_type[114] = 3
wep_auto[114] = 0
wep_load[114] = 10
wep_cost[114] = 20
wep_sprt[114] = sprWepDiscShield
wep_area[114] = 18
wep_text[114] = "this isnt a good idea"
wep_swap[114] = sndSwapBow

wep_name[115] = "FLAK SHIELD"
wep_type[115] = 2
wep_auto[115] = 0
wep_load[115] = 50
wep_cost[115] = 20
wep_sprt[115] = sprWepFlakShield
wep_area[115] = 18
wep_text[115] = ""
wep_swap[115] = sndSwapShotgun

wep_name[116] = "SLUGGER SHIELD"
wep_type[116] = 2
wep_auto[116] = 0
wep_load[116] = 80
wep_cost[116] = 20
wep_sprt[116] = sprWepSluggerShield
wep_area[116] = 19
wep_text[116] = ""
wep_swap[116] = sndSwapShotgun

wep_name[117] = "RICOCHET SHIELD"
wep_type[117] = 1
wep_auto[117] = 1
wep_load[117] = 3
wep_cost[117] = 3
wep_sprt[117] = sprWepRicochetShield
wep_area[117] = 20
wep_text[117] = "pop pop pop pop pop"
wep_swap[117] = sndSwapPistol

wep_name[118] = "GRENADE SHIELD"
wep_type[118] = 4
wep_auto[118] = 0
wep_load[118] = 60
wep_cost[118] = 12
wep_sprt[118] = sprWepGrenadeShield
wep_area[118] = 17
wep_text[118] = "boom"
wep_swap[118] = sndSwapExplosive

wep_name[119] = "MINI GRENADE SHIELD"
wep_type[119] = 4
wep_auto[119] = 0
wep_load[119] = 30
wep_cost[119] = 8
wep_sprt[119] = sprWepMiniGrenadeShield
wep_area[119] = 9
wep_text[119] = "boom"
wep_swap[119] = sndSwapExplosive

wep_name[120] = "FLAMETHROWER SHIELD"
wep_type[120] = 4
wep_auto[120] = 1
wep_load[120] = 2
wep_cost[120] = 4
wep_sprt[120] = sprWepFlamethrowerShield
wep_area[120] = 18
wep_text[120] = "set the world afire"
wep_swap[120] = sndSwapExplosive

wep_name[121] = "SHOTGUN SHIELD"
wep_type[121] = 2
wep_auto[121] = 0
wep_load[121] = 60
wep_cost[121] = 24
wep_sprt[121] = sprWepShotgunShield
wep_area[121] = 19
wep_text[121] = "me shoot in yuo"
wep_swap[121] = sndSwapShotgun

wep_name[122] = "FLARE SHIELD"
wep_type[122] = 4
wep_auto[122] = 0
wep_load[122] = 60
wep_cost[122] = 12
wep_sprt[122] = sprWepFlareShield
wep_area[122] = 17
wep_text[122] = "tastes like burning"
wep_swap[122] = sndSwapShotgun

wep_name[123] = "JACKHAMMER SHIELD"
wep_type[123] = 4
wep_auto[123] = 0
wep_load[123] = 80
wep_cost[123] = 24
wep_sprt[123] = sprWepJackhammerShield
wep_area[123] = 17
wep_text[123] = "noisy"
wep_swap[123] = sndSwapExplosive

wep_name[124] = "UNUSED - ARROW SHOTGUN"
wep_type[124] = 3
wep_auto[124] = 0
wep_load[124] = 45
wep_cost[124] = 5
wep_sprt[124] = sprWepArrowShotgun
wep_area[124] = -1
wep_text[124] = ""
wep_swap[124] = sndSwapBow

wep_name[125] = "FLAK RIFLE"
wep_type[125] = 2
wep_auto[125] = 0
wep_load[125] = 50
wep_cost[125] = 3
wep_sprt[125] = sprWepAssaultFlak
wep_area[125] = 10
wep_text[125] = ""
wep_swap[125] = sndSwapShotgun

wep_name[126] = "FLAK PISTOL"
wep_type[126] = 2
wep_auto[126] = 0
wep_load[126] = 10
wep_cost[126] = 1
wep_sprt[126] = sprWepFlakPistol
wep_area[126] = 2
wep_text[126] = "BOING"
wep_swap[126] = sndSwapPistol

wep_name[127] = "FLAK MACHINEGUN"
wep_type[127] = 2
wep_auto[127] = 1
wep_load[127] = 7
wep_cost[127] = 1
wep_sprt[127] = sprWepFlakMachinegun
wep_area[127] = 4
wep_text[127] = "BOING"
wep_swap[127] = sndSwapPistol

wep_name[128] = "FLAK ASSAULT RIFLE"
wep_type[128] = 2
wep_auto[128] = 0
wep_load[128] = 40
wep_cost[128] = 2
wep_sprt[128] = sprWepAssaultFlakRifle
wep_area[128] = 5
wep_text[128] = ""
wep_swap[128] = sndSwapPistol

wep_name[129] = "ARROW CANNON"
wep_type[129] = 3
wep_auto[129] = 0
wep_load[129] = 75
wep_cost[129] = 10
wep_sprt[129] = sprWepArrowCannon
wep_area[129] = 12
wep_text[129] = ""
wep_swap[129] = sndSwapBow

wep_name[130] = "BLUDGEON"
wep_type[130] = 0
wep_auto[130] = 0
wep_load[130] = 50
wep_cost[130] = 0
wep_sprt[130] = sprWepBludgeon
wep_area[130] = -1
wep_text[130] = "smash it"
wep_swap[130] = sndSwapHammer

wep_name[131] = "SUPER NAIL GUN"
wep_type[131] = 3
wep_auto[131] = 0
wep_load[131] = 50
wep_cost[131] = 5
wep_sprt[131] = sprWepSuperNailGun
wep_area[131] = 10
wep_text[131] = "acupuncture was never so fun"
wep_swap[131] = sndSwapPistol

wep_name[132] = "VENOM PISTOL"
wep_type[132] = 1
wep_auto[132] = 0
wep_load[132] = 8
wep_cost[132] = 3
wep_sprt[132] = sprWepVenomPistol
wep_area[132] = 2
wep_text[132] = "cough cough"
wep_swap[132] = sndSwapPistol

wep_name[133] = "VENOM MACHINEGUN"
wep_type[133] = 1
wep_auto[133] = 1
wep_load[133] = 6
wep_cost[133] = 3
wep_sprt[133] = sprWepVenomMachinegun
wep_area[133] = 6
wep_text[133] = "cough COUGH cough"
wep_swap[133] = sndSwapPistol

wep_name[134] = "VENOM ASSAULT RIFLE"
wep_type[134] = 1
wep_auto[134] = 0
wep_load[134] = 50
wep_cost[134] = 9
wep_sprt[134] = sprWepVenomAssaultRifle
wep_area[134] = 6
wep_text[134] = "cough COUGH cough"
wep_swap[134] = sndSwapPistol

wep_name[135] = "VENOM CANNON"
wep_type[135] = 1
wep_auto[135] = 0
wep_load[135] = 60
wep_cost[135] = 40
wep_sprt[135] = sprWepVenomCannon
wep_area[135] = 10
wep_text[135] = "cough COUGH cough"
wep_swap[135] = sndSwapDragon

wep_name[136] = "SPEAR GUN"
wep_type[136] = 3
wep_auto[136] = 0
wep_load[136] = 60
wep_cost[136] = 10
wep_sprt[136] = sprWepSpearGun
wep_area[136] = 7
wep_text[136] = "hunt em"
wep_swap[136] = sndSwapBow

wep_name[137] = "VENOM SHOTGUN"
wep_type[137] = 1
wep_auto[137] = 0
wep_load[137] = 20
wep_cost[137] = 10
wep_sprt[137] = sprWepVenomShotgun
wep_area[137] = 5
wep_text[137] = ""
wep_swap[137] = sndSwapShotgun

wep_name[138] = "PLASMA SHOTGUN"
wep_type[138] = 5
wep_auto[138] = 0
wep_load[138] = 40
wep_cost[138] = 7
wep_sprt[138] = sprWepPlasmaShotgun
wep_area[138] = 10
wep_text[138] = ""
wep_swap[138] = sndSwapEnergy

wep_name[139] = "PLASMA LAUNCHER"
wep_type[139] = 5
wep_auto[139] = 0
wep_load[139] = 60
wep_cost[139] = 10
wep_sprt[139] = sprWepPlasmaLauncher
wep_area[139] = 7
wep_text[139] = ""
wep_swap[139] = sndSwapEnergy

wep_name[140] = "DUAL REVOLVER"
wep_type[140] = 1
wep_auto[140] = 0
wep_load[140] = 6
wep_cost[140] = 2
wep_sprt[140] = sprWepDoubleRevolver
wep_area[140] = -1
wep_text[140] = "trusty double revolver"
wep_swap[140] = sndSwapPistol

wep_name[141] = "REVERSE GUN"
wep_type[141] = 1
wep_auto[141] = 0
wep_load[141] = 6
wep_cost[141] = 2
wep_sprt[141] = sprWepReverseGun
wep_area[141] = 8
wep_text[141] = "!revlover dlo ytsurt"
wep_swap[141] = sndSwapPistol

wep_name[142] = "SUPER PLASMA SHOTGUN"
wep_type[142] = 5
wep_auto[142] = 0
wep_load[142] = 300
wep_cost[142] = 20
wep_sprt[142] = sprWepSuperPlasmaShotgun
wep_area[142] = 15
wep_text[142] = "is fun"
wep_swap[142] = sndSwapEnergy

wep_name[143] = "SHUFFLE GUN"
wep_type[143] = irandom(4)+1
wep_auto[143] = 0
wep_load[143] = 20
wep_cost[143] = irandom(4)+2
wep_sprt[143] = sprWepShuffleGun2
wep_area[143] = 7
wep_text[143] = "is fun"
wep_swap[143] = sndSwapEnergy

wep_name[144] = "HARDRON COLLIDER"
wep_type[144] = 4
wep_auto[144] = 0
wep_load[144] = 150
wep_cost[144] = 26
wep_sprt[144] = sprWepLHC
wep_area[144] = 13
wep_text[144] = "havin' a blast!"
wep_swap[144] = sndSwapExplosive

wep_name[145] = "SKORPION"
wep_type[145] = 1
wep_auto[145] = 1
wep_load[145] = 3
wep_cost[145] = 2
wep_sprt[145] = sprWepSkorpion
wep_area[145] = -1
wep_text[145] = "tttshhhh"
wep_swap[145] = sndSwapExplosive

wep_name[146] = "PIPE"
wep_type[146] = 0
wep_auto[146] = 1
wep_load[146] = 15
wep_cost[146] = 0
wep_sprt[146] = sprWepPipe
wep_area[146] = -1
wep_text[146] = ""
wep_swap[146] = sndSwapHammer

wep_name[147] = "MOLE'S GUN"
wep_type[147] = 2
wep_auto[147] = 0
wep_load[147] = 25
wep_cost[147] = 1
wep_sprt[147] = sprWepMoleGun
wep_area[147] = -1
wep_text[147] = ""
wep_swap[147] = sndSwapShotgun

wep_name[148] = "THE FIREBALLER"
wep_type[148] = 4
wep_auto[148] = 0
wep_load[148] = 35
wep_cost[148] = 3
wep_sprt[148] = sprWepTheFireballer
wep_area[148] = -1
wep_text[148] = ""
wep_swap[148] = sndSwapShotgun

wep_name[149] = "HEAVY REVOLVER"
wep_type[149] = 1
wep_auto[149] = 0
wep_load[149] = 6
wep_cost[149] = 2
wep_sprt[149] = sprWepHeavyRevolver
wep_area[149] = 8
wep_text[149] = ""
wep_swap[149] = sndSwapMachinegun

wep_name[150] = "HEAVY MACHINEGUN"
wep_type[150] = 1
wep_auto[150] = 1
wep_load[150] = 5
wep_cost[150] = 2
wep_sprt[150] = sprWepHeavyMachineGun
wep_area[150] = 9
wep_text[150] = ""
wep_swap[150] = sndSwapMachinegun

wep_name[151] = "HEAVY ASSAULT RIFLE"
wep_type[151] = 1
wep_auto[151] = 0
wep_load[151] = 11
wep_cost[151] = 6
wep_sprt[151] = sprWepHeavyAssaultRifle
wep_area[151] = 10
wep_text[151] = ""
wep_swap[151] = sndSwapMachinegun

wep_name[152] = "SAWED-OFF SHOTGUN"
wep_type[152] = 2
wep_auto[152] = 0
wep_load[152] = 35
wep_cost[152] = 2
wep_sprt[152] = sprWepSawedOffShotgun
wep_area[152] = 6
wep_text[152] = ""
wep_swap[152] = sndSwapShotgun

wep_name[153] = "ERASER"
wep_type[153] = 2
wep_auto[153] = 0
wep_load[153] = 19
wep_cost[153] = 2
wep_sprt[153] = sprWepEraser
wep_area[153] = 7
wep_text[153] = ""
wep_swap[153] = sndSwapShotgun

wep_name[154] = "SUPER FLAK CANNON"
wep_type[154] = 2
wep_auto[154] = 0
wep_load[154] = 65
wep_cost[154] = 8
wep_sprt[154] = sprWepSuperFlakCannon
wep_area[154] = 11
wep_text[154] = ""
wep_swap[154] = sndSwapShotgun

wep_name[155] = "POP GUN"
wep_type[155] = 1
wep_auto[155] = 1
wep_load[155] = 2
wep_cost[155] = 1
wep_sprt[155] = sprWepPopGun
wep_area[155] = 2
wep_text[155] = ""
wep_swap[155] = sndSwapShotgun

wep_name[156] = "POP RIFLE"
wep_type[156] = 1
wep_auto[156] = 1
wep_load[156] = 4
wep_cost[156] = 2
wep_sprt[156] = sprWepPopRifle
wep_area[156] = 3
wep_text[156] = ""
wep_swap[156] = sndSwapShotgun

wep_name[157] = "FLAME SHOTGUN"
wep_type[157] = 2
wep_auto[157] = 0
wep_load[157] = 20
wep_cost[157] = 1
wep_sprt[157] = sprWepFlameShotgun
wep_area[157] = 5
wep_text[157] = ""
wep_swap[157] = sndSwapShotgun

wep_name[158] = "DOUBLE FLAME SHOTGUN"
wep_type[158] = 2
wep_auto[158] = 0
wep_load[158] = 40
wep_cost[158] = 2
wep_sprt[158] = sprWepDoubleFlameShotgun
wep_area[158] = 7
wep_text[158] = ""
wep_swap[158] = sndSwapShotgun

wep_name[159] = "AUTO FLAME SHOTGUN"
wep_type[159] = 2
wep_auto[159] = 1
wep_load[159] = 7
wep_cost[159] = 1
wep_sprt[159] = sprWepFlameShotgun
wep_area[159] = 10
wep_text[159] = ""
wep_swap[159] = sndSwapShotgun

wep_name[160] = "INCINERATOR"
wep_type[160] = 1
wep_auto[160] = 1
wep_load[160] = 2
wep_cost[160] = 3
wep_sprt[160] = sprWepIncinerator
wep_area[160] = 15
wep_text[160] = ""
wep_swap[160] = sndSwapShotgun

wep_name[161] = "BOUNCER SMG"
wep_type[161] = 1
wep_auto[161] = 1
wep_load[161] = 3
wep_cost[161] = 1
wep_sprt[161] = sprWepBouncySMG
wep_area[161] = 5
wep_text[161] = ""
wep_swap[161] = sndSwapMachinegun

wep_name[162] = "BOUNCER SHOTGUN"
wep_type[162] = 1
wep_auto[162] = 1
wep_load[162] = 16
wep_cost[162] = 5
wep_sprt[162] = sprWepBouncyShotgun
wep_area[162] = 15
wep_text[162] = ""
wep_swap[162] = sndSwapMachinegun

wep_name[163] = "HYPER SLUGGER"
wep_type[163] = 2
wep_auto[163] = 0
wep_load[163] = 10
wep_cost[163] = 1
wep_sprt[163] = sprWepHyperSlug
wep_area[163] = 9
wep_text[163] = ""
wep_swap[163] = sndSwapShotgun

wep_name[164] = "HEAVY SLUGGER"
wep_type[164] = 2
wep_auto[164] = 0
wep_load[164] = 16
wep_cost[164] = 2
wep_sprt[164] = sprWepHeavySlugger
wep_area[164] = 7
wep_text[164] = ""
wep_swap[164] = sndSwapShotgun

wep_name[165] = "HEAVY CROSSBOW"
wep_type[165] = 3
wep_auto[165] = 0
wep_load[165] = 45
wep_cost[165] = 2
wep_sprt[165] = sprWepHeavyCrossbow
wep_area[165] = 5
wep_text[165] = ""
wep_swap[165] = sndSwapBow

wep_name[166] = "AUTO HEAVY CROSSBOW"
wep_type[166] = 3
wep_auto[166] = 1
wep_load[166] = 14
wep_cost[166] = 2
wep_sprt[166] = sprWepAutoHeavyCrossbow
wep_area[166] = 12
wep_text[166] = ""
wep_swap[166] = sndSwapBow

wep_name[167] = "SPLINTER PISTOL"
wep_type[167] = 3
wep_auto[167] = 0
wep_load[167] = 10
wep_cost[167] = 1
wep_sprt[167] = sprWepSplinterPistol
wep_area[167] = 5
wep_text[167] = ""
wep_swap[167] = sndSwapBow

wep_name[168] = "SUPER SPLINTER GUN"
wep_type[168] = 3
wep_auto[168] = 0
wep_load[168] = 29
wep_cost[168] = 2
wep_sprt[168] = sprWepSuperSplinterGun
wep_area[168] = 8
wep_text[168] = ""
wep_swap[168] = sndSwapBow

wep_name[169] = "HEAVY GRENADE LAUNCHER"
wep_type[169] = 4
wep_auto[169] = 0
wep_load[169] = 28
wep_cost[169] = 2
wep_sprt[169] = sprWepHeavyGrenadeLauncher
wep_area[169] = 9
wep_text[169] = ""
wep_swap[169] = sndSwapExplosive

wep_name[170] = "CLUSTER LAUNCHER"
wep_type[170] = 4
wep_auto[170] = 0
wep_load[170] = 28
wep_cost[170] = 2
wep_sprt[170] = sprWepClusterLauncher
wep_area[170] = 8
wep_text[170] = ""
wep_swap[170] = sndSwapExplosive

wep_name[171] = "GATLING BAZOOKA"
wep_type[171] = 4
wep_auto[171] = 1
wep_load[171] = 10
wep_cost[171] = 1
wep_sprt[171] = sprWepGatlingBazooka
wep_area[171] = 11
wep_text[171] = ""
wep_swap[171] = sndSwapExplosive

wep_name[172] = "SUPER BAZOOKA"
wep_type[172] = 4
wep_auto[172] = 0
wep_load[172] = 40
wep_cost[172] = 5
wep_sprt[172] = sprWepSuperBazooka
wep_area[172] = 11
wep_text[172] = ""
wep_swap[172] = sndSwapExplosive

wep_name[173] = "BLOOD CANNON"
wep_type[173] = 4
wep_auto[173] = 1
wep_load[173] = 18
wep_cost[173] = 2
wep_sprt[173] = sprWepBloodCannon
wep_area[173] = 11
wep_text[173] = ""
wep_swap[173] = sndSwapExplosive

wep_name[174] = "FLAME CANNON"
wep_type[174] = 4
wep_auto[174] = 0
wep_load[174] = 85
wep_cost[174] = 4
wep_sprt[174] = sprWepFlameCannon
wep_area[174] = 9
wep_text[174] = ""
wep_swap[174] = sndSwapExplosive

wep_name[174] = "FLAME CANNON"
wep_type[174] = 4
wep_auto[174] = 0
wep_load[174] = 85
wep_cost[174] = 4
wep_sprt[174] = sprWepFlameCannon
wep_area[174] = 9
wep_text[174] = ""
wep_swap[174] = sndSwapExplosive

wep_name[175] = "PLASMA RIFLE"
wep_type[175] = 5
wep_auto[175] = 1
wep_load[175] = 10
wep_cost[175] = 2
wep_sprt[175] = sprWepPlasmaRifle
wep_area[175] = 6
wep_text[175] = ""
wep_swap[175] = sndSwapEnergy

wep_name[176] = "PLASMA MINIGUN"
wep_type[176] = 5
wep_auto[176] = 1
wep_load[176] = 3
wep_cost[176] = 2
wep_sprt[176] = sprWepPlasmaMinigun
wep_area[176] = 12
wep_text[176] = ""
wep_swap[176] = sndSwapEnergy

wep_name[177] = "LIGHTNING SMG"
wep_type[177] = 5
wep_auto[177] = 1
wep_load[177] = 8
wep_cost[177] = 1
wep_sprt[177] = sprWepLightningSMG
wep_area[177] = 8
wep_text[177] = ""
wep_swap[177] = sndSwapEnergy

wep_name[178] = "LIGHTNING CANNON"
wep_type[178] = 5
wep_auto[178] = 0
wep_load[178] = 41
wep_cost[178] = 8
wep_sprt[178] = sprWepLightningCannon
wep_area[178] = 12
wep_text[178] = ""
wep_swap[178] = sndSwapEnergy

wep_name[179] = "BLOOD HAMMER"
wep_type[179] = 0
wep_auto[179] = 0
wep_load[179] = 25
wep_cost[179] = 0
wep_sprt[179] = sprWepBloodHammer
wep_area[179] = 9
wep_text[179] = ""
wep_swap[179] = sndSwapHammer

wep_name[180] = "LIGHTNING HAMMER"
wep_type[180] = 0
wep_auto[180] = 0
wep_load[180] = 33
wep_cost[180] = 0
wep_sprt[180] = sprWepLightningHammer
wep_area[180] = 10
wep_text[180] = ""
wep_swap[180] = sndSwapHammer

wep_name[181] = "ULTRA REVOLVER"
wep_type[181] = 1
wep_auto[181] = 1
wep_load[181] = 3
wep_cost[181] = 2
wep_sprt[181] = sprWepUltraRevolver
wep_area[181] = 23
wep_text[181] = ""
wep_swap[181] = sndSwapPistol

wep_name[182] = "ULTRA SHOTGUN"
wep_type[182] = 2
wep_auto[182] = 0
wep_load[182] = 12
wep_cost[182] = 3
wep_sprt[182] = sprWepUltraShotgun
wep_area[182] = 23
wep_text[182] = ""
wep_swap[182] = sndSwapShotgun

wep_name[183] = "SEEKER PISTOL"
wep_type[183] = 3
wep_auto[183] = 0
wep_load[183] = 16
wep_cost[183] = 1
wep_sprt[183] = sprWepSeekerPistol
wep_area[183] = 5
wep_text[183] = ""
wep_swap[183] = sndSwapBow

wep_name[184] = "SEEKER SHOTGUN"
wep_type[184] = 3
wep_auto[184] = 0
wep_load[184] = 23
wep_cost[184] = 3
wep_sprt[184] = sprWepSeekerShotgun
wep_area[184] = 7
wep_text[184] = ""
wep_swap[184] = sndSwapBow

wep_name[185] = "ULTRA CROSSBOW"
wep_type[185] = 3
wep_auto[185] = 0
wep_load[185] = 12
wep_cost[185] = 1
wep_sprt[185] = sprWepUltraCrossbow
wep_area[185] = 23
wep_text[185] = ""
wep_swap[185] = sndSwapBow

wep_name[186] = "ULTRA GRENADE LAUNCHER"
wep_type[186] = 4
wep_auto[186] = 0
wep_load[186] = 16
wep_cost[186] = 1
wep_sprt[186] = sprWepUltraGrenadeLauncher
wep_area[186] = 23
wep_text[186] = ""
wep_swap[186] = sndSwapExplosive

wep_name[187] = "ULTRA LASER PISTOL"
wep_type[187] = 5
wep_auto[187] = 0
wep_load[187] = 5
wep_cost[187] = 3
wep_sprt[187] = sprWepUltraLaserGun
wep_area[187] = 23
wep_text[187] = ""
wep_swap[187] = sndSwapEnergy

wep_name[188] = "ULTRA SHOVEL"
wep_type[188] = 0
wep_auto[188] = 0
wep_load[188] = 15
wep_cost[188] = 0
wep_sprt[188] = sprWepUltraShovel
wep_area[188] = 23
wep_text[188] = ""
wep_swap[188] = sndSwapHammer

wep_name[189] = "GRENADE SHOTGUN"
wep_type[189] = 4
wep_auto[189] = 0
wep_load[189] = 17
wep_cost[189] = 1
wep_sprt[189] = sprWepGrenadeShotty
wep_area[189] = 6 
wep_text[189] = ""
wep_swap[189] = sndSwapExplosive

wep_name[190] = "AUTO GRENADE SHOTGUN"
wep_type[190] = 4
wep_auto[190] = 1
wep_load[190] = 6
wep_cost[190] = 1
wep_sprt[190] = sprWepAutoGrenadeShotty
wep_area[190] = 13 
wep_text[190] = ""
wep_swap[190] = sndSwapExplosive

wep_name[191] = "BLOOD SHOVEL"
wep_type[191] = 0
wep_auto[191] = 0
wep_load[191] = 15
wep_cost[191] = 0
wep_sprt[191] = sprWepBloodShovel
wep_area[191] = 12 
wep_text[191] = ""
wep_swap[191] = sndSwapHammer

wep_name[192] = "AUTO ERASER"
wep_type[192] = 2
wep_auto[192] = 1
wep_load[192] = 10
wep_cost[192] = 2
wep_sprt[192] = sprWepAutoEraser
wep_area[192] = 12 
wep_text[192] = ""
wep_swap[192] = sndSwapShotgun

wep_name[193] = "TRIPLE POP GUN"
wep_type[193] = 1
wep_auto[193] = 1
wep_load[193] = 3
wep_cost[193] = 3
wep_sprt[193] = sprWepTriplePopGun
wep_area[193] = 8
wep_text[193] = ""
wep_swap[193] = sndSwapShotgun

wep_name[194] = "AUTO BOUNCER SHOTGUN"
wep_type[194] = 1
wep_auto[194] = 1
wep_load[194] = 10
wep_cost[194] = 5
wep_sprt[194] = sprWepAutoBouncerShotgun
wep_area[194] = 12
wep_text[194] = ""
wep_swap[194] = sndSwapShotgun

wep_name[195] = "BOUNCER MINIGUN"
wep_type[195] = 1
wep_auto[195] = 1
wep_load[195] = 1
wep_cost[195] = 1
wep_sprt[195] = sprWepBouncerMinigun
wep_area[195] = 12
wep_text[195] = ""
wep_swap[195] = sndSwapShotgun

wep_name[196] = "HEAVY GATLING SLUGGER"
wep_type[196] = 2
wep_auto[196] = 1
wep_load[196] = 7
wep_cost[196] = 2
wep_sprt[196] = sprWepHeavyGatlingSlugger
wep_area[196] = 21
wep_text[196] = ""
wep_swap[196] = sndSwapShotgun

wep_name[197] = "HEAVY ASSAULT SLUGGER"
wep_type[197] = 2
wep_auto[197] = 0
wep_load[197] = 40
wep_cost[197] = 6
wep_sprt[197] = sprWepHeavyAssaultSlugger
wep_area[197] = 20
wep_text[197] = ""
wep_swap[197] = sndSwapShotgun

wep_name[198] = "HEAVY MINIGUN"
wep_type[198] = 1
wep_auto[198] = 1
wep_load[198] = 1
wep_cost[198] = 2
wep_sprt[198] = sprWepHeavyMinigun
wep_area[198] = 13
wep_text[198] = ""
wep_swap[198] = sndSwapMachinegun

wep_name[199] = "HEAVY TRIPLE MACHINEGUN"
wep_type[199] = 1
wep_auto[199] = 1
wep_load[199] = 4
wep_cost[199] = 6
wep_sprt[199] = sprWepHeavyTripleMachinegun
wep_area[199] = 13
wep_text[199] = ""
wep_swap[199] = sndSwapMachinegun

wep_name[200] = "SOUL DEVOURER"
wep_type[200] = 1
wep_auto[200] = 0
wep_load[200] = 120
wep_cost[200] = 20
wep_sprt[200] = sprWepSoulDevourer
wep_area[200] = 13
wep_text[200] = "so edgy"
wep_swap[200] = sndSwapMachinegun

wep_name[201] = "FLAME POP GUN"
wep_type[201] = 1
wep_auto[201] = 1
wep_load[201] = 3
wep_cost[201] = 1
wep_sprt[201] = sprWepFlamePopGun
wep_area[201] = 10
wep_text[201] = "ow!"
wep_swap[201] = sndSwapMachinegun

wep_name[202] = "BLOOD CLUSTER LAUNCHER"
wep_type[202] = 4
wep_auto[202] = 1
wep_load[202] = 30
wep_cost[202] = 2
wep_sprt[202] = sprWepClusterBloodLauncher
wep_area[202] = 10
wep_text[202] = "bloody"
wep_swap[202] = sndSwapMachinegun

wep_name[203] = "BOUNCER CANNON"
wep_type[203] = 1
wep_auto[203] = 0
wep_load[203] = 50
wep_cost[203] = 10
wep_sprt[203] = sprWepBouncerCannon
wep_area[203] = 7
wep_text[203] = "pew pew pew"
wep_swap[203] = sndSwapMachinegun

wep_name[204] = "TRIPLE ERASER"
wep_type[204] = 2
wep_auto[204] = 0
wep_load[204] = 50
wep_cost[204] = 3
wep_sprt[204] = sprWepTripleEraser
wep_area[204] = 14
wep_text[204] = "kill stuff"
wep_swap[204] = sndSwapMachinegun

wep_name[205] = "HEAVY BULLET SHOTGUN"
wep_type[205] = 1
wep_auto[205] = 0
wep_load[205] = 40
wep_cost[205] = 5
wep_sprt[205] = sprWepHeavyBulletShotgun
wep_area[205] = 8
wep_text[205] = ""
wep_swap[205] = sndSwapMachinegun

wep_name[206] = "AUTO HEAVY BULLET SHOTGUN"
wep_type[206] = 1
wep_auto[206] = 1
wep_load[206] = 7
wep_cost[206] = 5
wep_sprt[206] = sprWepAutoHeavyBulletShotgun
wep_area[206] = 15
wep_text[206] = ""
wep_swap[206] = sndSwapMachinegun

wep_name[207] = "BLASTER"
wep_type[207] = 4
wep_auto[207] = 1
wep_load[207] = 10
wep_cost[207] = 2
wep_sprt[207] = sprWepBlaster
wep_area[207] = 12
wep_text[207] = ""
wep_swap[207] = sndSwapMachinegun

wep_name[208] = "BLASTER RIFLE"
wep_type[208] = 4
wep_auto[208] = 1
wep_load[208] = 8
wep_cost[208] = 1
wep_sprt[208] = sprWepBlasterRifle
wep_area[208] = 8
wep_text[208] = ""
wep_swap[208] = sndSwapMachinegun

wep_name[209] = "TRIPLE BLASTER RIFLE"
wep_type[209] = 4
wep_auto[209] = 1
wep_load[209] = 8
wep_cost[209] = 3
wep_sprt[209] = sprWepTripleBlasterRifle
wep_area[209] = 13
wep_text[209] = ""
wep_swap[209] = sndSwapMachinegun

wep_name[210] = "ATOM BOMB LAUNCHER"
wep_type[210] = 4
wep_auto[210] = 0
wep_load[210] = 90
wep_cost[210] = 24
wep_sprt[210] = sprWepAtomBombLauncher
wep_area[210] = 18
wep_text[210] = ""
wep_swap[210] = sndSwapExplosive

wep_name[211] = "LIGHTNING MINIGUN"
wep_type[211] = 5
wep_auto[211] = 1
wep_load[211] = 2
wep_cost[211] = 1
wep_sprt[211] = sprWepLightningMinigun
wep_area[211] = 12
wep_text[211] = ""
wep_swap[211] = sndSwapEnergy

wep_name[212] = "DOUBLE AUTO SHOTGUN"
wep_type[212] = 2
wep_auto[212] = 1
wep_load[212] = 12
wep_cost[212] = 2
wep_sprt[212] = sprWepDoubleAutoShotgun
wep_area[212] = 12
wep_text[212] = ""
wep_swap[212] = sndSwapShotgun

wep_name[213] = "NAPALM LAUNCHER"
wep_type[213] = 4
wep_auto[213] = 0
wep_load[213] = 30
wep_cost[213] = 8
wep_sprt[213] = sprWepNapalmLauncher
wep_area[213] = 8
wep_text[213] = ""
wep_swap[213] = sndSwapExplosive

wep_name[214] = "SPLINTER MINIGUN"
wep_type[214] = 3
wep_auto[214] = 1
wep_load[214] = 2
wep_cost[214] = 1
wep_sprt[214] = sprWepSplinterMinigun
wep_area[214] = 16
wep_text[214] = ""
wep_swap[214] = sndSwapBow

wep_name[215] = "GATLING SUPER SLUGGER"
wep_type[215] = 2
wep_auto[215] = 1
wep_load[215] = 12
wep_cost[215] = 5
wep_sprt[215] = sprWepGatlingSuperSlugger
wep_area[215] = 13
wep_text[215] = ""
wep_swap[215] = sndSwapShotgun

wep_name[216] = "HEAVY SUPER SLUGGER"
wep_type[216] = 2
wep_auto[216] = 0
wep_load[216] = 20
wep_cost[216] = 10
wep_sprt[216] = sprWepHeavySuperSlugger
wep_area[216] = 10
wep_text[216] = ""
wep_swap[216] = sndSwapShotgun

wep_name[217] = "AUTO FLAK CANNON"
wep_type[217] = 2
wep_auto[217] = 1
wep_load[217] = 14
wep_cost[217] = 2
wep_sprt[217] = sprWepAutoFlakCannon
wep_area[217] = 8
wep_text[217] = ""
wep_swap[217] = sndSwapShotgun

wep_name[218] = "IDPD GRENADE LAUNCHER"
wep_type[218] = 4
wep_auto[218] = 0
wep_load[218] = 90
wep_cost[218] = 5
wep_sprt[218] = sprWepPopoNader
wep_area[218] = -1
wep_text[218] = "popo"
wep_swap[218] = sndSwapExplosive

wep_name[219] = "DOUBLE REVOLVER"
wep_type[219] = 1
wep_auto[219] = 0
wep_load[219] = 5
wep_cost[219] = 2
wep_sprt[219] = sprWepDoubleRevolvers
wep_area[219] = -1
wep_text[219] = "trusty double revolver"
wep_swap[219] = sndSwapPistol

wep_name[220] = "HEAVY GATLING SLUGGER"
wep_type[220] = 2
wep_auto[220] = 1
wep_load[220] = 8
wep_cost[220] = 2
wep_sprt[220] = sprWepHeavyGatlingSlugger
wep_area[220] = 16
wep_text[220] = "love"
wep_swap[220] = sndSwapPistol

wep_name[221] = "PORTAL GRENADE"
wep_type[221] = 4
wep_auto[221] = 0
wep_load[221] = 1
wep_cost[221] = 2
wep_sprt[221] = sprWepPortalGrenade
wep_area[221] = -1
wep_text[221] = "die"
wep_swap[221] = sndSwapPistol

wep_name[222] = "SEEKER MINIGUN"
wep_type[222] = 3
wep_auto[222] = 1
wep_load[222] = 1
wep_cost[222] = 1
wep_sprt[222] = sprWepSeekerMinigun
wep_area[222] = 15
wep_text[222] = "YES"
wep_swap[222] = sndSwapMachinegun

wep_name[223] = "SPADE"
wep_type[223] = 0
wep_auto[223] = 0
wep_load[223] = 30
wep_cost[223] = 0
wep_sprt[223] = sprWepSpade
wep_area[223] = 7
wep_text[223] = "all i want"
wep_swap[223] = sndSwapHammer

wep_name[224] = "DESTROYER"
wep_type[224] = 5
wep_auto[224] = 1
wep_load[224] = 2
wep_cost[224] = 2
wep_sprt[224] = sprWepDestroyer
wep_area[224] = 9
wep_text[224] = "destroy stuff"
wep_swap[224] = sndSwapHammer

wep_name[225] = "EVERYTHING SHOVEL"
wep_type[225] = 0
wep_auto[225] = 0
wep_load[225] = 20
wep_cost[225] = 0
wep_sprt[225] = sprWepEverythingShovel
wep_area[225] = -1
wep_text[225] = "TYSECHEWHWESAHAEWHgqGGSEUEIEJYYRI5EHHTHYREEHWHYEDXVDDE5K57KU5RWDGGRQWRHTJENRESEJERYIE5I"
wep_swap[225] = sndSwapHammer

wep_name[226] = "CORPSE CANNON"
wep_type[226] = 4
wep_auto[226] = 0
wep_load[226] = 4
wep_cost[226] = 1
wep_sprt[226] = sprWepCorpseCannon
wep_area[226] = 6
wep_text[226] = "what a morbid weapon"
wep_swap[226] = sndSwapExplosive

wep_name[227] = "POTATO GUN"
wep_type[227] = 2
wep_auto[227] = 0
wep_load[227] = 2
wep_cost[227] = 1
wep_sprt[227] = sprWepPotatoGun
wep_area[227] = 5
wep_text[227] = "running out of witty comments"
wep_swap[227] = sndSwapExplosive

wep_name[228] = "PLASMA WAVE RIFLE"
wep_type[228] = 5
wep_auto[228] = 1
wep_load[228] = 60
wep_cost[228] = 5
wep_sprt[228] = sprWepPlasmaWaveGun
wep_area[228] = 18
wep_text[228] = "yes"
wep_swap[228] = sndSwapExplosive

wep_name[229] = "BLASTER CANNON"
wep_type[229] = 4
wep_auto[229] = 0
wep_load[229] = 90
wep_cost[229] = 40
wep_sprt[229] = sprWepBlasterCannon
wep_area[229] = 17
wep_text[229] = "yes"
wep_swap[229] = sndSwapExplosive

wep_name[230] = "ENERGY SHOVEL"
wep_type[230] = 5
wep_auto[230] = 0
wep_load[230] = 25
wep_cost[230] = 6
wep_sprt[230] = sprWepEnergyShovel
wep_area[230] = 19
wep_text[230] = "kill"
wep_swap[230] = sndSwapExplosive

wep_name[231] = "PROTO CANNON"
wep_type[231] = 0
wep_auto[231] = 0
wep_load[231] = 15
wep_cost[231] = 0
wep_sprt[231] = sprWepProtoCannon
wep_area[231] = 24
wep_text[231] = "bloop"
wep_swap[231] = sndSwapExplosive

wep_name[232] = "SHRAPNEL RIFLE"
wep_type[232] = 2
wep_auto[232] = 1
wep_load[232] = 10
wep_cost[232] = 1
wep_sprt[232] = sprWepShrapnelRifle
wep_area[232] = 8
wep_text[232] = "pow pow"
wep_swap[232] = sndSwapShotgun

wep_name[233] = "WALL LAUNCHER"
wep_type[233] = 4
wep_auto[233] = 0
wep_load[233] = 25
wep_cost[233] = 1
wep_sprt[233] = sprWepWallLauncher
wep_area[233] = 5
wep_text[233] = "it's like a reverse kool aid man!"
wep_swap[233] = sndSwapShotgun

wep_name[234] = "AUTO BULLET SHOTGUN"
wep_type[234] = 1
wep_auto[234] = 1
wep_load[234] = 8
wep_cost[234] = 5
wep_sprt[234] = sprWepAutoBulletShotty
wep_area[234] = 15
wep_text[234] = "dream"
wep_swap[234] = sndSwapShotgun

wep_name[235] = "BFG 3000"
wep_type[235] = 5
wep_auto[235] = 1
wep_load[235] = 4
wep_cost[235] = 1
wep_sprt[235] = sprWepBFG3000
wep_area[235] = -1
wep_text[235] = "meet your doom"
wep_swap[235] = sndSwapEnergy

wep_name[236] = "ULTRA PLASMA GUN"
wep_type[236] = 5
wep_auto[236] = 1
wep_load[236] = 12
wep_cost[236] = 2
wep_sprt[236] = sprWepUltraPlasmaGun
wep_area[236] = 25
wep_text[236] = "ultra fun"
wep_swap[236] = sndSwapEnergy

wep_name[237] = "SUPER GATLING ASSAULT SEEKER FLAME HEAVY CANNON LASER ENERGY BAZOOKA RIFLE"
wep_type[237] = 4
wep_auto[237] = 0
wep_load[237] = 60
wep_cost[237] = 10
wep_sprt[237] = sprWepSGASFHCFLEBazookaRifleCannon
wep_area[237] = 43
wep_text[237] = "trusty#SUPER GATLING ASSAULT SEEKER FLAME HEAVY CANNON LASER ENERGY BAZOOKA RIFLE"
wep_swap[237] = sndSwapEnergy

wep_name[238] = "HEAVY SHOTGUN"
wep_type[238] = 2
wep_auto[238] = 0
wep_load[238] = 15
wep_cost[238] = 2
wep_sprt[238] = sprWepHeavyShotgun
wep_area[238] = 9
wep_text[238] = "beware of recoil"
wep_swap[238] = sndSwapShotgun

wep_name[239] = "AUTO HEAVY SHOTGUN"
wep_type[239] = 2
wep_auto[239] = 1
wep_load[239] = 8
wep_cost[239] = 2
wep_sprt[239] = sprWepAutoHeavyShotgun
wep_area[239] = 20
wep_text[239] = ""
wep_swap[239] = sndSwapShotgun

wep_name[240] = "DOUBLE HEAVY SHOTGUN"
wep_type[240] = 2
wep_auto[240] = 0
wep_load[240] = 25
wep_cost[240] = 4
wep_sprt[240] = sprWepDoubleHeavyShotgun
wep_area[240] = 12
wep_text[240] = ""
wep_swap[240] = sndSwapShotgun

wep_name[241] = "HEAVY ERASER"
wep_type[241] = 2
wep_auto[241] = 0
wep_load[241] = 10
wep_cost[241] = 4
wep_sprt[241] = sprWepHeavyEraser
wep_area[241] = 12
wep_text[241] = ""
wep_swap[241] = sndSwapShotgun

wep_name[242] = "AUTO HEAVY ERASER"
wep_type[242] = 2
wep_auto[242] = 1
wep_load[242] = 7
wep_cost[242] = 4
wep_sprt[242] = sprWepAutoHeavyEraser
wep_area[242] = 19
wep_text[242] = ""
wep_swap[242] = sndSwapShotgun

wep_name[243] = "FLAME FLAK CANNON"
wep_type[243] = 2
wep_auto[243] = 0
wep_load[243] = 45
wep_cost[243] = 4
wep_sprt[243] = sprWepFlameFlakCannon
wep_area[243] = 6
wep_text[243] = ""
wep_swap[243] = sndSwapShotgun

wep_name[244] = "SUPER FLAME FLAK CANNON"
wep_type[244] = 2
wep_auto[244] = 0
wep_load[244] = 65
wep_cost[244] = 8
wep_sprt[244] = sprWepSuperFlameFlakCannon
wep_area[244] = 10
wep_text[244] = ""
wep_swap[244] = sndSwapShotgun

wep_name[245] = "DEVASTATOR"
wep_type[245] = 5
wep_auto[245] = 0
wep_load[245] = 40
wep_cost[245] = 4
wep_sprt[245] = sprWepDevastator
wep_area[245] = 13
wep_text[245] = ""
wep_swap[245] = sndSwapEnergy

wep_name[246] = "SUPER DEVASTATOR"
wep_type[246] = 5
wep_auto[246] = 0
wep_load[246] = 30
wep_cost[246] = 8
wep_sprt[246] = sprWepSuperDevastator
wep_area[246] = 15
wep_text[246] = ""
wep_swap[246] = sndSwapEnergy

wep_name[247] = "DOUBLE MINIGUN"
wep_type[247] = 1
wep_auto[247] = 1
wep_load[247] = 1
wep_cost[247] = 2
wep_sprt[247] = sprWepDoubleMinigun
wep_area[247] = 13
wep_text[247] = ""
wep_swap[247] = sndSwapMachinegun

wep_name[248] = "OCTUPLE SHOTGUN"
wep_type[248] = 2
wep_auto[248] = 0
wep_load[248] = 70
wep_cost[248] = 8
wep_sprt[248] = sprWepOctupleShotgun
wep_area[248] = 9
wep_text[248] = "octo fun!"
wep_swap[248] = sndSwapShotgun

wep_name[249] = "SUPER FLAME CANNON"
wep_type[249] = 4
wep_auto[249] = 0
wep_load[249] = 200
wep_cost[249] = 20
wep_sprt[249] = sprWepSuperFlameCannon
wep_area[249] = 18
wep_text[249] = "burn baby burn"
wep_swap[249] = sndSwapExplosive

wep_name[250] = "SUPER BLOOD CANNON"
wep_type[250] = 4
wep_auto[250] = 0
wep_load[250] = 200
wep_cost[250] = 7
wep_sprt[250] = sprWepSuperBloodCannon
wep_area[250] = 16
wep_text[250] = "paint the walls red"
wep_swap[250] = sndSwapExplosive

wep_name[251] = "HAND CANNON"
wep_type[251] = 1
wep_auto[251] = 0
wep_load[251] = 6
wep_cost[251] = 5
wep_sprt[251] = sprWepHandCannon
wep_area[251] = 6
wep_text[251] = ""
wep_swap[251] = sndSwapShotgun

wep_name[252] = "HEAVY HAND CANNON"
wep_type[252] = 1
wep_auto[252] = 0
wep_load[252] = 5
wep_cost[252] = 10
wep_sprt[252] = sprWepHeavyHandCannon
wep_area[252] = 12
wep_text[252] = ""
wep_swap[252] = sndSwapShotgun

maxwep = 252

//0 = melee 1 = bullets 2 = shells 3 = bolts 4 = explosives
typ_ammo[0] = 333 typ_ammo[1] = 32 typ_ammo[2] = 8 typ_ammo[3] = 7 typ_ammo[4] = 6 typ_ammo[5] = 10

if instance_exists(Player){ if Player.race = 1{
typ_ammo[1] = 40 typ_ammo[2] = 10 typ_ammo[3] = 9 typ_ammo[4] = 8 typ_ammo[5] = 13}}

if instance_exists(Player){
typ_amax[0] = 1000 typ_amax[1] = 255+Player.skill_got[10]*300 typ_amax[2] = 55+Player.skill_got[10]*44 typ_amax[3] = 55+Player.skill_got[10]*44 typ_amax[4] = 55+Player.skill_got[10]*44 typ_amax[5] = 55+Player.skill_got[10]*44}
else{
typ_amax[0] = 1000 typ_amax[1] = 255 typ_amax[2] = 55 typ_amax[3] = 55 typ_amax[4] = 55 typ_amax[5] = 55}

typ_name[0] = "MELEE" typ_name[1] = "BULLETS" typ_name[2] = "SHELLS" typ_name[3] = "BOLTS" typ_name[4] = "EXPLOSIVES" typ_name[5] = "ENERGY"
