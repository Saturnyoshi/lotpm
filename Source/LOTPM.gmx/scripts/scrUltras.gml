ultra_name[0] = ""
ultra_text[0] = ""
ultra_msnd[0] = sndMut
ultra_tips[0] = "" //IGNORE ALL OF THESE GOD DAMN TIPS THEY'RE POINTLESS

ultra_name[1] = "CONFISCATE"
ultra_text[1] = "ENEMIES HAVE A CHANCE OF DROPPING#ALL KINDS OF CHESTS"
ultra_msnd[1] = sndMut
ultra_tips[1] = "just one more day"

ultra_name[2] = "GUN WARRANT"
ultra_text[2] = "SHOOTING DOESN'T COST AMMO FOR A#FEW SECONDS AFTER SPAWNING"
ultra_msnd[2] = sndMut
ultra_tips[2] = "run forever"

ultra_name[3] = "PAY RAISE"
ultra_text[3] = "WEAKER, BOUNTIFUL PICKUP DROPS"
ultra_msnd[3] = sndMut
ultra_tips[3] = "unstoppable"

ultra_name[4] = "HOMING MISSILE"
ultra_text[4] = "POWERFUL ROLL#HIGHER FIRERATE WHEN ROLLING"
ultra_msnd[4] = sndMut
ultra_tips[4] = "getting used to this"

//CRYSTAL

ultra_name[5] = "FORTRESS"
ultra_text[5] = "+6 MAX HP"
ultra_msnd[5] = sndMut
ultra_tips[5] = "stay strong"

ultra_name[6] = "JUGGERNAUT"
ultra_text[6] = "WALK WHILE SHIELDING"
ultra_msnd[6] = sndMut
ultra_tips[6] = "just a scratch"

ultra_name[7] = "SENTRY"
ultra_text[7] = "SHOOT WHILE SHIELDING"
ultra_msnd[7] = sndMut
ultra_tips[7] = "hold on"

ultra_name[8] = "MATRIX"
ultra_text[8] = "EMIT LASERS WHEN SHIELDING#INCREASED SHIELDING BACKSWING"
ultra_msnd[8] = sndMut
ultra_tips[8] = "burden"

//EYES

ultra_name[9] = "PROJECTILE STYLE"
ultra_text[9] = "PROJECTILES HOME TOWARDS YOU CURSOR# WHILE USING TELEKINESIS"
ultra_msnd[9] = sndMut
ultra_tips[9] = "keep it inside"

ultra_name[10] = "MONSTER STYLE"
ultra_text[10] = "PUSH AWAY NEARBY ENEMIES"
ultra_msnd[10] = sndMut
ultra_tips[10] = "great strength"

ultra_name[11] = "SEEKER STYLE"
ultra_text[11] = "PROJECTILES HOME WHEN USING TELEKINESIS"
ultra_msnd[11] = sndMut
ultra_tips[11] = "show nothing"

ultra_name[12] = "ATTRACTOR STYLE"
ultra_text[12] = "PULL IN ONSCREEN PICKUPS THROUGH WALLS#WHEN USING TELEKINESIS"
ultra_msnd[12] = sndMut
ultra_tips[12] = "know everything"

//MELTINGO

ultra_name[13] = "BRAIN CAPACITY"
ultra_text[13] = "ENEMIES CAN BE BLOWN UP AT LOW HP#GRANTS ONE MORE MUTATION CHOICE"
ultra_msnd[13] = sndMut
ultra_tips[13] = "please stop..."

ultra_name[14] = "CREMATION"
ultra_text[14] = "CORPSES HAVE A CHANCE OF NOT BEING#CONSUMED WHEN BLOWN UP#GRANTS ONE MORE MUTATION CHOICE"
ultra_msnd[14] = sndMut
ultra_tips[14] = "the pain..."

ultra_name[15] = "CLOSED CASKET"
ultra_text[15] = "THE FIRST ENEMY YOU KILL ELIMINATES#ALL ONSCREEN ENEMIES OF THAT TYPE#GRANTS ONE MORE MUTATION CHOICE"
ultra_msnd[15] = sndMut
ultra_tips[15] = "please..."

ultra_name[16] = "IMMOLATION"
ultra_text[16] = "LINGERING BLOOD EXPLOSIONS#GRANTS ONE MORE MUTATION CHOICE"
ultra_msnd[16] = sndMut
ultra_tips[16] = "stop..."

//PLANT

ultra_name[17] = "KILLER"
ultra_text[17] = "SNARED ENEMIES SPAWNS SAPLINGS#GRANTS BIG SNARES"
ultra_msnd[17] = sndMut
ultra_tips[17] = "end end end"

ultra_name[18] = "LEECH"
ultra_text[18] = "QUICKER DEATHS FOR THE ENSNARED#ENSNARED KILLS MAY HEAL PLANT"
ultra_msnd[18] = sndMut
ultra_tips[18] = "kill kill kill"

ultra_name[19] = "BARRICADE"
ultra_text[19] = "VINE WALLS WITH SCALING HEALTH; BLOCKS #DAMAGE FROM ALL SOURCES#IMPASSABLE UNTIL DESTROYED OR REDEPLOYED"
ultra_msnd[19] = sndMut
ultra_tips[19] = "every shot connects"

ultra_name[20] = "JUNGLE FEVER"
ultra_text[20] = "TBI"
ultra_msnd[20] = sndMut
ultra_tips[20] = "see them fly"

//YV

ultra_name[21] = "IMA GUN GOD"
ultra_text[21] = "EVEN HIGHER RATE OF FIRE"
ultra_msnd[21] = sndMut
ultra_tips[21] = "bolts everywhere"

ultra_name[22] = "BACK 2 BIZNIZ"
ultra_text[22] = "EXTRA POP POP#DOESN'T CONSUME AMMO"
ultra_msnd[22] = sndMut
ultra_tips[22] = "shaking"

ultra_name[23] = "YV 3" // name
ultra_text[23] = "TBI" // description
ultra_msnd[23] = sndMut // sound when picking it
ultra_tips[23] = "boom" // tip

ultra_name[24] = "YV 4" // name
ultra_text[24] = "TBI" // description
ultra_msnd[24] = sndMut // sound when picking it
ultra_tips[24] = "cough cough" // tip

//STEROIDS/

ultra_name[25] = "AMBIDEXTROUS" // name
ultra_text[25] = "BIGGER WEAPON AND AMMO CHESTS" // description
ultra_msnd[25] = sndMut // sound when picking it
ultra_tips[25] = "long arms won't save you" // tip

ultra_name[26] = "STEROIDS 2" // name
ultra_text[26] = "TBI" // description
ultra_msnd[26] = sndMut // sound when picking it
ultra_tips[26] = "bloodshot" // tip

ultra_name[27] = "STEROIDS 3" // name
ultra_text[27] = "TBI" // description
ultra_msnd[27] = sndMut // sound when picking it
ultra_tips[27] = "asphyxia" // tip

ultra_name[28] = "STEROIDS 4" // name
ultra_text[28] = "TBI" // description
ultra_msnd[28] = sndMut // sound when picking it
ultra_tips[28] = "scratch em" // tip

//ROBOT
ultra_name[29] = "REGURGITATE" // name
ultra_text[29] = "BURPS RANDOM CHESTS WHEN EATING WEAPONS#GRANTS BETTER WEAPON DROPS" // description
ultra_msnd[29] = sndMut // sound when picking it
ultra_tips[29] = "scratch em" // tip

ultra_name[30] = "robot 2" // name
ultra_text[30] = "fuck" // description
ultra_msnd[30] = sndMut // sound when picking it
ultra_tips[30] = "scratch em" // tip

ultra_name[31] = "robot 3" // name
ultra_text[31] = "fuck" // description
ultra_msnd[31] = sndMut // sound when picking it
ultra_tips[31] = "scratch em" // tip

ultra_name[32] = "robot 4" // name
ultra_text[32] = "fuck" // description
ultra_msnd[32] = sndMut // sound when picking it
ultra_tips[32] = "scratch em" // tip

//CHICKEN
ultra_name[33] = "HARDER TO KILL" // name
ultra_text[33] = "EACH KILL GRANTS ONE MORE SECOND#ALIVE WHEN HEADLESS" // description
ultra_msnd[33] = sndMut // sound when picking it
ultra_tips[33] = "scratch em" // tip

ultra_name[34] = "chicken 2" // name
ultra_text[34] = "fuck" // description
ultra_msnd[34] = sndMut // sound when picking it
ultra_tips[34] = "scratch em" // tip

ultra_name[35] = "chicken 3" // name
ultra_text[35] = "fuck" // description
ultra_msnd[35] = sndMut // sound when picking it
ultra_tips[35] = "scratch em" // tip

ultra_name[36] = "chicken 4" // name
ultra_text[36] = "fuck" // description
ultra_msnd[36] = sndMut // sound when picking it
ultra_tips[36] = "scratch em" // tip

//REBEL

ultra_name[37] = "PERSONAL GUARD" // name
ultra_text[37] = "START THE LEVEL WITH FOUR ALLIES#ALLIES HAVE MORE HEALTH" // description
ultra_msnd[37] = sndMut // sound when picking it
ultra_tips[37] = "scratch em" // tip

ultra_name[38] = "RIOT" // name
ultra_text[38] = "fuck" // description
ultra_msnd[38] = sndMut // sound when picking it
ultra_tips[38] = "scratch em" // tip

ultra_name[39] = "rebel 3" // name
ultra_text[39] = "fuck" // description
ultra_msnd[39] = sndMut // sound when picking it
ultra_tips[39] = "scratch em" // tip

ultra_name[40] = "rebel 4" // name
ultra_text[40] = "fuck" // description
ultra_msnd[40] = sndMut // sound when picking it
ultra_tips[40] = "scratch em" // tip

//BRUTE

ultra_name[41] = "brute 1" // name
ultra_text[41] = "fuck" // description
ultra_msnd[41] = sndMut // sound when picking it
ultra_tips[41] = "scratch em" // tip

ultra_name[42] = "brute 2" // name
ultra_text[42] = "fuck" // description
ultra_msnd[42] = sndMut // sound when picking it
ultra_tips[42] = "scratch em" // tip

ultra_name[43] = "brute 3" // name
ultra_text[43] = "fuck" // description
ultra_msnd[43] = sndMut // sound when picking it
ultra_tips[43] = "scratch em" // tip

ultra_name[44] = "brute 4" // name
ultra_text[44] = "fuck" // description
ultra_msnd[44] = sndMut // sound when picking it
ultra_tips[44] = "scratch em" // tip

//grub
ultra_name[45] = "BOTTOM FEEDER" // name
ultra_text[45] = "ENEMIES AT LOW HP#CAN BE EATEN" // description
ultra_msnd[45] = sndMut // sound when picking it
ultra_tips[45] = "sure, i could go for a bite" // tip

ultra_name[46] = "INFECTIOUS FEEDER" // name
ultra_text[46] = "EATING CORPSES SPAWNS#FRIENDLY TOXIC" // description
ultra_msnd[46] = sndMut // sound when picking it
ultra_tips[46] = "younglings in loot" // tip

ultra_name[47] = "SYMBIOTIC FEEDER" // name
ultra_text[47] = "BIGGER MAGGOTS THAT DEAL MORE DAMAGE#AND EXPLODE INTO SMALLER ONES" // description
ultra_msnd[47] = sndMut // sound when picking it
ultra_tips[47] = "they grow up so fast" // tip

ultra_name[48] = "JUICY FEEDER" // name
ultra_text[48] = "EATING CORPSES SPAWNS DROPS" // description
ultra_msnd[48] = sndMut // sound when picking it
ultra_tips[48] = "scratch em" // tip

//TURRET
ultra_name[49] = "ALPHA" // name
ultra_text[49] = "TURRETS FIRE HEAVY BULLETS#DECREASES TURRET CAP#HIGHER DAMAGE OUTPUT" // description
ultra_msnd[49] = sndMut // sound when picking it
ultra_tips[49] = "can't stop" // tip

ultra_name[50] = "BETA" // name
ultra_text[50] = "UPGRADED, 4-WAY TURRETS" // description
ultra_msnd[50] = sndMut // sound when picking it
ultra_tips[50] = "updating surviving skills..." // tip

ultra_name[51] = "GAMMA" // name
ultra_text[51] = "HIGHER TURRETS' FIRE RATE#TURRETS' BULLETS HOME IN" // description
ultra_msnd[51] = sndMut // sound when picking it
ultra_tips[51] = "scratch em" // tip

ultra_name[52] = "DELTA" // name
ultra_text[52] = "CHAOTIC TURRETFIRE" // description
ultra_msnd[52] = sndMut // sound when picking it
ultra_tips[52] = "scratch em" // tip

//PLATYPUS
ultra_name[53] = "plat1" // name
ultra_text[53] = "UPGRADED, 4-WAY TURRETS" // description
ultra_msnd[53] = sndMut // sound when picking it
ultra_tips[53] = "updating surviving skills..." // tip

ultra_name[54] = "plat2" // name
ultra_text[54] = "fuck" // description
ultra_msnd[54] = sndMut // sound when picking it
ultra_tips[54] = "scratch em" // tip

ultra_name[55] = "plat3" // name
ultra_text[55] = "fuck" // description
ultra_msnd[55] = sndMut // sound when picking it
ultra_tips[55] = "scratch em" // tip

ultra_name[56] = "plat4" // name
ultra_text[56] = "fuck" // description
ultra_msnd[56] = sndMut // sound when picking it
ultra_tips[56] = "scratch em" // tip

//HORROR
ultra_name[57] = "STALKER"
ultra_text[57] = "ENEMIES EXPLODE IN RADIATION UPON DEATH"
ultra_msnd[57] = sndMut
ultra_tips[57] = "just one more day"

ultra_name[58] = "ANOMALY"
ultra_text[58] = "LEVELS END EARLY"
ultra_msnd[58] = sndMut
ultra_tips[58] = "run forever"

ultra_name[59] = "FALLOUT"
ultra_text[59] = "DOUBLE RAD CAPACITY#GAIN MISSING RADS EACH LEVEL"
ultra_msnd[59] = sndMut
ultra_tips[59] = "unstoppable"

ultra_name[60] = "HALF-LIFE"
ultra_text[60] = "DECREASED RAD CONSUMPTION"
ultra_msnd[60] = sndMut
ultra_tips[60] = "getting used to this"

//EXTRA HORROR MUTATION
ultra_name[999] = "ROENTGEN"
ultra_text[999] = "SPAWN CLONES FROM BEAM [NYI]"
ultra_msnd[999] = sndMut
ultra_tips[999] = "getting used to this"

maxultra = 60

dir = 0
repeat(maxultra+1)
{ultra_got[dir] = 0
dir += 1}
//ultra_got[20] = 1
