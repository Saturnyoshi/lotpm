/// scrDrop(Item Drop Chance, Weapon Drop Chance);
if instance_exists(Player)
{
if Player.crown = 5
argument1 += 5

if Player.crown = 11
{
    if Player.my_health = Player.maxhealth
    argument0 -= argument0/2;
    else
    argument0 += argument0*2;
}
//calculate need
need = 0

if Player.ammo[Player.wep_type[Player.wep]] < Player.typ_amax[Player.wep_type[Player.wep]]*0.2
need += 0.75
else if Player.ammo[Player.wep_type[Player.wep]] > Player.typ_amax[Player.wep_type[Player.wep]]*0.6
need += 0.1 else need += 0.5

if Player.bwep = 0 need += 0.5
else if Player.ammo[Player.wep_type[Player.bwep]] < Player.typ_amax[Player.wep_type[Player.bwep]]*0.2
need += 0.75
else if Player.ammo[Player.wep_type[Player.bwep]] > Player.typ_amax[Player.wep_type[Player.bwep]]*0.6
need += 0.1 else need += 0.5

//drop items
if random(100) < argument0*(1+need+Player.skill_got[4]*0.6)
{
if random(Player.maxhealth) > Player.my_health and random(3) < 2 
instance_create(x+random(4)-2,y+random(4)-2,HPPickup)
else if Player.crown != 5 and Player.crown != 2
instance_create(x+random(4)-2,y+random(4)-2,AmmoPickup)
}
else if random(100) < argument1*(1+Player.skill_got[4]*0.6) 
{
//drop weps
with instance_create(x+random(4)-2,y+random(4)-2,WepPickup)
{
scrWeapons()
scrDecideWep(0)
name = wep_name[wep]
type = wep_type[wep]
ammo = 50
curse = 0
sprite_index = wep_sprt[wep]
}
}
}
