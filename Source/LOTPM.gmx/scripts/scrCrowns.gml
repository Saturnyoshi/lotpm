crown_name[0] = "[RANDOM]"
crown_text[0] = "???"
crown_have[0] = 1
crown_tips[0] = ""

crown_name[1] = "[NONE]"
crown_text[1] = "A BARE HEAD#IS A FAIR HEAD"
crown_have[1] = 1
crown_tips[1] = ""

crown_name[2] = "[CROWN OF LIFE]"
crown_text[2] = "HEALTH CHESTS ARE MORE COMMON#NO HEALTH PACKS"
crown_have[2] = 1
crown_tips[2] = "life"

crown_name[3] = "[CROWN OF DEATH]"
crown_text[3] = "-1 MAX HP#REVENGE#MORE EXPLOSIONS"
crown_have[3] = 1
crown_tips[3] = "nothing goes unpunished"

crown_name[4] = "[CROWN OF HASTE]"
crown_text[4] = "PICKUPS FADE FAST#ARE WORTH MORE"
crown_have[4] = 1
crown_tips[4] = "no time for jokes"

crown_name[5] = "[CROWN OF GUNS]"
crown_text[5] = "NO AMMO DROPS#MORE WEAPON DROPS"
crown_have[5] = 1
crown_tips[5] = "guns are your friends"

crown_name[6] = "[CROWN OF HATRED]"//THIS ONE SUCKS
crown_text[6] = "LOSE HEALTH & GAIN#RADIATION OVER TIME"
crown_have[6] = 1
crown_tips[6] = "something is wrong"

crown_name[7] = "[CROWN OF BLOOD]"
crown_text[7] = "MORE ENEMIES"
crown_have[7] = 1
crown_tips[7] = "bring it"

crown_name[8] = "[CROWN OF DESTINY]"
crown_text[8] = "FREE MUTATION#NARROW FUTURE"
crown_have[8] = 1
crown_tips[8] = "no such thing as free will"

crown_name[9] = "[CROWN OF LOVE]"
crown_text[9] = "AMMO CHESTS ONLY"
crown_have[9] = 1
crown_tips[9] = "you really like these weapons"

crown_name[10] = "[CROWN OF CURSES]"
crown_text[10] = "MORE CURSED CHESTS"
crown_have[10] = 1
crown_tips[10] = ""

crown_name[11] = "[CROWN OF RISK]"
crown_text[11] = "HIGHER DROP CHANCE AT LOW HEALTH#LOWER DROP CHANCE AT FULL HEALTH"
crown_have[11] = 1
crown_tips[11] = ""

crown_name[12] = "[CROWN OF PROTECTION]"
crown_text[12] = "WEAPONS GIVE HEALTH INSTEAD#OF AMMO"
crown_have[12] = 1
crown_tips[12] = ""

crown_name[13] = "[CROWN OF LUCK]"
crown_text[13] = "START LEVELS AT 1 HP#RANDOM ENEMIES GET 1 HEALTH"
crown_have[13] = 1
crown_tips[13] = ""


crownmax = 13
