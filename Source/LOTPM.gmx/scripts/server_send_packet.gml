///server_send_packet(packet type(,values))
if !global.online exit

var buff=buffer_create(2,buffer_grow,1)
buffer_write(buff,buffer_u8,argument[0])
buffer_write(buff,buffer_u16,m_id)
switch argument[0] {
    case pac_enemy: {
        buffer_write(buff,buffer_f64,x)
        buffer_write(buff,buffer_f64,y)
        buffer_write(buff,buffer_bool,(right=1))
        buffer_write(buff,buffer_u8,alarm[1])
        buffer_write(buff,buffer_f16,direction)
        buffer_write(buff,buffer_f16,speed)
        /* PACKET INFO -
                Update standard enemy types.
            201 byte length total.
        */
    }
}
buffer_delete(buff)
