//OPTIONS
//audio
opt_sfxvol = 0.75
opt_musvol = 0.25
opt_ambvol = 0.15

//visual
opt_fulscrn = 1
opt_scaling = 1
opt_niceshd = 1
opt_nicedrk = 1

//controls
opt_gamepad = 0
opt_autoaim = 1

//other
opt_shake = 0.5
opt_mousecp = 0

//more visual
opt_fitscrn = 0


//DETECT RESOLUTION
opt_scaling = min(floor(display_get_width()/320),floor(display_get_height()/240))

ini_write_real("OPTIONS","Sound Effects Volume",opt_sfxvol)
ini_write_real("OPTIONS","Music Volume",opt_musvol)
ini_write_real("OPTIONS","Ambient Volume",opt_ambvol)

ini_write_real("OPTIONS","Fullscreen",opt_fulscrn)
ini_write_real("OPTIONS","Scaling",opt_scaling)
ini_write_real("OPTIONS","Nice Shadows",opt_niceshd)
ini_write_real("OPTIONS","Nice Dark",opt_nicedrk)

ini_write_real("OPTIONS","Use Gamepad",opt_gamepad)
ini_write_real("OPTIONS","Gamepad Autoaim",opt_autoaim)

ini_write_real("OPTIONS","Screenshake",opt_shake)
ini_write_real("OPTIONS","Capture Mouse",opt_mousecp)

ini_write_real("OPTIONS","Fit Screen",opt_fitscrn)

ini_write_real("OPTIONS","View Scaling",opt_scaling)
