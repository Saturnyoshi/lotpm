if widescreen < 48
widescreen += 8


mouseover = -1

with CharSelect{
if mouse_check_button_pressed(vk_left) and other.mouseover != num // and y > view_yview+48 and y < view_yview+view_hview-48
other.mouseover = num;}

if mouseover = race and race != 0
extra = 96
else if mouseover != -1
extra = 0

draw_set_font(fntM)
draw_set_valign(fa_bottom)

/*with CampChar {
if other.mouseover = num or (other.race = num and other.mouseover = -1){
draw_set_color(c_black)
draw_set_alpha(0.8)
draw_rectangle(x-2-string_width(Menu.race_name[num])/2,y-26-string_height("A"),x+2+string_width(Menu.race_name[num])/2,y-22,0)
draw_set_alpha(1)
draw_sprite_ext(sprMenuPointer,0,x,y-22,1,1,0,c_white,0.8)
draw_set_halign(fa_center)
draw_text(x+1,y-24,string(Menu.race_name[num]))
draw_text(x+1,y-24+1,string(Menu.race_name[num]))
draw_text(x,y-24+1,string(Menu.race_name[num]))
draw_set_color(c_white)
draw_text(x,y-24,string(Menu.race_name[num]))
}}*/
draw_sprite(sprMenuCharPortrait,race,view_xview,view_yview)
if race != 0 then draw_sprite(sprMenuStain1,race,view_xview,view_yview)
draw_sprite(sprMenuCharNames,race,view_xview,view_yview-8)
draw_set_halign(fa_left)


if mouseover = -1
txt2 = "##"+string(race_pass[race])+"#"+string(race_acti[race])
else if UberCont.cgot[mouseover] = 1
txt2 = "##"+string(race_pass[mouseover])+"#"+string(race_acti[mouseover])
else
txt2 = "##"+string(race_lock[mouseover])

if mouseover = -1
drawx = view_xview+10
else
drawx = view_xview+10
drawy = view_yview+10+view_hview-widescreen


if drawx-string_width(txt2)/2-2 < view_xview+2
drawx = view_xview+4

if drawx+extra > view_xview+view_wview-2
drawx = view_xview+view_wview-4-string_width(txt2)/2-extra



draw_text(drawx,drawy-13,string(txt2))
draw_text(drawx+1,drawy-13,string(txt2))
draw_text(drawx+1,drawy-14,string(txt2))
draw_set_color(c_silver)
draw_text(drawx,drawy-14,string(txt2))
draw_set_color(c_white)



img += 0.1

if mouseover = -1
draw_sprite(sprRMBIcon,-1,drawx-string_width(race_acti[race])-8,drawy-22)
else if UberCont.cgot[mouseover] = 1
draw_sprite(sprRMBIcon,-1,drawx-string_width(race_acti[mouseover])-8,drawy-22)
