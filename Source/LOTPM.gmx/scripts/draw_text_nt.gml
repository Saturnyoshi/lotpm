///draw_text_nt(x,y,string)
drawcol = c_white;
if (draw_get_color() != drawcol) draw_set_color(drawcol);
var txt = "";
var spc = "";
var dir = 0;
var rst = "";
var dix = 0;
var lx = 0;
var lw = 0;
var ly = 0;
var lend = 0;
if (string_pos("@", argument2) == 0) {
    draw_set_color(c_black);
    draw_text(argument0, argument1 + 1, string_upper(argument2));
    draw_text(argument0 + 1, argument1 + 1, string_upper(argument2));
    draw_text(argument0 + 1, argument1, string_upper(argument2));
    draw_set_color(c_white);
    draw_text(argument0, argument1, string_upper(argument2));
} else {
    do {
        dir += 1;
        if (string_char_at(argument2, dir) != "@") {
            if (string_char_at(argument2, dir) == "#") {
                spc += "#";
                txt += "#";
                lx = 0;
                lw = 0;
                ly += 1;
            } else {
                spc += " ";
                txt += string_char_at(argument2, dir);
                lx += 1;
                lw += 1;
            }
        }
        if (string_char_at(argument2, dir) == "@" || dir > string_length(argument2)) {
            dix = dir;
            rst = "";
            lend = 0;
            do {
                if (string_char_at(argument2, dix) == "#") {
                    rst += "#";
                    lend = 1;
                } else if (string_char_at(argument2, dix) == "@") {
                    dix += 1;
                } else if (string_char_at(argument2, dix) != "@") {
                    if (lend == 0) lw += 1;
                    rst += " ";
                }
                dix += 1;
            } until (dix > string_length(argument2) + 1);
            draw_set_color(c_black);
            draw_text(argument0, argument1 + 1, string_upper(txt) + string(rst));
            draw_text(argument0 + 1, argument1 + 1, string_upper(txt) + string(rst));
            draw_text(argument0 + 1, argument1, string_upper(txt) + string(rst));
            draw_set_color(drawcol);
            draw_text(argument0, argument1, string_upper(txt) + string(rst));
            txt = spc;
            dir += 1;
            if (string_char_at(argument2, dir) == "s") drawcol = make_color_rgb(125, 131, 141);//LIGHT GRAY OR SILVER TEXT
            if (string_char_at(argument2, dir) == "d") drawcol = make_color_rgb(59, 62, 67);//DARK GRAY TEXT
            if (string_char_at(argument2, dir) == "y") drawcol = make_color_rgb(250, 171, 0);//YELLOW TEXT
            if (string_char_at(argument2, dir) == "r") drawcol = make_color_rgb(252, 56, 0);//RED OR ORANGE TEXT
            if (string_char_at(argument2, dir) == "b") drawcol = make_color_rgb(22, 97, 223);//BLUE TEXT
            if (string_char_at(argument2, dir) == "g") drawcol = make_color_rgb(68, 198, 22);//GREEN TEXT
            if (string_char_at(argument2, dir) == "p") drawcol = make_color_rgb(86, 34, 110);//PURPLE TEXT
            if (string_char_at(argument2, dir) == "w") drawcol = make_color_rgb(255, 255, 255);//WHITE TEXT
            if (string_char_at(argument2, dir) == "q") {//SHAKING TEXT
                argument0 += random(2) - 1;
                argument1 += random(2) - 1;
            }
        }
    } until (dir > string_length(argument2) + 1);
}
