////////SHIT PRESSED////////
if KeyCont.key_spec[p] = 1

{
//PLATYPUS
if Player.race = 14
{
    if Player.rad > 10 then
    {
        if race = 14 and Player.platypusmode == 0 then
        {
            //becomes berserk
            platypusmode = 1
            maxspeed = 8
            maxhealth -= 3
            if my_health > 6+(4*Player.skill_got[1]) then my_health = 6+(4*Player.skill_got[1])
            accuracy += 0.4
            dir = instance_create(x,y,PopupText)
            dir.mytext = "BERSERK MODE"
            
            
            spr_idle = sprMutant14MadIdle
            spr_hurt = sprMutant14MadHurt
            spr_walk = sprMutant14MadWalk
            
            exit;
        }
        
        if race = 14 and Player.platypusmode == 1 then
        {
            //becomes control
            platypusmode = 0
            maxspeed = 4.3
            maxhealth += 3
            accuracy -= 0.4
            
            dir = instance_create(x,y,PopupText)
            dir.mytext = "CONTROL MODE"
            
            spr_idle = sprMutant14Idle
            spr_hurt = sprMutant14Hurt
            spr_walk = sprMutant14Walk
            
            exit;
        }

    }
    else
    {
        dir = instance_create(x,y,PopupText)
        dir.mytext = "NOT ENOUGH RADS"
    }
}
//BRUTE

//ROBOT
if race = 8
{
if bwep != 0
{if string_copy(wep_name[wep],0,4) = "GOLD"
{repeat(4)
{if random(maxhealth) > my_health and Player.crown != 2
instance_create(x,y,HPPickup)
else
instance_create(x,y,AmmoPickup)}}
with instance_create(x,y,RobotEat)
image_xscale = Player.right

if random(maxhealth) > my_health and Player.crown != 2
instance_create(x,y,HPPickup)
else
instance_create(x,y,AmmoPickup)

if curse = 1
{
curse = 0
my_health -= 7
repeat(10)
instance_create(x+random(16)-8,y+random(16)-8,Curse)
}

scrSwapWeps()
bwep = 0

if skill_got[5] = 1
{
snd_play(sndRobotEatUpg)
instance_create(x,y,AmmoPickup)
}
else
snd_play(sndRobotEat)

breload = 0
instance_create(x,y,Smoke)

//REGURGITATE
if ultra_got[29] == 1
{
if random(100)<43 then instance_create(x,y,choose(AmmoChest,AmmoChest,WeaponChest,RadChest,HealthChest))
}


}
}

//FISH
if race = 1
{if speed < 0.4
direction = point_direction(x,y,mouse_x,mouse_y)
speed = 4
roll = 1
snd_play(sndRoll)

if skill_got[5] = 1
snd_play(sndFishRollUpg)

instance_create(x,y,Dust)
}

//REBEL
if race = 10 and my_health > 1+instance_exists(Ally)
{canrebel = 1

if skill_got[5] = 1
snd_play(sndSpawnSuperAlly)
else
snd_play(sndSpawnAlly)

sprite_index = spr_hurt
image_index = 0
my_health -= 1+instance_exists(Ally)
snd_play(snd_hurt)
sleep(20);
instance_create(x,y,Dust)

with Ally
{
instance_create(x,y,HealFX)
alarm[2] = 120
my_health = 12
}
instance_create(x,y,Ally)

//RIOT
if ultra_got[38] == 1
{
with instance_create(x+random(choose(4,-4)),y+random(choose(4,-4)),Ally)
{
instance_create(x,y,HealFX)
alarm[2] = 120
my_health = 12
}
}


}

//CRYSTAL
if race = 2 and !instance_exists(CrystalShield) and !instance_exists(fuckingnothing2)
{if skill_got[5] = 1 and place_meeting(mouse_x,mouse_y,Floor) and !place_meeting(mouse_x,mouse_y,Wall)
{x = mouse_x
y = mouse_y}
instance_create(x,y,CrystalShield)
}

//MELTING
if race = 4
{
with Corpse
{if image_speed = 0 and (instance_number(enemy) > 0 or instance_exists(Portal)) and x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
{

snd_play(sndExplosion)

if Player.skill_got[5] = 1
snd_play(sndCorpseExploUpg)
else
snd_play(sndCorpseExplo)

if Player.ultra_got[14] == 0
{instance_destroy()}

else if Player.ultra_got[14] == 1
{
//CREMATION
if random(25)>3 then {instance_destroy()}
}

with instance_create(x,y,BloodStreak)
{
motion_add(point_direction(Player.x,Player.y,x,y),8)
image_angle = direction
}
with instance_create(x,y,Scorchmark){ if Player.skill_got[5] sprite_index = sprMeltSplatBig; }
if Player.skill_got[5] = 1
{ang = random(360)
instance_create(x+lengthdir_x(24,ang),y+lengthdir_y(24,ang),MeatExplosion)
instance_create(x+lengthdir_x(24,ang+120),y+lengthdir_y(24,ang+120),MeatExplosion)
instance_create(x+lengthdir_x(24,ang+240),y+lengthdir_y(24,ang+240),MeatExplosion)}
instance_create(x,y,MeatExplosion)}
}

//BRAIN CAPACITY

if Player.ultra_got[13] == 1 then
with enemy
{
if my_health < 6
{
snd_play(sndExplosion)

if Player.skill_got[5] = 1
snd_play(sndCorpseExploUpg)
else
snd_play(sndCorpseExplo)

instance_destroy()
with instance_create(x,y,BloodStreak)
{
motion_add(point_direction(Player.x,Player.y,x,y),8)
image_angle = direction
}

with instance_create(x,y,Scorchmark){ if Player.skill_got[5] sprite_index = sprMeltSplatBig; }
if Player.skill_got[5] = 1
{ang = random(360)
instance_create(x+lengthdir_x(24,ang),y+lengthdir_y(24,ang),MeatExplosion)
instance_create(x+lengthdir_x(24,ang+120),y+lengthdir_y(24,ang+120),MeatExplosion)
instance_create(x+lengthdir_x(24,ang+240),y+lengthdir_y(24,ang+240),MeatExplosion)}
instance_create(x,y,MeatExplosion)
}

}

}

//PLANT
if race = 5
{
with Tangle
instance_destroy()
with TangleSeed
instance_destroy()
with instance_create(x,y,TangleSeed)
{motion_add(point_direction(x,y,mouse_x,mouse_y),12)
image_angle = direction
team = other.team}
}

//YUNG VENUZ
if wep_type[wep] != 0
{
if race = 6 and can_shoot = 1
{
if ammo[wep_type[wep]] < wep_cost[wep]*(2+Player.skill_got[5]*2) and KeyCont.key_spec[p] = 1 and wep_type[wep] != 0
scrEmpty()

if ammo[wep_type[wep]] >= wep_cost[wep]*(2+Player.skill_got[5]*2)
{repeat(2+Player.skill_got[5]*2)
{scrFire(point_direction(x,y,mouse_x,mouse_y))
can_shoot = 1}
can_shoot = 0

if Player.skill_got[5] = 1
snd_play(sndPopPopUpg)
else
snd_play(sndPopPop)

//reload *= 2+Player.skill_got[5]*1.4
motion_add(point_direction(x,y,mouse_x,mouse_y)+180,4)
BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y))
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y))
BackCont.shake += 1}

if ultra_got[22] == 1
{
if ammo[wep_type[wep]] < wep_cost[wep]*(2) and KeyCont.key_spec[p] = 1 and wep_type[wep] != 0
scrEmpty()

if ammo[wep_type[wep]] >= wep_cost[wep]*(2)
{repeat(2)
{scrFire(point_direction(x,y,mouse_x,mouse_y))
ammo[wep_type[wep]] += wep_cost[wep]
can_shoot = 1}
can_shoot = 0
}

}
}
}
else if race = 6
snd_play(sndMutant6No);

}

////////SHIT HELD////////
if KeyCont.key_spec[p] = 1 or KeyCont.key_spec[p] = 2
{
    //HORROR
    if race = 15
    {
        if rad > max(1,round((horrorcharge/30)-(((horrorcharge/30)*0.238)*ultra_got[60])))
        {
            rad -= max(1,round((horrorcharge/30)-(((horrorcharge/30)*0.238)*ultra_got[60])));
            
            if KeyCont.key_spec[p] = 1
            snd_play(sndHorrorBeam);
            
            horrorcharge += 1+(skill_got[5]);
            alarm[0] = 60;
            
            if skill_got[5]
            {
                if !audio_is_playing(sndHorrorLoop)
                snd_play_global(sndHorrorLoop)
            }
            else
            {
                if !audio_is_playing(sndHorrorLoopTB)
                snd_play_global(sndHorrorLoopTB)
            }
            
            repeat(max(1,floor(horrorcharge/15)))
            with instance_create(x+random(4)+random(horrorcharge/15),y+random(4)+random(horrorcharge/15),HorrorBullet)
            {
                motion_add(point_direction(x,y,mouse_x,mouse_y),12)
                image_angle = direction
                team = other.team
            }
            
            BackCont.viewx2 += lengthdir_x(round(horrorcharge/30),point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
            BackCont.viewy2 += lengthdir_y(round(horrorcharge/30),point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
            BackCont.shake += round(horrorcharge/30)
        }
        else if KeyCont.key_spec[p] = 1
        {
            snd_play(sndHorrorEmpty);
            audio_stop_sound(sndHorrorLoop);
            audio_stop_sound(sndHorrorLoopTB);
            
            with instance_create(x,y,PopupText)
            mytext = "NOT ENOUGH RADS";
        }
    }

    //ANALMAN
    if race = 16
    {
        if spr_idle = sprMutant16Idle
        {
            spr_idle = sprMutant16FlyStart;
            sprite_index = spr_idle;
            image_index = 0;
            speed = 0;
        }
    }
    
    //CHICKEN
    //SUPER HOT
    //SUPER HOT!
    //SUPER! HOT!
    if race = 9
    {
        
        
        if !audio_is_playing(sndChickenLoop) {snd_play(sndChickenStart) snd_loop(sndChickenLoop)}
        
        if Player.speed < 2
        {
        if Player.my_health > 0 && reload > 0 
        reload += 0.8-(Player.skill_got[5]*0.3);
        
        with enemy
        {
        speed *= 0
        x = xprevious
        y = yprevious
        image_index -= image_speed*0.00001
        if sprite_index = spr_walk then sprite_index = spr_idle
        
        alarm[0] += 1
        alarm[1] += 1
        
        }
        
        with projectile
        {
        if point_distance(x,y,other.x,other.y) < 220
        {x -= hspeed
        y -= vspeed
        speed += friction
        
        alarm[0] += 1
        alarm[1] += 1
        }
        }
        
        with RainDrop
        {
        if point_distance(x+addx,y-addy,other.x,other.y) < 96{
        addx += 10
        addy += 10}
        }
        
        with SnowFlake
        {
        if point_distance(x+addx,y-addy,other.x,other.y) < 96{
        addx += sin(wave/5)*0.7
        addy += (1-sin(wave/3)/2)*0.7
        wave -= 0.2*0.7}
        }
        }
    
    }
    
    //ORIGINAL CHICKEN
    /*
    if race = 9
    {
    
    
    if !audio_is_playing(sndChickenLoop) {snd_play(sndChickenStart) snd_loop(sndChickenLoop)}
    
    if reload > 0 and skill_got[5] = 0
    reload += 0.5
    speed *= 0.3
    image_index -= image_speed*0.7
    
    with enemy
    {
    if point_distance(x,y,other.x,other.y) < 96
    {
    speed *= 0.5
    image_index -= image_speed*0.5
    }
    }
    with projectile
    {
    if point_distance(x,y,other.x,other.y) < 96
    {x -= hspeed*0.6
    y -= vspeed*0.6}
    }
    
    with RainDrop
    {
    if point_distance(x+addx,y-addy,other.x,other.y) < 96{
    addx += 10
    addy += 10}
    }
    
    with SnowFlake
    {
    if point_distance(x+addx,y-addy,other.x,other.y) < 96{
    addx += sin(wave/5)*0.7
    addy += (1-sin(wave/3)/2)*0.7
    wave -= 0.2*0.7}
    }
    
    
    }
    */
    
    //STEROIDS
    if race = 7 and bwep != 0
    {
        scrSwapWeps()
        
        if ammo[wep_type[wep]] < wep_cost[wep] and KeyCont.key_spec[p] = 1 and wep_type[wep] != 0
        scrEmpty()
        
        
        if can_shoot = 1 and ammo[wep_type[wep]] >= wep_cost[wep]
        {
        if wep_auto[wep] = 0 and KeyCont.key_spec[p] = 1
        {
        speed /= 4
        scrFire(point_direction(x,y,mouse_x,mouse_y))
        clicked = 0
        }
        if wep_auto[wep] = 1
        scrFire(point_direction(x,y,mouse_x,mouse_y))
        }
        scrSwapWeps()
    }
    
    //MIND CONTROL
    if race = 3
    {
        if !audio_is_playing(sndEyesLoop) snd_loop(sndEyesLoop)
        
        if !audio_is_playing(sndEyesLoopUpg) and Player.skill_got[5] =1 snd_loop(sndEyesLoopUpg)
        
        with enemy
        {
        if object_index != FireTurret
        {
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        }
        }
        }
        
        with projectile
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview and team != 2 and object_index != EnemyLaser
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)+180),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)+180)
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)+180))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)+180)}}
        
        //PICK UPS
        //ATTRACTOR STYLE
        if ultra_got[12] = 1
        {
        
        with HPPickup
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        }
        
        }
        with AmmoPickup
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        depth = -8
        }
        
        }
        with Rad
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        depth = -8
        }
        
        }
        with BigRad
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        depth = -8
        }
        
        }
        with WepPickup
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        depth = -8
        }
        
        }
        with chestprop
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        depth = -8
        }
        
        }
        with RadChest
        {
        
        if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {
        x += lengthdir_x(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        y += lengthdir_y(2+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        depth = -8
        }
        
        }
        
        }
        
        else
        
        {
        
        with HPPickup
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        with AmmoPickup
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        with Rad
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        with BigRad
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        with WepPickup
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        with chestprop
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        with RadChest
        {if x > view_xview and x < view_xview+view_wview and y > view_yview and y < view_yview+view_hview
        {if place_free(x+lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)),y)
        x += lengthdir_x(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))
        if place_free(x,y+lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y)))
        y += lengthdir_y(1+Player.skill_got[5],point_direction(x,y,Player.x,Player.y))}}
        
        }
    
    }

}
else
{ 
    if audio_is_playing(sndEyesLoop) or audio_is_playing(sndChickenLoop) or audio_is_playing(sndHorrorLoop) or audio_is_playing(sndHorrorLoopTB){audio_stop_sound(sndEyesLoop) audio_stop_sound(sndEyesLoopUpg) audio_stop_sound(sndChickenLoop) audio_stop_sound(sndHorrorLoop) audio_stop_sound(sndHorrorLoopTB)} 
    if sprite_index = sprMutant16Fly and collision_rectangle(x-6,y-6,x+6,y+6,Floor,false,false)
    {
        spr_idle = sprMutant16FlyLand;
        sprite_index = spr_idle;
        z = 0;
        speed = 0;
    }
}
