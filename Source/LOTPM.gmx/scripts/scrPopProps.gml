if !place_meeting(x,y,Player) and !place_meeting(x,y,enemy) and !place_meeting(x,y,RadChest) and !place_meeting(x,y,AmmoChest) and !place_meeting(x,y,WeaponChest) and !place_meeting(x,y,MeleeFake) and !place_meeting(x,y,GatorSmoke) and !place_meeting(x,y,prop)
{

    if random(5) < 1 and !place_meeting(x,y,NOWALLSHEREPLEASE) and Player.area != 100 and Player.area != 9 and Player.area != 6 
    and Player.area != 11 and (Player.area !=5 or random(3) < 1) and Player.area != 102 and Player.area != 104 and Player.subarea != -1
    {
        myx = x+choose(0,16)
        myy = y+choose(0,16)
        
        instance_create(myx,myy,Wall)
        instance_create(x,y,NOWALLSHEREPLEASE)
    }
    else if random(12) < 1
    {
        if spawnarea = 1
        {
            if random(60) < 1
            instance_create(x+16,y+16,BigSkull)
            else
            instance_create(x+16,y+16,Cactus)
        }
        
        if spawnarea = 2
        instance_create(x+16,y+16,choose(Pipe,Pipe,ToxicBarrel))
        if spawnarea = 0
        instance_create(x+16,y+16,NightCactus)
        if spawnarea = 4
        instance_create(x+16,y+16,choose(Crystal,Crystal,Cocoon))
        if spawnarea = 3
        instance_create(x+16,y+16,choose(Tires,Car))
        if spawnarea = 5
        {
        if random(35) < 1
        instance_create(x+16,y+16,choose(SnowMan,SodaMachine))
        else if random(3) < 1
        {
        repeat(3)
        instance_create(x+16,y+16,StreetLight)
        }
        else
        instance_create(x+16,y+16,choose(Hydrant,Car))
        }
        if spawnarea = 6 and random(4) < 1
        instance_create(x+16,y+16,choose(Tube,Tube,Tube,Tube,MutantTube))
        if spawnarea = 8
        {
        if random(60) < 1
        instance_create(x+16,y+16,MushProp2)
        else
        instance_create(x+16,y+16,MushProp1)
        }
        if spawnarea = 7
        {
        if random(60) < 1
        instance_create(x+16,y+16,Pillar)
        else
        instance_create(x+16,y+16,Brick)
        }
        
        if spawnarea = 9 and Player.subarea = 3 and random(3) < 1
        instance_create(x+16,y+16,choose(SunkenLight,Debris1))
        if spawnarea = 100
        instance_create(x+16,y+16,Torch)
        if spawnarea = 101
        instance_create(x+16,y+16,OasisProp)
        if spawnarea = 103
        instance_create(x+16,y+16,choose(MoneyPile,MoneyPile,MoneyPile,YVStatue,GoldBarrel,MoneyPile))
        if spawnarea = 102
        instance_create(x+16,y+16,PizzaBox)
        if spawnarea = 106
        instance_create(x+16,y+16,Crystal)
    }
    else if random(5) < 1
    {
        if spawnarea = 108
        instance_create(x+16,y+16,choose(Bush,Bush,Bush,BigFlower));
    }

}
