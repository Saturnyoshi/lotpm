with Player
{
if !instance_exists(SkillIcon) and (joy_u(KeyCont.gamepad[p]) > 0.3 or -joy_u(KeyCont.gamepad[p]) > 0.3 or joy_r(KeyCont.gamepad[p]) > 0.3 or -joy_r(KeyCont.gamepad[p]) > 0.3)
{
joyx = joy_u(KeyCont.gamepad[p])*view_hview*0.4
joyy = joy_r(KeyCont.gamepad[p])*view_hview*0.4
}
else if point_distance(x,y,x+joyx,y+joyy) > view_hview*0.4*0.3
{
joyx *=0.99
joyy *=0.99
}

if (instance_exists(enemy) or instance_exists(prop)) and UberCont.opt_autoaim > 0
{
if instance_exists(enemy)
{
if instance_exists(prop)
{
dix = instance_nearest(x+joyx,y+joyy,enemy)
diy = instance_nearest(x+joyx,y+joyy,prop)

if point_distance(x+joyx,y+joyy,dix.x,dix.y) < point_distance(x+joyx,y+joyy,diy.x,diy.y)
dir = dix
else
dir = diy
}
else
dir = instance_nearest(x+joyx,y+joyy,enemy)
}
else
dir = instance_nearest(x+joyx,y+joyy,prop)
aimdist = point_distance(x,y,x+joyx,y+joyy)*0.6
if point_distance(x+joyx,y+joyy,dir.x,dir.y) < aimdist
{
joyaimx = joyaimx-(joyaimx-(lengthdir_x(max(0,(point_distance(x+joyx,y+joyy,dir.x,dir.y)/aimdist)*point_distance(x+joyx,y+joyy,dir.x,dir.y)),point_direction(x+joyx,y+joyy,dir.x,dir.y))))*(0.08)*UberCont.opt_autoaim
joyaimy = joyaimy-(joyaimy-(lengthdir_y(max(0,(point_distance(x+joyx,y+joyy,dir.x,dir.y)/aimdist)*point_distance(x+joyx,y+joyy,dir.x,dir.y)),point_direction(x+joyx,y+joyy,dir.x,dir.y))))*(0.08)*UberCont.opt_autoaim
}else
{joyaimx *= 0.9
joyaimy *= 0.9}
}else
{joyaimx *= 0.9
joyaimy *= 0.9}

if !instance_exists(GenCont) {
var xx =(mouse_x-(mouse_x-(x+joyx+joyaimx))*0.6);
var yy =(mouse_y-(mouse_y-(y+joyy+joyaimy))*0.6);
xx=(xx-view_xview)/view_wview*view_wport
yy=(yy-view_yview)/view_hview*view_hport

window_mouse_set(xx,yy)
}
/*
if -joy_z(KeyCont.gamepad[p]) > 0.3
{
if keyfire = 1
keyfire = 2

if keyfire = 0
keyfire = 1
}
else
keyfire = 0*/
}
