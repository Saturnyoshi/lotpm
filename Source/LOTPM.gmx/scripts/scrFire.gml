if !instance_exists(Player)
exit;

if wep_type[wep] = 5 && instance_exists(Player) && Player.skill_got[17] = 1
with instance_create(x,y,Breath){sprite_index = sprLaserBrain; image_alpha = 1;}

can_shoot = 0
reload += wep_load[wep]
if !(Player.race = 7 and Player.skill_got[5] = 1 and random(5) < 1)
ammo[wep_type[wep]] -= wep_cost[wep]
//else
//snd_play_global(sndSteroidsUpg)

drawempty = 10

//GUNWARRANT
if Player.ultra_got[2] == 1 and Player.gunwarrant > 0 then
{ammo[wep_type[wep]] += wep_cost[wep]}

gunangle = argument0;

//REVOLVER
if wep = 1
{
snd_play_global(sndPistol)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//TRIPLE 
if wep = 2
{
snd_play_global(sndMachinegun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle-15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//SLEDGEHAMMER
if wep = 3
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 4
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//
if wep = 4
{
snd_play_global(sndMachinegun)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(12)-6)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 2
}

//SHOTGUN
if wep = 5
{
snd_play_global(sndShotgun)

repeat(7)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}


//CROSSBOW
if wep = 6
{
snd_play_global(sndCrossbow)

with instance_create(x,y,Bolt)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//NADER
if wep = 7
{
snd_play_global(sndGrenade)

with instance_create(x,y,Grenade)
{
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//DOUBLE SHOTGUN
if wep = 8
{
snd_play_global(sndShotgun)

repeat(14)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(50)-30)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

motion_add(other.gunangle+180,2)

BackCont.viewx2 += lengthdir_x(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 16
wkick = 8
}

//MINIGUN
if wep = 9
{
snd_play_global(sndMinigun)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(80)-40,3+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(26)-13)*other.accuracy,16)
image_angle = direction
team = other.team}
motion_add(other.gunangle+180,0.6)
BackCont.viewx2 += lengthdir_x(7,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(7,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//AUTO SHOTGUN
if wep = 10
{
snd_play_global(sndShotgun)

repeat(6)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 5
}

//AUTO CROSSBOW
if wep = 11
{
snd_play_global(sndCrossbow)

with instance_create(x,y,Bolt)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,24)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//SUPER CROSSBOW
if wep = 12
{
snd_play_global(sndCrossbow)

with instance_create(x,y,Bolt)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}
with instance_create(x,y,Bolt)
{motion_add(other.gunangle+5*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,Bolt)
{motion_add(other.gunangle-5*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,Bolt)
{motion_add(other.gunangle+10*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,Bolt)
{motion_add(other.gunangle-10*other.accuracy,24)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,1)

BackCont.viewx2 += lengthdir_x(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 14
wkick = 8
}


//SHOVEL
if wep = 13
{
snd_play_global(sndShovel)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//BAZOOKA
if wep = 14
{
snd_play_global(sndRocket)

with instance_create(x,y,Rocket)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,2)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

// STICKY NADER
if wep = 15
{
snd_play_global(sndGrenade)

with instance_create(x,y,Grenade)
{
sprite_index = sprStickyGrenade
sticky = 1
motion_add(other.gunangle+(random(6)-3)*other.accuracy,11)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//SMG
if wep = 16
{
snd_play_global(sndPistol)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(60)-30,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(32)-16)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 2
}


//ASSAULT RIFLE
if wep = 17
{
with instance_create(x,y,Burst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}


//DISC GUN
if wep = 18
{
snd_play_global(sndDiscgun)

with instance_create(x,y,Disc)
{motion_add(other.gunangle+(random(10)-5)*other.accuracy,5)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 6
wkick = 4
}


//LASER PISTOL
if wep = 19
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)
with instance_create(x,y,Laser)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(2)-1)*other.accuracy
team = other.team
event_perform(ev_alarm,0)}

BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}


//LASER RIFLE
if wep = 20
{
if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)
with instance_create(x,y,Laser)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(6)-3)*other.accuracy
team = other.team
event_perform(ev_alarm,0)}

BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}


//SLUGGER
if wep = 21
{
snd_play_global(sndSlugger)

with instance_create(x,y,Slug)
{motion_add(other.gunangle+(random(10)-5)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 8
}

//GATLING SLUGGER
if wep = 22
{
snd_play_global(sndSlugger)

with instance_create(x,y,Slug)
{motion_add(other.gunangle+(random(12)-6)*other.accuracy,18)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 8
}

//ASSAULT SLUGGER
if wep = 23
{
with instance_create(x,y,SlugBurst)
{
creator = other.id
ammo = 3
time = 3
team = other.team
event_perform(ev_alarm,0) 
}
}

//ENERGY SWORD
if wep = 24
{

if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),EnergySlash)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//SUPER SLUGGER
if wep = 25
{
snd_play_global(sndSlugger)

motion_add(other.gunangle+180,3)

with instance_create(x,y,Slug)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle+10*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle+20*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle-10*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle-20*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 15
wkick = 8
}

//HYPER RIFLE
if wep = 26
{
with instance_create(x,y,Burst)
{
creator = other.id
ammo = 6
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}



//MINES
/*
if wep = 27
{
snd_play_global(sndGrenade)

with instance_create(x,y,Mine)
{
motion_add(other.gunangle+(random(30)-15)*other.accuracy,4)
team = other.team}

BackCont.viewx2 += lengthdir_x(5,point_direction(x,y,mouse_x,mouse_y))
BackCont.viewy2 += lengthdir_y(5,point_direction(x,y,mouse_x,mouse_y))
wkick = 8
}*/

//SCREWDRIVER
if wep = 27
{
snd_play_global(sndScrewdriver)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),Shank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+(random(10)-5)*other.accuracy,3+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,4)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -8
}

//LASER MINIGUN
if wep = 28
{
if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)
with instance_create(x,y,Laser)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(16)-8)*other.accuracy
team = other.team
event_perform(ev_alarm,0)}

BackCont.viewx2 += lengthdir_x(5,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(5,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 8

motion_add(other.gunangle+180,0.6)
}


//BLOOD NADER
if wep = 29
{
snd_play_global(sndBloodLauncher)

with instance_create(x,y,BloodGrenade)
{
sticky = 0
motion_add(other.gunangle+(random(12)-6)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(5,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(5,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 4
}

//SPLINTER GUN
if wep = 30
{
snd_play_global(sndSplinterGun)

repeat(4)
{
with instance_create(x,y,Splinter)
{motion_add(other.gunangle+(random(20)-10)*other.accuracy,20+random(4))
image_angle = direction
team = other.team}
with instance_create(x,y,Splinter)
{motion_add(other.gunangle+(random(10)-5)*other.accuracy,20+random(4))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick -= 3
}


//TOXIC BOW
if wep = 31
{
snd_play_global(sndCrossbow)

with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle,22)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//SENTRY GUN
if wep = 32
{
snd_play_global(sndGrenade)

with instance_create(x,y,SentryGun)
{
sticky = 0
motion_add(other.gunangle,6)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(5,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(5,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake

wkick = -10
}

//WAVE GUN
if wep = 33
{
with instance_create(x,y,WaveBurst)
{
creator = other.id
ammo = 7
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//PLASMA GUN
if wep = 34
{
if skill_got[17] = 1
snd_play_global(sndPlasmaUpg)
else
snd_play_global(sndPlasma)

with instance_create(x,y,PlasmaBall)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,2)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,3)
BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 5
}

//PLASMA CANNON
if wep = 35
{
if skill_got[17] = 1
snd_play_global(sndPlasmaBigUpg)
else
snd_play_global(sndPlasmaBig)

with instance_create(x,y,PlasmaBig)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,2)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,6)
BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 10
}

//ENERGY HAMMER
if wep = 36
{

if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),EnergyHammerSlash)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*2
motion_add(other.gunangle,1+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 2
wkick = -3
}

//JACKHAMMER
if wep = 37
{
with instance_create(x,y,SawBurst)
{
creator = other.id
ammo = 12
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//FLAK CANNON
if wep = 38
{
snd_play_global(sndFlakCannon)

with instance_create(x,y,FlakBullet)
{
motion_add(other.gunangle+(random(12)-6)*other.accuracy,11+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}


//GOLDEN REVOLVER
if wep = 39
{
snd_play_global(sndPistol)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//GOLDEN HAMMER
if wep = 40
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 4
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 2
wkick = -6
}


//GOLDEN 
if wep = 41
{
snd_play_global(sndMachinegun)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}


//GOLDEN SHOTGUN
if wep = 42
{
snd_play_global(sndShotgun)

repeat(8)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 8
}



//GOLDEN CROSSBOW
if wep = 43
{
snd_play_global(sndCrossbow)

with instance_create(x,y,Bolt)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(44,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(44,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 6
wkick = 6
}

//GOLDEN NADER
if wep = 44
{
snd_play_global(sndGrenade)

with instance_create(x,y,Grenade)
{
sprite_index = sprGoldGrenade
sticky = 0
motion_add(other.gunangle,12)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}

//GOLDEN LASER PISTOL
if wep = 45
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)
with instance_create(x,y,Laser)
{image_angle = point_direction(x,y,mouse_x,mouse_y)
team = other.team
event_perform(ev_alarm,0)}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}


//CHICKEN SWORD
if wep = 46
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

ang = point_direction(x,y,mouse_x,mouse_y)
move_contact_solid(ang,16)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,ang),y+lengthdir_y(Player.skill_got[13]*20,ang),Slash)
{
ang = other.ang
dmg = 3
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(ang,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
speed = -speed/2
BackCont.viewx2 += lengthdir_x(8,ang)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,ang)*UberCont.opt_shake
BackCont.shake += 1
wkick = -6
}


//NUKE LAUNCHER
if wep = 47
{
snd_play_global(sndRocket)

with instance_create(x,y,Nuke)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,2)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 10
}

//ION CANNON
if wep = 48
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)
with instance_create(x,y,IonBurst)
{
creator = other.id
ammo = 10
time = 1
team = other.team
alarm[0] = 30
}

BackCont.shake += 6
wkick = 3
}


//QUADRUPLE 
if wep = 49
{
snd_play_global(sndMachinegun)

repeat(4)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,4+random(3))
}

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+6*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle-6*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+18*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle-18*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 6
wkick = 8
}

//FLAMETHROWER
if wep = 50
{
if !instance_exists(FlameSound)
instance_create(x,y,FlameSound)
with instance_create(x,y,FlameBurst)
{
creator = other.id
ammo = 6
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//DRAGON
if wep = 51
{
if !instance_exists(DragonSound)
instance_create(x,y,DragonSound)
with instance_create(x,y,DragonBurst)
{
creator = other.id
ammo = 3
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}


//FLARE GUN
if wep = 52
{
snd_play_global(sndFlare)

with instance_create(x,y,Flare)
{
sticky = 0
motion_add(other.gunangle+(random(14)-7)*other.accuracy,9)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 5
}


//ENERGY SCREWDRIVER
if wep = 53
{
snd_play_global(sndScrewdriver)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),EnergyShank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+(random(10)-5)*other.accuracy,3+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,5)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 2
wkick = -8
}


//HYPER LAUNCHER
if wep = 54
{
snd_play_global(sndGrenade)

with instance_create(x,y,HyperGrenade)
{
direction = point_direction(x,y,mouse_x,mouse_y)+(random(4)-2)*other.accuracy
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 8
}

//LASER CANNON
if wep = 55
{


if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)

with instance_create(x,y,LaserCannon)
{
creator = other.id
ammo = 5+other.skill_got[17]*2
time = 1
team = other.team
alarm[0] = 20
}
}


//RUSTY REVOLVER
if wep = 56
{
snd_play_global(sndPistol)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}


//LIGHTNING PISTOL
if wep = 57
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)


with instance_create(x,y,Lightning)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(30)-15)*other.accuracy
team = other.team
ammo = 14
event_perform(ev_alarm,0)
visible = 0
with instance_create(x,y,LightningSpawn)
image_angle = other.image_angle}

BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//LIGHTNING RIFLE
if wep = 58
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)


with instance_create(x,y,Lightning)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(6)-3)*other.accuracy
team = other.team
ammo = 30
event_perform(ev_alarm,0)
visible = 0
with instance_create(x,y,LightningSpawn)
image_angle = other.image_angle}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 8
}


//LIGHTNING SHOTGUN
if wep = 59
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)

repeat(8)
{
with instance_create(x,y,Lightning)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(180)-60)*other.accuracy
team = other.team
ammo = 9+random(3)
event_perform(ev_alarm,0)
visible = 0
with instance_create(x,y,LightningSpawn)
image_angle = other.image_angle}}


BackCont.viewx2 += lengthdir_x(4,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(4,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 5
}

//***************LOTPM WEAPONS**********************************************
//***************LOTPM WEAPONS**********************************************
//***************LOTPM WEAPONS**********************************************
//***************LOTPM WEAPONS**********************************************
//***************LOTPM WEAPONS**********************************************
//***************LOTPM WEAPONS**********************************************
//***************LOTPM WEAPONS**********************************************

//TOASTER
if wep = 60
{
snd_play_global(sndShotgun);
repeat(100)
{
    with(instance_create(x,y,Flame))
    {
        motion_add(other.gunangle+(random(2)-1)*other.accuracy,1+random(16));
        image_angle = direction;
        team = other.team;
    }
}
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.shake += 8;
wkick = 6;
}

//DISC MINIGUN
if wep = 61
{
snd_play_global(sndCrossbow);
{
    with(instance_create(x,y,Disc))
    {
        motion_add(other.gunangle+(random(40)-20)*other.accuracy,8);
        image_angle = direction;
        team = other.team;
    }
}
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.shake += 8;
wkick = 6;
}

//LIGHTNING LAUNCHER
if wep = 62
{
snd_play_global(sndGrenade);
{
    with(instance_create(x,y,LightningGrenade))
    {
        motion_add(other.gunangle+(random(5)-2.50)*other.accuracy,8);
        image_angle = direction;
        team = other.team;
    }
}
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.shake += 8;
wkick = 6;
}

//FLARE BOW
if wep = 63
{
snd_play_global(sndCrossbow);
{
    with(instance_create(x,y,FlareBolt))
    {
        motion_add(other.gunangle+(random(5)-2.50)*other.accuracy,32);
        image_angle = direction;
        team = other.team;
    }
}
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake;
BackCont.shake += 8;
wkick = 6;
}

//CARABIN
if wep = 64
{
snd_play_global(sndShotgun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

repeat(15){
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle,30)
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//BULLET SHOTGUN
if wep = 65
{
snd_play_global(sndShotgun)

repeat(5+(random(3)))
{
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//NAIL GUN
if wep = 66
{
snd_play_global(sndCrossbow)
with instance_create(x,y,Nail)
{motion_add(other.gunangle,8)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 6
}

//LASER SHOTGUN
if wep = 67
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)
repeat(5+random(2)) {with instance_create(x,y,Laser)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(40)-20)*other.accuracy
team = other.team
event_perform(ev_alarm,0)}}

BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 6
wkick = 2
}

//DISC CANNON
if wep = 68
{
snd_play_global(sndDiscgun)
with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),SuperDisc)
{motion_add(other.gunangle,8)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 6
}

//SCYTHE
if wep = 69
{
snd_play_global(sndScrewdriver)

instance_create(x,y,Dust)


with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),Shank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+(random(10)-5)*other.accuracy,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),Shank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+36,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),Shank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-36,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),Shank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-72,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),Shank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+72,3+longarms)
image_angle = direction
team = other.team}


wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}


//ENERGY SCYTHE
if wep = 70
{
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),EnergyShank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),EnergyShank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+20,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),EnergyShank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-20,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),EnergyShank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-50,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*10,point_direction(x,y,mouse_x,mouse_y)),EnergyShank)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+50,3+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//SUPER DISC CANNON
if wep = 71
{
snd_play_global(sndDiscgun)
with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),HyperDisc)
{motion_add(other.gunangle,5)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//PLASMA PISTOL
if wep = 72
{
snd_play_global(sndPlasma)

with instance_create(x,y,PlasmaBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SAWED OFF 
if wep = 73
{
snd_play_global(sndMachinegun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+13*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle-13*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//GRENADE PISTOL
if wep = 74
{
snd_play_global(sndGrenade)

with instance_create(x,y,BulletGrenade)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//CAR LAUNCHER
if wep = 75
{
snd_play_global(sndRocket)

if random(100)<1 then
{
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),CarThrowYv)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,2)
image_angle = direction
image_speed = 0.5
team = other.team}
}
else
{
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),CarThrow)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,2)
image_angle = direction
image_speed = 0.5
team = other.team}
}
BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//ASSAULT SHOTGUN
if wep = 76
{
with instance_create(x,y,ShotgunBurst)
{
creator = other.id
ammo = choose(3,4,5)
time = 2
team = other.team
event_perform(ev_alarm,0) 
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//PLASMA SWORD
if wep = 77
{

if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x,y,PlasmaSlash)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//NAIL CANNON
if wep = 78
{
snd_play_global(sndCrossbow)

with instance_create(x,y,NailCannonBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,5)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SUPER PLASMA CANNON
if wep = 79
{
if skill_got[17] = 1
snd_play_global(sndPlasmaBigUpg)
else
snd_play_global(sndPlasmaBig)

with instance_create(x,y,PlasmaBigger)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,1)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,6)
BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 10
}

//BROOM
if wep = 80
{
snd_play_global(sndShotgun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

with instance_create(x,y,Bullet2)
{motion_add(point_direction(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),mouse_x,mouse_y)+(random(6)-3)*other.accuracy,5)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),Bullet2)
{motion_add(other.gunangle,10)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),Bullet2)
{motion_add(other.gunangle+15,10)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),Bullet2)
{motion_add(other.gunangle-15,10)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),Bullet2)
{motion_add(other.gunangle-30,10)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),Bullet2)
{motion_add(other.gunangle+30,10)
image_angle = direction
team = other.team}
BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//BURNER
if wep = 81
{
snd_play_global(sndRocket)
with instance_create(x,y,Meteorite)
{
motion_add(other.gunangle,6)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//GRENADE 
if wep = 82
{
snd_play_global(sndGrenade)

with instance_create(x,y,BulletGrenade)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//GRENADE RIFLE
if wep = 83
{
with instance_create(x,y,GrenadeBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//SUPER SHUFFLE GUN
if wep = 84
{
wep_type[wep] = irandom(4)+1
snd_play_global(choose(sndGrenade,sndPistol,sndShotgun,sndPlasmaBig,sndCrossbow))

with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),
choose(choose(Rocket,Grenade,PopoNade,PlasmaBig,PlasmaBig),Slug,SlugBurst,PlasmaBall,PlasmaBall,CarThrow,FlakBullet,Burst,WaveBurst,DragonBurst,Burst,Nuke,WaveBurst,Bolt,ToxicBolt))
{
motion_add(other.gunangle+(random(8)-4)*other.accuracy,12)
image_angle = direction
team = other.team
if object_index = DragonBurst then {creator = other.id; ammo = 4; time = 1; team = other.team; event_perform(ev_alarm,0) }
if object_index = Burst then {creator = other.id; ammo = 6; time = 1; team = other.team; event_perform(ev_alarm,0) }
if object_index = SlugBurst then {creator = other.id; ammo = 3; time = 1; team = other.team; event_perform(ev_alarm,0) }
if object_index = WaveBurst then {creator = other.id; ammo = 7; time = 1; team = other.team; event_perform(ev_alarm,0) }
if object_index = Grenade then {sticky = 0;}
if object_index = Rocket then {speed = 8;}
if object_index = Bolt or object_index = ToxicBolt then {speed = 24;}
if object_index = Laser then {event_perform(ev_alarm,0)}
}
BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SUPER ASSAULT RIFLE
if wep = 85
{
with instance_create(x,y,Burst)
{
creator = other.id
ammo = 15
time = 1
team = other.team
event_perform(ev_alarm,0) 
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//SUPER WAVE GUN
if wep = 86
{
with instance_create(x,y,WaveBurst)
{
creator = other.id
ammo = 20
time = 2
team = other.team
event_perform(ev_alarm,0) 
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//TSUNAMI GUN
if wep = 87
{
with instance_create(x,y,TsunamiBurst)
{
creator = other.id
ammo = 40
time = 2
team = other.team
event_perform(ev_alarm,0) 
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//ROCKET LAUNCHER
if wep = 88
{
snd_play_global(sndRocket)
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),JockRocket)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,4)
friction = -0.25
image_angle = direction
image_speed = 0.5
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//ASSAULT GRENADE LAUNCHER
if wep = 89
{
with instance_create(x,y,GrenadeBurst2)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//BRATATATATA
if wep = 90
{
snd_play_global(sndMachinegun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

with instance_create(x,y,Bullet2)
{motion_add(point_direction(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),mouse_x,mouse_y)+(random(6)-3)*other.accuracy,5)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),BratataProjectile)
{motion_add(other.gunangle,16)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),BratataProjectile)
{motion_add(other.gunangle+15,16)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),BratataProjectile)
{motion_add(other.gunangle-15,16)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),BratataProjectile)
{motion_add(other.gunangle-30,16)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),BratataProjectile)
{motion_add(other.gunangle+30,16)
image_angle = direction
team = other.team}
BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//AUTO TOXIC BOW
if wep = 91
{
snd_play_global(sndCrossbow)

with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle,22)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//SUPER TOXIC BOW
if wep = 92
{
snd_play_global(sndCrossbow)

with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}
with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle+5*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle-5*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle+10*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,ToxicBolt)
{motion_add(other.gunangle-10*other.accuracy,24)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,1)

BackCont.viewx2 += lengthdir_x(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 14
wkick = 8
}

//PLASMA MACHINEGUN
if wep = 93
{
snd_play_global(sndPlasma)

with instance_create(x,y,PlasmaBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//PLASMA ASSAULT RIFLE
if wep = 94
{
with instance_create(x,y,PlasmaBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}

//UZI
if wep = 95
{
snd_play_global(sndMachinegun)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(12)-6)*other.accuracy,25)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//MEAT GRINDER
if wep = 96
{
with instance_create(x,y,GrinderBurst)
{
creator = other.id
ammo = 48
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//BONE BREAKER
if wep = 97
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
image_yscale += 0.5;
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//MACE CLEAVER
if wep = 98
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 1
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
image_yscale -= 0.15;
image_xscale -= 0.15;
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//PLASMA SHOVEL
if wep = 99
{
snd_play_global(sndLaserSwordUpg)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),PlasmaSlash)
{
image_xscale -= 0.5
dmg = 3
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),PlasmaSlash)
{
image_xscale -= 0.5
dmg = 3
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),PlasmaSlash)
{
image_xscale -= 0.5
dmg = 3
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//PLASMA HAMMER
if wep = 100
{

if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x,y,PlasmaSlash)
{
image_yscale = 1.33
image_xscale -= 0.33
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

with instance_create(x,y,PlasmaSlash)
{
image_yscale = 1.33
image_xscale -= 0.33
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+180,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//PLASMA SCREWDRIVER
if wep = 101
{

if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x,y,PlasmaSlash)
{
image_xscale -= 0.6
image_yscale -= 0.6
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//RICOCHET GUN
if wep = 102
{
snd_play_global(sndMinigun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,RicochetBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//TRIPLE RICOCHET GUN
if wep = 103
{
snd_play_global(sndMinigun)

repeat(3) {with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))}

with instance_create(x,y,RicochetBullet)
{motion_add(other.gunangle+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,RicochetBullet)
{motion_add(other.gunangle+15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,RicochetBullet)
{motion_add(other.gunangle-15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//RICOCHET CANNON
if wep = 104
{
snd_play_global(sndMinigun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,RicochetCannonBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//WRECKER
if wep = 105
{
snd_play_global(sndRoll)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,WreckerProjectile)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//AUTO FLARE BOW
if wep = 106
{
snd_play_global(sndCrossbow)

with instance_create(x,y,FlareBolt)
{motion_add(other.gunangle,22)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//SUPER FLARE BOW
if wep = 107
{
snd_play_global(sndCrossbow)

with instance_create(x,y,FlareBolt)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}
with instance_create(x,y,FlareBolt)
{motion_add(other.gunangle+5*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,FlareBolt)
{motion_add(other.gunangle-5*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,FlareBolt)
{motion_add(other.gunangle+10*other.accuracy,24)
image_angle = direction
team = other.team}
with instance_create(x,y,FlareBolt)
{motion_add(other.gunangle-10*other.accuracy,24)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,1)

BackCont.viewx2 += lengthdir_x(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 14
wkick = 8
}

//BUG ZAPPER
if wep = 108
{
if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),LightningSlash)
{
image_xscale -= 0.33
damage = 14
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),LightningSlash)
{
image_xscale -= 0.33
damage = 14
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),LightningSlash)
{
image_xscale -= 0.33
damage = 14
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//ULTRA DISC GUN
if wep = 109
{

if Player.rad > floor(6-(6*(0.238*Player.ultra_got[60]))) then
{
snd_play_global(sndDiscgun)
Player.rad -= floor(6-(6*(0.238*Player.ultra_got[60])))
with instance_create(x,y,UltraDisc)
{motion_add(other.gunangle+(random(10)-5)*other.accuracy,5)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 6
wkick = 4
}

else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}

}

//SHIELDED CROSSBOW
if wep = 110
{
snd_play_global(sndCrossbow)

repeat(20) with 
instance_create(x,y,Bolt) {
motion_add(other.shielddir,16); image_angle = direction; team = other.team; friction = -0.06;
other.shielddir += 360/20
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED REVOLVER
if wep = 111
{
snd_play_global(sndPistol)

repeat(26) with 
instance_create(x,y,Bullet1) {
motion_add(other.shielddir,12); image_angle = direction; team = other.team;
other.shielddir += 360/26
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED BAZOOKA
if wep = 112
{
snd_play_global(sndRocket)

repeat(10) with 
instance_create(x,y,Rocket) {
motion_add(other.shielddir,2); image_angle = direction; team = other.team;
other.shielddir += 360/10
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED PLASMA GUN
if wep = 113
{
snd_play_global(sndPlasma)

repeat(9) with 
instance_create(x,y,PlasmaBall) {
motion_add(other.shielddir,10); image_angle = direction; team = other.team;
other.shielddir += 360/9
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED DISC GUN
if wep = 114
{
snd_play_global(sndDiscgun)

repeat(15) with 
instance_create(x,y,Disc) {
motion_add(other.shielddir,4); image_angle = direction; team = other.team;
other.shielddir += 360/15
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED FLAK
if wep = 115
{
snd_play_global(sndFlakCannon)

repeat(9) with 
instance_create(x,y,FlakBullet) {
motion_add(other.shielddir,12); image_angle = direction; team = other.team;
other.shielddir += 360/9
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED SLUG
if wep = 116
{
snd_play_global(sndShotgun)

repeat(11) with 
instance_create(x,y,Slug) {
motion_add(other.shielddir,12); image_angle = direction; team = other.team;
other.shielddir += 360/11
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED RICOCHET
if wep = 117
{
snd_play_global(sndShotgun)

repeat(6) with 
instance_create(x,y,RicochetBullet) {
motion_add(other.shielddir,12); image_angle = direction; team = other.team;
other.shielddir += 360/6
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED GRENADE
if wep = 118
{
snd_play_global(sndGrenade)

repeat(8) with 
instance_create(x,y,Grenade) {
sticky = 0;
motion_add(other.shielddir,8); image_angle = direction; team = other.team;
other.shielddir += 360/8
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED mini GRENADE
if wep = 119
{
snd_play_global(sndGrenade)

repeat(12) with 
instance_create(x+lengthdir_x(10,shielddir),y+lengthdir_y(10,shielddir),BulletGrenade) {
sticky = 0;
motion_add(other.shielddir,8); image_angle = direction; team = other.team;
other.shielddir += 360/12
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//FLAMETHROWER SHIELD
if wep = 120
{
repeat(30) with 
instance_create(x,y,Flame)
{
creator = other.id
ammo = 6
time = 1
motion_add(other.shielddir,8);
team = other.team
event_perform(ev_alarm,0)
other.shielddir += 360/30
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 1
wkick = 2
}

//SHIELDED SHOTGUN
if wep = 121
{
snd_play_global(sndShotgun)

repeat(8)
{
other.shielddir += 360/8
repeat(7)
{
with instance_create(x,y,Bullet2)
{motion_add(other.shielddir+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED FLARE GUN
if wep = 122
{
snd_play_global(sndFlare)

repeat(13) with 
instance_create(x+lengthdir_x(10,shielddir),y+lengthdir_y(10,shielddir),Flare) {
sticky = 0;
motion_add(other.shielddir,8); image_angle = direction; team = other.team;
other.shielddir += 360/13
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHIELDED JACKHAMMER
if wep = 123
{
repeat(18)
{
with instance_create(x,y,SJBurst)
{
creator = other.id
ammo = 48
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
shielddir+=360/18
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//BOLT SHOTGUN
if wep = 124
{

repeat(choose(5,7,8))
{
with instance_create(x,y,Bolt)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,16+random(8))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//FLAK RIFLE
if wep = 125
{
with instance_create(x,y,FlakBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}

//FLAK PISTOL
if wep = 126
{
snd_play_global(sndFlakCannon)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,MiniFlakBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//FLAK MACHINEGUN
if wep = 127
{
snd_play_global(sndFlakCannon)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,MiniFlakBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//ASSAULT FLAK RIFLE
if wep = 128
{
with instance_create(x,y,MiniFlakBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}

//ARROW CANNON
if wep = 129
{
snd_play_global(sndCrossbow)

with instance_create(x,y,BoltCannon)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,1)

BackCont.viewx2 += lengthdir_x(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(60,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 14
wkick = 8
}

//BLUDGEON
if wep = 130
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
image_xscale = 1.1
dmg = 16
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//SUPER NAIL GUN
if wep = 131
{
with instance_create(x,y,NailBurst)
{
creator = other.id
ammo = choose(3,4,5)
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//venom pistol
if wep = 132
{
snd_play_global(sndScorpionHit)

with instance_create(x,y,VenomBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//venom MACHINEGUN
if wep = 133
{
snd_play_global(sndScorpionHit)

with instance_create(x,y,VenomBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//SUPER NAIL GUN
if wep = 134
{
with instance_create(x,y,VenomBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}

//venom CANNON
if wep = 135
{
snd_play_global(sndFlare)

with instance_create(x,y,VenomCannon)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,8)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//CROSSBOW
if wep = 136
{
snd_play_global(sndCrossbow)

with instance_create(x,y,SpearProjectile)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//VENOM SHOTGUN
if wep = 137
{
snd_play_global(sndShotgun)

repeat(choose(7,8,9))
{
with instance_create(x,y,VenomBullet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//PLASMA SHOTGUN
if wep = 138
{
snd_play_global(sndPlasmaUpg)

repeat(choose(7,8,9))
{
with instance_create(x,y,PlasmaBall)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//PLASMA LAUNCHER
if wep = 139
{
snd_play_global(sndPlasmaUpg)

with instance_create(x,y,PlasmaGrenade)
{
direction = point_direction(x,y,mouse_x,mouse_y)+(random(4)-2)*other.accuracy
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 8
}

//DOUBLE REVOLVER
if wep = 140
{
snd_play_global(sndPistol)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+180+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}


BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//REVERSE GUN
if wep = 141
{
snd_play_global(sndPistol)

with instance_create(x,y,ReverseBullet)
{
direction = point_direction(x,y,mouse_x,mouse_y)+(random(4)-2)*other.accuracy
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 8
}

//PLASMA SHOTGUN
if wep = 142
{
snd_play_global(sndPlasmaUpg)

repeat(choose(7,8,9))
{
with instance_create(x,y,PlasmaBig)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//SHUFFLE GUN
if wep = 143
{
wep_type[wep] = irandom(4)+1
snd_play_global(choose(sndGrenade,sndPistol,sndShotgun,sndPlasmaBig,sndCrossbow))

with instance_create(x+lengthdir_x(16,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(16,point_direction(x,y,mouse_x,mouse_y)),
choose(Slug,SlugBurst,Burst,WaveBurst,ShotgunBurst,Burst,WaveBurst,))
{
motion_add(other.gunangle+(random(8)-4)*other.accuracy,12)
image_angle = direction
team = other.team
if object_index = ShotgunBurst then {creator = other.id; ammo = choose(1,1,1,1,1,1,2); time = 2; team = other.team; event_perform(ev_alarm,0) }
if object_index = Burst then {creator = other.id; ammo = choose(2,3,4,5); time = 2; team = other.team; event_perform(ev_alarm,0) }
if object_index = SlugBurst then {creator = other.id; ammo = choose(1,2); time = 1; team = other.team; event_perform(ev_alarm,0)}
if object_index = WaveBurst then {creator = other.id; ammo = choose(7,8,9,10); time = 2; team = other.team; event_perform(ev_alarm,0)}
}
BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//LHC
if wep = 144
{
snd_play_global(sndRocket)
with instance_create(x,y,LHCProjectile)
{
motion_add(other.gunangle,6)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 8
}

//SKORPION
if wep = 145
{
snd_play_global(sndScorpionFire)
repeat(choose(0,1,2)) with instance_create(x,y,Bullet1)
{
sprite_index = sprScorpionBullet
image_xscale = 1.3
image_yscale = 1.3
motion_add(other.gunangle+random(40)-20,5+random(3))
image_angle = direction
team = other.team
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake = 3
wkick = 8
}

//PIPE
if wep = 146
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = random(8)
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//BROOM
if wep = 147
{
snd_play_global(sndShotgun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

shootangle = point_direction(x,y,mouse_x,mouse_y)-30

repeat(5)
with instance_create(x,y,Bullet22)
{
motion_add(other.shootangle,15)
image_angle = direction
team = other.team
other.shootangle += 10
}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//FIREBALLER RIFLE
if wep = 148
{
with instance_create(x,y,FireballerBurst)
{
creator = other.id
ammo = 2+choose(3,4)
time = 3
team = other.team
event_perform(ev_alarm,0) 
}
}

//HEAVY REVOLVER
if wep = 149
{
snd_play_global(sndHeavyRevolver)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 2
}

//HEAVY MGUN
if wep = 150
{
snd_play_global(sndHeavyMachinegun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 2
}

//HEAVY ASSAULT RIFLE
if wep = 151
{
with instance_create(x,y,HeavyBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}

//SAWED SHOTGUN
if wep = 152
{
snd_play_global(sndShotgun)

repeat(17)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(90)-45)*other.accuracy,6+random(6))
image_angle = direction
team = other.team}
}

motion_add(other.gunangle+180,2)

BackCont.viewx2 += lengthdir_x(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 16
wkick = 8
}

//ERASER
if wep = 153
{
snd_play_global(sndEraser)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

repeat(17)
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle,4+random(16))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SUPER FLAK CANNON
if wep = 154
{
snd_play_global(sndSuperFlakCannon)

with instance_create(x,y,SuperFlakBullet)
{
motion_add(other.gunangle+(random(12)-6)*other.accuracy,11+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}

//POP GUN
if wep = 155
{
snd_play_global(sndPopGun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//POP RIFLE
if wep = 156
{
with instance_create(x,y,Bullet2Burst)
{
creator = other.id
ammo = 3
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//FLAME SHOTGUN
if wep = 157
{
snd_play_global(sndShotgun)

repeat(6)
{
with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//DB FLAME SHOTGUN
if wep = 158
{
snd_play_global(sndShotgun)

repeat(14)
{
with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//AUTO FLAME SHOTGUN
if wep = 159
{
snd_play_global(sndShotgun)

repeat(6)
{
with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//INCI 
if wep = 160
{
snd_play_global(sndIncinerator)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle+15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle-15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//BOUNCY SMG
if wep = 161
{
snd_play_global(sndBouncerSMG)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,BouncyBullet)
{motion_add(other.gunangle+(random(32)-16)*other.accuracy,4)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//BOUNCY SHOTTY
if wep = 162
{
snd_play_global(sndBouncerShotgun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

shootangle = point_direction(x,y,mouse_x,mouse_y)-60

repeat(7)
with instance_create(x,y,BouncyBullet)
{
motion_add(other.shootangle,4)
image_angle = direction
team = other.team
other.shootangle += 10
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 2
}

//HYPER SLUG
if wep = 163
{
snd_play_global(sndSlugger)

with instance_create(x,y,HyperSlug)
{
direction = point_direction(x,y,mouse_x,mouse_y)+(random(4)-2)*other.accuracy
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 8
}

//heavy SLUGGER
if wep = 164
{
snd_play_global(sndSlugger)

with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle+(random(10)-5)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 8
}

//HEAVY CROSSBOW
if wep = 165
{
snd_play_global(sndCrossbow)

with instance_create(x,y,HeavyBolt)
{motion_add(other.gunangle,24)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//AUTO HEAVY CROSSBOW
if wep = 166
{
snd_play_global(sndCrossbow)

with instance_create(x,y,HeavyBolt)
{motion_add(other.gunangle+random(20)-10*other.accuracy,24)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(40,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//SPLINTER PISTOL
if wep = 167
{
snd_play_global(sndSplinterGun)

repeat(4)
{
with instance_create(x,y,Splinter)
{motion_add(other.gunangle+(random(12)-6)*other.accuracy,20+random(4))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick -= 3
}

//SUPER SPLINTER GUN
if wep = 168
{
with instance_create(x,y,SplinterBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}


//HEVNADER
if wep = 169
{
snd_play_global(sndGrenade)

with instance_create(x,y,BigGrenade)
{
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//CLUSTER
if wep = 170
{
snd_play_global(sndGrenade)

with instance_create(x,y,Cluster)
{
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//G. BAZOOKA
if wep = 171
{
snd_play_global(sndRocket)

with instance_create(x,y,Rocket)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,2)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//SUPER BAZOOKA
if wep = 172
{
snd_play_global(sndRocket)

shootangle = point_direction(x,y,mouse_x,mouse_y)-30

repeat(5)
{
with instance_create(x,y,Rocket)
    {
motion_add(other.shootangle,3)
image_angle = direction
team = other.team
other.shootangle += 10
    }
}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10

}

//BLOOD LAUNCHER
if wep = 173
{
snd_play_global(sndGrenade)

with instance_create(x,y,BloodCannon)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,8)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//FLAME LAUNCHER
if wep = 174
{
snd_play_global(sndFlameCannon)

with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),FlameCannonProjectile)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,3)
image_angle = direction
friction = 0.025
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//PLASMA RIFLE
if wep = 175
{
if skill_got[17] = 1
snd_play_global(sndPlasmaUpg)
else
snd_play_global(sndPlasma)

with instance_create(x,y,PlasmaBall)
{motion_add(other.gunangle+(random(14)-7)*other.accuracy,2)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,3)
BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 5
}

//PLASMA MINIGUN
if wep = 176
{
if skill_got[17] = 1
snd_play_global(sndPlasmaUpg)
else
snd_play_global(sndPlasma)

with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),PlasmaBall)
{motion_add(other.gunangle+(random(35)-28)*other.accuracy,2)
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,3)
BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 5
}

//LIGHTNING SMG
if wep = 177
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)


with instance_create(x,y,Lightning)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(30)-15)*other.accuracy
team = other.team
ammo = 14
event_perform(ev_alarm,0)
visible = 0
with instance_create(x,y,LightningSpawn)
image_angle = other.image_angle}

BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//LIGHTNING CAANON
if wep = 178
{
snd_play_global(sndPlasma)

with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),LightningCannonProjectile)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,3)
image_angle = direction
friction = 0.025
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//SLEDGEHAMMER
if wep = 179
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),BloodSlash)
{
dmg = 4
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//BUG ZAPPER
if wep = 180
{

if skill_got[17] = 1
snd_play_global(sndLaserSwordUpg)
else
snd_play_global(sndLaserSword)
instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),LightningSlash2)
{
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,2+longarms)
image_angle = direction
team = other.team}


wepangle = -wepangle
motion_add(other.gunangle,7)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//U REVOLVER
if wep = 181
{

    if Player.rad>floor(4-(4*(0.238*Player.ultra_got[60])))
    {
        with instance_create(x,y,UltraBullet1)
        {
        snd_play_global(sndPistol)
        Player.rad -= floor(4-(4*(0.238*Player.ultra_got[60])))
        motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
        image_angle = direction
        team = other.team}
        
        BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
        BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
        BackCont.shake += 4
        other.wkick = 2
    }
    else
    {
        Player.reload = 1
        Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
        if instance_exists(PopupText) = 0
        dir = instance_create(x,y,PopupText)
        dir.mytext = "NOT ENOUGH RADS"
    }
}

//U SHOTTY
if wep = 182
{

if Player.rad>floor(14-(14*(0.238*Player.ultra_got[60])))
    {
snd_play_global(sndUltraShotgun)
Player.rad -= floor(14-(14*(0.238*Player.ultra_got[60])))
repeat(9)
{
with instance_create(x,y,UltraBullet2)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,16+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
other.wkick = 2
        }
else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}
}

//SEEKER PISTOL
if wep = 183
{
snd_play_global(sndSplinterGun)

with instance_create(x,y,Seeker)
{motion_add(other.gunangle+12+(random(8)-4)*other.accuracy,6)
image_angle = direction
team = other.team}

with instance_create(x,y,Seeker)
{motion_add(other.gunangle-12+(random(8)-4)*other.accuracy,6)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SEEKER shotgun
if wep = 184
{
snd_play_global(sndSplinterGun)

repeat(6)
with instance_create(x,y,Seeker)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,6+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//UCROSSBOW
if wep = 185
{
if Player.rad>floor(12-(12*(0.238*Player.ultra_got[60])))
    {
snd_play_global(sndCrossbow)
Player.rad -= floor(12-(12*(0.238*Player.ultra_got[60])))
repeat(1)
{
with instance_create(x,y,UltraBolt)
{motion_add(other.gunangle+random(5)-2.5*other.accuracy,24)
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
other.wkick = 2
        }
else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}
}

//ULEANCHE
if wep = 186
{
if Player.rad>floor(16-(16*(0.238*Player.ultra_got[60])))
    {
snd_play_global(sndCrossbow)
Player.rad -= floor(16-(16*(0.238*Player.ultra_got[60])))
with instance_create(x,y,UltraGrenade)
{
sticky = 0
motion_add(other.gunangle,12)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
other.wkick = 2
        }
else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}
}

//ULASER
if wep = 187
{
if Player.rad>16
    {
snd_play_global(sndLaser)
Player.rad -= 16

shootangle = point_direction(x,y,mouse_x,mouse_y)-30

repeat(5)
{
with instance_create(x,y,Laser)
    {
motion_add(other.shootangle,3)
image_angle = direction
team = other.team
other.shootangle += 10
event_perform(ev_alarm,0)
    }
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
other.wkick = 2
        }
else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}
}

//USHOVL
if wep = 188
{
if Player.rad>floor(14-(14*(0.238*Player.ultra_got[60])))
    {
Player.rad -= floor(14-(14*(0.238*Player.ultra_got[60])))

snd_play_global(sndUltraShovel)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
sprite_index = sprUltraSlash
dmg = 30
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,5+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),Slash)
{
sprite_index = sprUltraSlash
dmg = 30
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+60*other.accuracy,5+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),Slash)
{
sprite_index = sprUltraSlash
dmg = 30
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-60*other.accuracy,5+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
other.wkick = 2
        }
else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}
}

//GRENADE SHOTGUN
if wep = 189
{
snd_play_global(sndShotgun)

repeat(4)
{
with instance_create(x,y,BulletGrenade)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//GRENADE SHOTGUN
if wep = 190
{
snd_play_global(sndShotgun)

repeat(4)
{
with instance_create(x,y,BulletGrenade)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 6
}

//BSHOVL
if wep = 191
{
snd_play_global(sndShovel)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),BloodSlash)
{
dmg = 30
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),BloodSlash)
{
dmg = 30
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+30*other.accuracy,3+longarms)
image_angle = direction
team = other.team}

with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),BloodSlash)
{
dmg = 30
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-30*other.accuracy,3+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
other.wkick = 2
        }
        

//AUTO ERASER
if wep = 192
{
snd_play_global(sndShotgun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

repeat(17)
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle,4+random(16))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//TRIPLE POP 
if wep = 193
{
snd_play_global(sndMachinegun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle-15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//AUTO BOUNCY SHOTTY
if wep = 194
{
snd_play_global(sndMachinegun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

shootangle = point_direction(x,y,mouse_x,mouse_y)-30

repeat(5)
with instance_create(x,y,BouncyBullet)
{
motion_add(other.shootangle,4)
image_angle = direction
team = other.team
other.shootangle += 10
}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//BOUNCY MINIGUN
if wep = 195
{
snd_play_global(sndMinigun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,BouncyBullet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,4+random(3))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 4
}

//heavy GATLING SLUGGER
if wep = 196
{
snd_play_global(sndSlugger)

with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle+(random(20)-10)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 8
}

//HEAVY ASSAULT SLUGGER
if wep = 197
{
with instance_create(x,y,HSlugBurst)
{
creator = other.id
ammo = 3
time = 2
team = other.team
event_perform(ev_alarm,0) 
}
}

//HEAVY MINIGUN
if wep = 198
{
snd_play_global(sndMinigun)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(80)-40,3+random(2))

with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+(random(26)-13)*other.accuracy,16)
image_angle = direction
team = other.team}
motion_add(other.gunangle+180,0.6)
BackCont.viewx2 += lengthdir_x(7,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(7,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//TRIPLE HEAVY MACHINEGUN 
if wep = 199
{
snd_play_global(sndMachinegun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle-15*other.accuracy+(random(6)-3)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 7
wkick = 6
}

//DEVOURER
if wep = 200
{
with instance_create(x,y,DevourerBurst)
{
with instance_create(x,y,DevourerEffect) {scale = 1 crescent = 1}
snd_play_global(LOTPMsndDevourer)
myalpha = 0
anim = 0
creator = other.id
ammo = 1
time = 200
team = other.team
event_perform(ev_alarm,0) 
}
}

//FLAME POP GUN
if wep = 201
{
snd_play_global(sndMachinegun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,FlamePellet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//BLOOD CLUSTER
if wep = 202
{
snd_play_global(sndGrenade)

with instance_create(x,y,BloodCluster)
{
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//BOUNCER CANNON
if wep = 203
{
snd_play_global(sndGrenade)

with instance_create(x,y,BouncerCannon)
{
motion_add(other.gunangle+(random(12)-6)*other.accuracy,11+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}

//TRIPLE ERASER
if wep = 204
{
snd_play_global(sndShotgun)

repeat(3)
{
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(70)-35,2+random(2))
}

repeat(17)
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle*other.accuracy,4+random(16))
image_angle = direction
team = other.team}
repeat(17)
with instance_create(x,y,Bullet2)
{motion_add((point_direction(x,y,mouse_x,mouse_y)+16)*other.accuracy,4+random(16))
image_angle = direction
team = other.team}
repeat(17)
with instance_create(x,y,Bullet2)
{motion_add((point_direction(x,y,mouse_x,mouse_y)-16)*other.accuracy,4+random(16))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(8,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 6
}

//HEAVY BULLET SHOTGUN
if wep = 205
{
snd_play_global(sndSlugger)

repeat(5)
{
with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(10))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//HEAVY BULLET SHOTGUN
if wep = 206
{
snd_play_global(sndSlugger)

repeat(5)
{
with instance_create(x,y,HeavyBullet)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,12+random(10))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//BLASTER
if wep =  207
{
with instance_create(x,y,BlasterBurst)
{
creator = other.id
ammo = 3
time = 3
team = other.team
event_perform(ev_alarm,0) 
}
}

//BLASTERRIFLE
if wep = 208
{
with instance_create(x,y,BlasterBurst2)
{
acc = 0
creator = other.id
ammo = 6
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//TRIPLE BLASTERRIFLE
if wep = 209
{
with instance_create(x,y,BlasterBurst2)
{
acc = 0
creator = other.id
ammo = 6
time = 1
team = other.team
event_perform(ev_alarm,0) 
}

with instance_create(x,y,BlasterBurst2)
{
acc = 15
creator = other.id
ammo = 6
time = 1
team = other.team
event_perform(ev_alarm,0) 
}

with instance_create(x,y,BlasterBurst2)
{
acc = -15
creator = other.id
ammo = 6
time = 1
team = other.team
event_perform(ev_alarm,0) 
}

}

//ATOM BOMB
if wep = 210
{
snd_play_global(sndGrenade)
with instance_create(x,y,AtomBomb)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,8)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//LIGHTNING MINIGUN
if wep = 211
{

if skill_got[17] = 1
snd_play_global(sndLaserUpg)
else
snd_play_global(sndLaser)


with instance_create(x,y,Lightning)
{image_angle = point_direction(x,y,mouse_x,mouse_y)+(random(30)-15)*other.accuracy
team = other.team
ammo = 19
event_perform(ev_alarm,0)
visible = 0
with instance_create(x,y,LightningSpawn)
image_angle = other.image_angle}

BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 5
wkick = 4
}

//AUTO DOUBLE SHOTGUN
if wep = 212
{
snd_play_global(sndShotgun)

repeat(14)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(50)-30)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

motion_add(other.gunangle+180,2)

BackCont.viewx2 += lengthdir_x(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 16
wkick = 8
}

//NADER
if wep = 213
{
snd_play_global(sndGrenade)

with instance_create(x,y,NapalmGrenade)
{
big = 1
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//SPLINTER MINIGUNGUN
if wep = 214
{
snd_play_global(sndSplinterGun)

repeat(6)
{
with instance_create(x,y,Splinter)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,20+random(4))
image_angle = direction
team = other.team}
with instance_create(x,y,Splinter)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,20+random(4))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(15,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 3
wkick = 4
}


//GATLING SUPER SLUGGER
if wep = 215
{
snd_play_global(sndSlugger)

motion_add(other.gunangle+180,3)

with instance_create(x,y,Slug)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle+10*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle+20*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle-10*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,Slug)
{motion_add(other.gunangle-20*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 15
wkick = 8
}


//HEAVY SUPER SLUGGER
if wep = 216
{
snd_play_global(sndSlugger)

motion_add(other.gunangle+180,3)

with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle+10*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle+20*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle-10*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}
with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle-20*other.accuracy+(random(8)-4)*other.accuracy,18)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 15
wkick = 8
}

//AUTO FLAK CANNON
if wep = 217
{
snd_play_global(sndFlakCannon)

with instance_create(x,y,FlakBullet)
{
motion_add(other.gunangle+(random(12)-6)*other.accuracy,11+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}

//popo nader
if wep = 218
{
snd_play_global(sndGrenade)

with instance_create(x,y,PopoNade)
{
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,7)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5
}

//DOUBLE REVOLVER
if wep = 219
{
snd_play_global(sndPistol)

repeat(2) with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x-5,y-5,Bullet1)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}
with instance_create(x+5,y+5,Bullet1)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//heavy gatling SLUGGER
if wep = 220
{
snd_play_global(sndSlugger)

with instance_create(x,y,HeavySlug)
{motion_add(other.gunangle+(random(10)-5)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(14,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 10
wkick = 8
}

//PORTAL GRENADE

if wep = 221
{
snd_play_global(sndGrenade)

with instance_create(x,y,PortalGrenade)
{
sticky = 0
motion_add(other.gunangle+(random(6)-3)*other.accuracy,10)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 2
wkick = 5

wep = bwep
bwep = 0
exit;
}

//SEEKER MINIGUN
if wep = 222
{
snd_play_global(sndSplinterGun)

repeat(choose(2,3))
with instance_create(x,y,Seeker)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,6+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SPADE
if wep = 223
{
snd_play_global(sndShovel)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(30+Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(30+Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle*other.accuracy,4+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(60+Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(60+Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)),Slash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle*other.accuracy,5+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//DESTROYER
if wep = 224
{
snd_play_global(sndSlugger)

with instance_create(x,y,DestroyerExplode)
{
direction = point_direction(x,y,mouse_x,mouse_y)+(random(4)-2)*other.accuracy
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(20,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 8
}

//SHOVEL
if wep = 225
{
snd_play_global(sndShovel)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),EverythingSlash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),EverythingSlash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),EverythingSlash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//CORPSE CANNON
if wep = 226
{
snd_play_global(sndBanditDie)
snd_play_global(sndSlugger)

with instance_create(x,y,Corpse)
{
size = 2
mask_index = mskBandit
motion_add(other.gunangle-2*Player.accuracy,16)
sprite_index = choose(sprBanditDead,sprExploderDead,sprRatDead,sprGatorDead,sprRavenDead,sprSniperDead,sprSpiderDead,sprSnowBotDead,sprRuinsSargeDead,sprMushDead,sprFungDead)
image_xscale = choose(1,-1)
}

motion_add(180+point_direction(x,y,mouse_x,mouse_y),6)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = 8
}

//POTATO GUN
if wep = 227
{
snd_play_global(sndHitWall)

with instance_create(x,y,Potato)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,7)
image_angle = direction
team = other.team
}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//PLASMA WAVE GUN
if wep = 228
{
with instance_create(x,y,PlasmaWaveBurst)
{
creator = other.id
ammo = 7
time = 1
team = other.team
event_perform(ev_alarm,0) 
}
}

//BLASTER CANNON
if wep = 229
{
snd_play_global(sndFlare)

with instance_create(x,y,BlasterCannon)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,5)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SHOVEL
if wep = 230
{
snd_play_global(sndHammer)

instance_create(x,y,Dust)

with instance_create(x+lengthdir_x(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(Player.skill_got[13]*20,point_direction(x,y,mouse_x,mouse_y)),EnergyHammerSlash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle,3+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)+60*Player.accuracy),EnergyHammerSlash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle+60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}
with instance_create(x+lengthdir_x(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),y+lengthdir_y(Player.skill_got[13]*15,point_direction(x,y,mouse_x,mouse_y)-60*Player.accuracy),EnergyHammerSlash)
{
dmg = 8
longarms = 0
if instance_exists(Player)
longarms = Player.skill_got[13]*3
motion_add(other.gunangle-60*other.accuracy,2+longarms)
image_angle = direction
team = other.team}

wepangle = -wepangle
motion_add(other.gunangle,6)
BackCont.viewx2 += lengthdir_x(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(24,point_direction(x,y,mouse_x,mouse_y))*UberCont.opt_shake
BackCont.shake += 1
wkick = -4
}

//PROTO CANNON
if wep = 231
{

if Player.rad>60
    {
with instance_create(x,y,ProtoBullet)
{
snd_play_global(LOTPMsndThroneBall)
Player.rad -= 60
motion_add(other.gunangle+(random(8)-4)*other.accuracy,9)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 5
        }
else
{
Player.reload = 1
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}
}

//SHRAPNEL
if wep = 232
{
snd_play_global(sndHeavyRevolver)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,ShrapnelBullet)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//WALL LAUNCHER
if wep = 233
{
if distance_to_object(Wall)<8 then

{
with instance_nearest(x,y,Wall) {instance_destroy(); instance_create(x,y,FloorExplo)}
snd_play_global(sndGrenade)

repeat(3) with instance_create(x+random(8)-4,y+random(8)-4,WallProjectile)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,12+random(4))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

else
{
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
reload = 1
}

}

//AUTO BULLET SHOTGUN
if wep = 234
{
snd_play_global(sndShotgun)

repeat(6)
{
with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//BFG 3000
if wep = 235
{
if skill_got[17] = 1
snd_play_global(sndPlasmaUpg)
else
snd_play_global(sndPlasma)

repeat(4) with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),BFGBall)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,2+random(14))
image_angle = direction
team = other.team}

motion_add(other.gunangle+180,3)
BackCont.viewx2 += lengthdir_x(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(3,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake = 2
wkick = 5
}

//ULTRA PLASMA GUN
if wep = 236
{

if Player.rad > floor(16-(16*(0.238*Player.ultra_got[60]))) then
{
snd_play_global(sndPlasmaUpg)
Player.rad -= floor(16-(16*(0.238*Player.ultra_got[60])))

with instance_create(x,y,UltraBall)
{
motion_add(other.gunangle+(random(10)-5)*other.accuracy,16)
image_angle = direction
team = other.team
}

with instance_create(x,y,UltraBall)
{
motion_add(other.gunangle+(random(10)-5)*other.accuracy+15,12)
image_angle = direction
team = other.team
}

with instance_create(x,y,UltraBall)
{
motion_add(other.gunangle+(random(10)-5)*other.accuracy-15,12)
image_angle = direction
team = other.team
}

BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 6
wkick = 4
}

else
{
Player.reload = 1
Player.ammo[other.wep_type[other.wep]] += wep_cost[other.wep]
if instance_exists(PopupText) = 0
dir = instance_create(x,y,PopupText)
dir.mytext = "NOT ENOUGH RADS"
}

}

//EVERYTHING BAZOOKA
if wep = 237
{

repeat(3)
with instance_create(x,y,EnemyBurstObj)
{
dmg = 2
creator = other.id
obj = EverythingRocket
accuracy = 30
ammo = 5
time = 2
setspeed = 8+choose(2,3,4)
team = other.team
event_perform(ev_alarm,0) 
}


BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake = 10
wkick = 4

}

//HEAVY SHOTGUN
if wep = 238
{
snd_play_global(sndHeavyShotgun)

repeat(7)
{
with instance_create(x,y,HeavyBullet2)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,10+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 9
wkick = 7
}

//AUTO HEAVY SHOTGUN
if wep = 239
{
snd_play_global(sndHeavyShotgun)

repeat(7)
{
with instance_create(x,y,HeavyBullet2)
{motion_add(other.gunangle+(random(40)-20)*other.accuracy,10+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 9
wkick = 7
}

//DOUBLE HEAVY SHOTGUN
if wep = 240
{
snd_play_global(sndHeavyShotgun)

repeat(16)
{
with instance_create(x,y,HeavyBullet2)
{motion_add(other.gunangle+(random(30)-15)*other.accuracy,10+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 9
wkick = 7
}

//H ERASER
if wep = 241
{
snd_play_global(sndHeavyShotgun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

repeat(17)
with instance_create(x,y,HeavyBullet2)
{motion_add(other.gunangle,4+random(16))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 7
wkick = 4
}

//AUTO H ERASER
if wep = 242
{
snd_play_global(sndHeavyShotgun)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

repeat(12)
with instance_create(x,y,HeavyBullet2)
{motion_add(other.gunangle,4+random(16))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 7
wkick = 4
}

//FLAME FLAK CANNON
if wep = 243
{
snd_play_global(sndFlakCannon)

with instance_create(x,y,FlameFlakBullet)
{
motion_add(other.gunangle+(random(12)-6)*other.accuracy,11+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}

//SUPER FLAME FLAK CANNON
if wep = 244
{
snd_play_global(sndSuperFlakCannon)

with instance_create(x,y,SuperFlameFlakBullet)
{
motion_add(other.gunangle+(random(12)-6)*other.accuracy,11+random(2))
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(32,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 7
}


//DEVASTATOR
if wep = 245
{
snd_play_global(sndDevastator)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,Devastator)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,16)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//SUPER DEVASTATOR
if wep = 246
{
snd_play_global(sndDevastator)

with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(50)-25,2+random(2))

with instance_create(x,y,SuperDevastator)
{motion_add(other.gunangle+(random(8)-4)*other.accuracy,8)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(6,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 2
}

//MINIGUN
if wep = 247
{
snd_play_global(sndMinigun)
with instance_create(x,y,Shell)
motion_add(other.gunangle+other.right*100+random(80)-40,3+random(2))
repeat(2)

with instance_create(x,y,Bullet1)
{motion_add(other.gunangle+(random(36)-18)*other.accuracy,16)
image_angle = direction
team = other.team}
motion_add(other.gunangle+180,0.6)
BackCont.viewx2 += lengthdir_x(7,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(7,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 4
}

//OCTO SHOTGUN
if wep = 248
{
snd_play_global(sndShotgun)
repeat(7*8)
{
with instance_create(x,y,Bullet2)
{motion_add(other.gunangle+(random(50)-25)*other.accuracy,12+random(6))
image_angle = direction
team = other.team}
}

BackCont.viewx2 += lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 8
wkick = 6
}

//SUPER FLAME
if wep = 249
{
snd_play_global(sndFlameCannon)

with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),SFCProjectile)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,3)
image_angle = direction
friction = 0.025
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//SUPER BLOOD
if wep = 250
{
snd_play_global(sndGrenade)

with instance_create(x+lengthdir_x(12,point_direction(x,y,mouse_x,mouse_y)),y+lengthdir_y(12,point_direction(x,y,mouse_x,mouse_y)),SBCProjectile)
{motion_add(other.gunangle+(random(4)-2)*other.accuracy,3)
image_angle = direction
team = other.team}

BackCont.viewx2 += lengthdir_x(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(30,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake += 4
wkick = 10
}

//HAND CANNON
if wep = 251
{

repeat(3)
with instance_create(x,y,PlayerBurstObj)
{
snd = sndPistol
dmg = 5
creator = other.id
obj = Bullet1
accuracy = 30
ammo = 2
time = 2
setspeed = 14+choose(2,4,6)
team = other.team
event_perform(ev_alarm,0) 
}


BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake = 10
wkick = 4

}

//HEAVY HAND CANNON
if wep = 252
{

repeat(3)
with instance_create(x,y,PlayerBurstObj)
{
snd = sndHeavyRevolver
dmg = 5
creator = other.id
obj = HeavyBullet
accuracy = 30
ammo = 2
time = 2
setspeed = 14+choose(2,4,6)
team = other.team
event_perform(ev_alarm,0) 
}


BackCont.viewx2 += lengthdir_x(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(10,point_direction(x,y,mouse_x,mouse_y)+180)*UberCont.opt_shake
BackCont.shake = 10
wkick = 4

}
