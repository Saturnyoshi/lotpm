var butindex
switch argument1 {
    case 7: butindex=gp_start; break;           //pause
    case 4: butindex=gp_shoulderlb; break;      //active
    case 5: butindex=gp_shoulderrb; break;      //fire
    case 3: butindex=gp_face4; break;
    case 2: butindex=gp_face3; break;
    case 1: butindex=gp_face2; break;
    case 0: butindex=gp_face1; break;
}
return gamepad_button_check(argument0,butindex)
