
x += lengthdir_x(32,direction)
y += lengthdir_y(32,direction)

area = 0
if instance_exists(Player)
{
    area = Player.area
    subarea = Player.subarea
}

//DESERT NORMAL
if global.hardmode = 0 then
if area = 1 { if random(2) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
}else instance_create(x,y,Floor)} 

//DESERT HARD
if global.hardmode = 1 then
if area = 1 { if random(2) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)} } 

//SEWERS NORMAL
if area=2 and global.hardmode=0 then instance_create(x,y,Floor)

//SEWERS HARD
if area=2 and global.hardmode=1 then 
if random(10) <1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else {instance_create(x,y,Floor)} 

if area = 0 or area = 102
instance_create(x,y,Floor)

///SCRAPYARD NORMAL
if area = 3 {
    if subarea != 3
    {
        if random(8) < 1
        {
            instance_create(x,y,Floor)
            instance_create(x+32,y,Floor)
            instance_create(x+32,y+32,Floor)
            instance_create(x,y+32,Floor)
            instance_create(x,y-32,Floor)
            instance_create(x-32,y,Floor)
            instance_create(x+32,y-32,Floor)
            instance_create(x-32,y-32,Floor)
            instance_create(x-32,y+32,Floor)
        }
        else instance_create(x,y,Floor)
    }
    else
    {
        instance_create(x,y,Floor)
        instance_create(x+32,y,Floor)
        instance_create(x+32,y+32,Floor)
        instance_create(x,y+32,Floor)
        instance_create(x,y-32,Floor)
        instance_create(x-32,y,Floor)
        instance_create(x+32,y-32,Floor)
        instance_create(x-32,y-32,Floor)
        instance_create(x-32,y+32,Floor)
    }
} 

//CAVES NORMAL
//CAVES NORMAL
//CAVES NORMAL
if area = 4 and global.hardmode = 0 then
{
instance_create(x,y,Floor)
}

//CAVES HARD
//CAVES HARD
//CAVES HARD
//CAVES HARD
if (area = 4 and global.hardmode= 1) or area = 106 then
if random(8) < 1
{instance_create(x,y,Floor)
}
else
{
instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)} 

//FROZEN CITY NORMAL
//FROZEN CITY NORMAL
//FROZEN CITY NORMAL
//FROZEN CITY NORMAL
if area = 5 and global.hardmode=0 { if random(11) < 1
{
if random(2) < 1
{
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else
{
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}
}else instance_create(x,y,Floor)} 

//FROZEN CITY HARD
//FROZEN CITY HARD
//FROZEN CITY HARD
//FROZEN CITY HARD
if area = 5 and global.hardmode=1 { if random(11) < 1
{
if random(12) < 1 and distance_to_point(xstart,ystart)>300
{
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}
else
{
instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
}
}else instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)} 



if area = 100 { if random(8) < 1
{
dir = choose(1,2)
if dir = 1
{
instance_create(x+32,y,Floor)
instance_create(x+64,y,Floor)
instance_create(x,y,Floor)
instance_create(x-32,y,Floor)
instance_create(x-64,y,Floor)
}
else
{
instance_create(x,y+32,Floor)
instance_create(x,y+64,Floor)
instance_create(x,y,Floor)
instance_create(x,y-32,Floor)
instance_create(x,y-64,Floor)
}
}else instance_create(x,y,Floor)} 

if area = 101
{
    if random(3) < 2
    {
        instance_create(x,y,Floor)
        instance_create(x+32,y,Floor)
        instance_create(x+32,y+32,Floor)
        instance_create(x,y+32,Floor)
        instance_create(x+64,y,Floor)
        instance_create(x+64,y+64,Floor)
        instance_create(x,y+64,Floor)
        instance_create(x+96,y,Floor)
        instance_create(x+96,y+96,Floor)
        instance_create(x,y+96,Floor)
    }else instance_create(x,y,Floor)
}


if area = 103 
{
if random(3)<1 then
{instance_create(x,y,Floor)}
else
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)}

if instance_exists(Player)
if Player.subarea = -1
{
instance_create(x,y,NOWALLSHEREPLEASE)
instance_create(x+32,y,NOWALLSHEREPLEASE)
instance_create(x+32,y+32,NOWALLSHEREPLEASE)
instance_create(x,y+32,NOWALLSHEREPLEASE)
goal = 40
}

}


//if area = 1
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 2 or area = 102
trn = choose(0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 3
{
    if subarea != 3
    trn = choose(0,0,0,0,90,-90)
    else
    trn = choose(0,0,0,0,0,0,90,0,-90,180);
}
if area = 4 and global.hardmode= 0 then
trn = choose(0,0,0,0,90,-90,180)
if area = 4 and global.hardmode= 1 then
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,0)
if area = 5
trn = choose(0,0,0,0,0,0,90,0,-90,180)
if area = 6
trn = choose(0,0,0,0,0,0,0,0,0,0,0,90,-90,180)
if area = 8
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 9
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 10
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 11
trn = choose(90,-90,180)
if area = 100
trn = choose(0,0,0,0,0,0,0,0,0,0,90,-90,180,180)
if area = 101
trn = choose(0,0,0,0,0,0,-90,-90,180,-180)
if area = 103
trn = choose(0,0,0,90,-90,180)
if area = 104
trn = choose(0,0,0,0,0,0,0,0,90,-90)

direction += trn

//SHOP
if area = 107
{
    with(FloorMaker)
    {
            if(id > other.id)
            {
                    instance_destroy();
            }
    }
    repeat(6)
    {
        styleb = 0;
        y -= 32;
        for(i = -128; i <= 128; i += 32)
        {
            instance_create(x+i,y+96,Floor);
        }
    }
    
    instance_create(x+16,y+150,Carpet);
    
    with(Floor)
    {
            instance_create(x,y,NOWALLSHEREPLEASE);
    }
    instance_destroy();
}

if abs(trn) = 90 and area = 6 and random(2) < 1
{
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
if random(3) < 1
{
if random(4) < 3
instance_create(x-16,y-16,Server)
if random(4) < 3
instance_create(x-16+64,y-16,Server)
if random(4) < 3
instance_create(x-16,y-16+64,Server)
if random(4) < 3
instance_create(x-16+64,y-16+64,Server)
}

}

//instance_create(x,y,Floor)

if (trn = 180 or (abs(trn) = 90 and area = 3)) and point_distance(x,y,10016,10016) > 48{
instance_create(x,y,Floor)
instance_create(x+16,y+16,WeaponChest)}



if area = 1 or area = 101
{
if random(19+instance_number(FloorMaker)) > 20
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
}
if random(8) < 1
instance_create(x,y,FloorMaker)
}
if area = 2
{
if random(14+instance_number(FloorMaker)) > 15
{
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
instance_destroy()
}
if random(15) < 1
instance_create(x,y,FloorMaker)
}

if area = 3
{
if random(39+instance_number(FloorMaker)) > 40
{
if point_distance(x,y,10016,10016) > 48
{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
instance_destroy()
}
if random(25) < 1
instance_create(x,y,FloorMaker)
}
if area = 4
{
if random(9+instance_number(FloorMaker)) > 10
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
}
if random(4) < 1
instance_create(x,y,FloorMaker)
}
if area = 5
{
if random(14+instance_number(FloorMaker)) > 15
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x,y,Floor)
instance_create(x+16,y+16,AmmoChest)}
}
if random(25) < 1
instance_create(x,y,FloorMaker)
}
if area = 6
{
goal = 160;
if random(21+instance_number(FloorMaker)) > 22
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48
{
instance_create(x,y,Floor)
instance_create(x+16,y+16,AmmoChest)}
}
if random(80) < 1
instance_create(x,y,FloorMaker)
}


if area = 102
{
if random(9+instance_number(FloorMaker)) > 10
{
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
instance_destroy()
}
if random(5) < 1
instance_create(x,y,FloorMaker)
}

if instance_exists(Player)
{
if area = 103
if Player.subarea != -1 //yv's cribs
{
if random(31+instance_number(FloorMaker)) > 32
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x,y,Floor)
instance_create(x+16,y+16,AmmoChest)}
}
if random(20) < 1
instance_create(x,y,FloorMaker)
}
}

if area = 6
{
if random(90) < 1 and distance_to_point(xstart,ystart)>120
{
instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else instance_create(x,y,Floor)
} 

//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************
//**************LOTPM CUSTOM AREAS*******************

//RUINS

if area = 7{
goal = 120;
if random(8) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
}else 
if random(2)<1 then
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)}
else
{instance_create(x,y,Floor)}
} 

//MYCELIUM CAVES
if area = 8
{
if random(8)<1
{
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}
else {instance_create(x,y,Floor)}
goal = 180;
} 

//DROWNED CITY
if area = 9 {
if Player.subarea != 3 then goal = 240
if random(2) < 1
{instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}else {
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}
if random(19+instance_number(FloorMaker)) > 20
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
}
if random(8) < 1
instance_create(x,y,FloorMaker)
}

//TEMPLE
if area = 10 {
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)

}

//REMAINS
if area = 11 {
goal = 120
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}

//HQ
if area = 104 
{
if random(25) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
}
else instance_create(x,y,Floor);

if random(19+instance_number(FloorMaker)) > 20
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
}
if random(8) < 1
instance_create(x,y,FloorMaker)

}

//JUNGLE
if area = 108 { if random(2) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)} }


/*if area = 100
{
if random(29+instance_number(FloorMaker)) > 30
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48
instance_create(x+16,y+16,AmmoChest)
}
if random(11) < 1
instance_create(x,y,FloorMaker)
}*/


/*
instance_create(x+64,y,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y-64,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y-64,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)


instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x+32,y-64,Floor)
instance_create(x-32,y-64,Floor)
*/
