///host_send(packettype, arg[0]... arg[14])
buffer_seek(global.coopbuffer,buffer_seek_start,0)
buffer_write(global.coopbuffer,buffer_u8,argument[0])

switch argument[0] {
    case PAC_UPDATEENEMY:
        buffer_write(global.coopbuffer,buffer_u16,m_id)
        buffer_write(global.coopbuffer,buffer_f32,x)
        buffer_write(global.coopbuffer,buffer_f32,y)
        buffer_write(global.coopbuffer,buffer_f32,gunangle)
        buffer_write(global.coopbuffer,buffer_s8,right)
        buffer_write(global.coopbuffer,buffer_u16,alarm[0])
        buffer_write(global.coopbuffer,buffer_f32,speed)
        buffer_write(global.coopbuffer,buffer_f32,direction)
    break
    
    case PAC_SPAWN:
        buffer_write(global.coopbuffer,buffer_f32,argument[1])
        buffer_write(global.coopbuffer,buffer_f32,argument[2])
        buffer_write(global.coopbuffer,buffer_u16,argument[3])
        with instance_create(argument[1],argument[2],argument[3]) {
            set_m_id()
            buffer_write(global.coopbuffer,buffer_u16,m_id)
        }
    break
}


var val=ds_list_find_value(sockets,0);
var tt=0;
while !is_undefined(val) {
    network_send_packet(val,global.coopbuffer,buffer_tell(global.coopbuffer))
    tt++
    val=ds_list_find_value(sockets,tt)
}
