
x += lengthdir_x(32,direction)
y += lengthdir_y(32,direction)

area = 0
subarea = 0
if instance_exists(Player)
area = Player.area
subarea = BackCont.subarea

if area = 1 or area = 104 { if random(2) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
}else instance_create(x,y,Floor)} 

if area = 0 or area = 2 or area = 102 or area = 6
instance_create(x,y,Floor)

if (area = 3 && subarea != 3) or area = 7 { if random(8) < 1
{instance_create(x,y,Floor)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else if (area = 3 && subarea = 3) {
if random(8) < 1 {
        instance_create(x,y,Floor);
        instance_create(x-32,y,Floor);
        instance_create(x-64,y,Floor);
        instance_create(x-96,y,Floor);
        instance_create(x-128,y,Floor);
        instance_create(x-160,y,Floor);
        instance_create(x+32,y,Floor);
        instance_create(x+64,y,Floor);
        instance_create(x+96,y,Floor);
        instance_create(x+128,y,Floor);
        instance_create(x+160,y,Floor);
        instance_create(x,y+32,Floor);
        instance_create(x-32,y+32,Floor);
        instance_create(x-64,y+32,Floor);
        instance_create(x-96,y+32,Floor);
        instance_create(x-128,y+32,Floor);
        instance_create(x-160,y+32,Floor);
        instance_create(x+32,y+32,Floor);
        instance_create(x+64,y+32,Floor);
        instance_create(x+96,y+32,Floor);
        instance_create(x+128,y+32,Floor);
        instance_create(x+160,y+32,Floor);
        instance_create(x,y+64,Floor);
        instance_create(x-32,y+64,Floor);
        instance_create(x-64,y+64,Floor);
        instance_create(x-96,y+64,Floor);
        instance_create(x-128,y+64,Floor);
        instance_create(x-160,y+64,Floor);
        instance_create(x+32,y+64,Floor);
        instance_create(x+64,y+64,Floor);
        instance_create(x+96,y+64,Floor);
        instance_create(x+128,y+64,Floor);
        instance_create(x+160,y+64,Floor);
        instance_create(x,y+96,Floor);
        instance_create(x-32,y+96,Floor);
        instance_create(x-64,y+96,Floor);
        instance_create(x-96,y+96,Floor);
        instance_create(x-128,y+96,Floor);
        instance_create(x-160,y+96,Floor);
        instance_create(x+32,y+96,Floor);
        instance_create(x+64,y+96,Floor);
        instance_create(x+96,y+96,Floor);
        instance_create(x+128,y+96,Floor);
        instance_create(x+160,y+96,Floor);
        instance_create(x,y+128,Floor);
        instance_create(x-32,y+128,Floor);
        instance_create(x-64,y+128,Floor);
        instance_create(x-96,y+128,Floor);
        instance_create(x-128,y+128,Floor);
        instance_create(x-160,y+128,Floor);
        instance_create(x+32,y+128,Floor);
        instance_create(x+64,y+128,Floor);
        instance_create(x+96,y+128,Floor);
        instance_create(x+128,y+128,Floor);
        instance_create(x+160,y+128,Floor);
        instance_create(x,y+160,Floor);
        instance_create(x-32,y+160,Floor);
        instance_create(x-64,y+160,Floor);
        instance_create(x-96,y+160,Floor);
        instance_create(x-128,y+160,Floor);
        instance_create(x-160,y+160,Floor);
        instance_create(x+32,y+160,Floor);
        instance_create(x+64,y+160,Floor);
        instance_create(x+96,y+160,Floor);
        instance_create(x+128,y+160,Floor);
        instance_create(x+160,y+160,Floor);
}
} else
instance_create(x,y,Floor)} 

if area = 4
instance_create(x,y,Floor)

if area = 5 { if random(11) < 1
{
if random(2) < 1
{
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
}
else
{
instance_create(x+64,y-64,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y-64,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x-32,y-64,Floor)
instance_create(x+32,y-64,Floor)

instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)
}
}else instance_create(x,y,Floor)} 


if area = 100 { if random(8) < 1
{
dir = choose(1,2)
if dir = 1
{
instance_create(x+32,y,Floor)
instance_create(x+64,y,Floor)
instance_create(x,y,Floor)
instance_create(x-32,y,Floor)
instance_create(x-64,y,Floor)
}
else
{
instance_create(x,y+32,Floor)
instance_create(x,y+64,Floor)
instance_create(x,y,Floor)
instance_create(x,y-32,Floor)
instance_create(x,y-64,Floor)
}
}else instance_create(x,y,Floor)} 

if area = 103 {
if round(instance_number(Floor)/12) = instance_number(Floor)/12 and instance_number(Floor) != 0
{
x += lengthdir_x(32,direction)
y += lengthdir_y(32,direction)
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
} else instance_create(x,y,Floor)}


//if area = 1
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 2 or area = 102
trn = choose(0,0,0,0,0,0,0,0,90,-90,90,-90,180)
if area = 3 or area = 7
trn = choose(0,0,0,0,90,-90)
if area = 4
trn = choose(0,0,0,0,90,-90,180)
if area = 5
trn = choose(0,0,0,0,0,180)
if area = 6
trn = choose(0,0,0,0,0,0,0,0,0,0,0,90,-90,180)
if area = 100
trn = choose(0,0,0,0,0,0,0,0,0,0,90,-90,180,180)
if area = 103
trn = choose(0,0,0,90,-90,180)
if area = 104
trn = choose(0,0,0,0,0,0,0,0,0,90,-90,90,-90,180)

direction += trn

if abs(trn) = 90 and area = 6 and random(2) < 1
{
instance_create(x+32,y,Floor)
instance_create(x+32,y+32,Floor)
instance_create(x,y+32,Floor)
instance_create(x,y-32,Floor)
instance_create(x-32,y,Floor)
instance_create(x+32,y-32,Floor)
instance_create(x-32,y-32,Floor)
instance_create(x-32,y+32,Floor)
if random(3) < 1
{
if random(4) < 3
instance_create(x-16,y-16,Server)
if random(4) < 3
instance_create(x-16+64,y-16,Server)
if random(4) < 3
instance_create(x-16,y-16+64,Server)
if random(4) < 3
instance_create(x-16+64,y-16+64,Server)
}

}
if area = 105
{
        with(FloorMaker)
        {
                if(id > other.id)
                {
                        instance_destroy();
                }
        }
        repeat(8)
        {
            styleb = 0;
            y -= 32;
            instance_create(x,y,Floor);
            instance_create(x-32,y,Floor);
            instance_create(x-64,y,Floor);
            instance_create(x-96,y,Floor);
            instance_create(x-128,y,Floor);
            instance_create(x+32,y,Floor);
            instance_create(x+64,y,Floor);
            instance_create(x+96,y,Floor);
            instance_create(x+128,y,Floor);
            instance_create(x,y+32,Floor);
            instance_create(x-32,y+32,Floor);
            instance_create(x-64,y+32,Floor);
            instance_create(x-96,y+32,Floor);
            instance_create(x-128,y+32,Floor);
            instance_create(x+32,y+32,Floor);
            instance_create(x+64,y+32,Floor);
            instance_create(x+96,y+32,Floor);
            instance_create(x+128,y+32,Floor);
        }
        with(Floor)
        {
                instance_create(x,y,NOWALLSHEREPLEASE);
        }
        instance_destroy();
        
        instance_create(x+16,y+128,Gambler);
}
else
{
        if(random(2) < 1)
        {
                instance_create(x,y,Floor);
                instance_create(x+32,y,Floor);
                instance_create(x+32,y+32,Floor);
                instance_create(x,y+32,Floor);
        }
        else
        {
                instance_create(x,y,Floor);
        }
}

//instance_create(x,y,Floor)

if (trn = 180 or (abs(trn) = 90 and area = 3 or area = 7)) and point_distance(x,y,10016,10016) > 48{
instance_create(x,y,Floor)
instance_create(x+16,y+16,WeaponChest)}



if area = 1 or area = 104
{
if random(19+instance_number(FloorMaker)) > 20
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
}
if random(8) < 1
instance_create(x,y,FloorMaker)
}
if area = 2
{
if random(14+instance_number(FloorMaker)) > 15
{
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
instance_destroy()
}
if random(15) < 1
instance_create(x,y,FloorMaker)
}

if area = 3 or area = 7
{
if random(39+instance_number(FloorMaker)) > 40
{
if point_distance(x,y,10016,10016) > 48
{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
instance_destroy()
}
if random(25) < 1
instance_create(x,y,FloorMaker)
}
if area = 4
{
if random(9+instance_number(FloorMaker)) > 10
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
}
if random(4) < 1
instance_create(x,y,FloorMaker)
}
if area = 5
{
if random(14+instance_number(FloorMaker)) > 15
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x,y,Floor)
instance_create(x+16,y+16,AmmoChest)}
}
if random(25) < 1
instance_create(x,y,FloorMaker)
}
if area = 6
{
if random(21+instance_number(FloorMaker)) > 22
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48
{
instance_create(x,y,Floor)
instance_create(x+16,y+16,AmmoChest)}
}
if random(20) < 1
instance_create(x,y,FloorMaker)
}


if area = 102
{
if random(9+instance_number(FloorMaker)) > 10
{
if point_distance(x,y,10016,10016) > 48{
instance_create(x+16,y+16,AmmoChest)
instance_create(x,y,Floor)}
instance_destroy()
}
if random(5) < 1
instance_create(x,y,FloorMaker)
}

if area = 103
{
if random(31+instance_number(FloorMaker)) > 32
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48{
instance_create(x,y,Floor)
instance_create(x+16,y+16,AmmoChest)}
}
}
/*if area = 100
{
if random(29+instance_number(FloorMaker)) > 30
{
instance_destroy()
if point_distance(x,y,10016,10016) > 48
instance_create(x+16,y+16,AmmoChest)
}
if random(11) < 1
instance_create(x,y,FloorMaker)
}*/


/*
instance_create(x+64,y,Floor)
instance_create(x+64,y-32,Floor)
instance_create(x+64,y-64,Floor)
instance_create(x+64,y+32,Floor)
instance_create(x+64,y+64,Floor)

instance_create(x-64,y,Floor)
instance_create(x-64,y-32,Floor)
instance_create(x-64,y-64,Floor)
instance_create(x-64,y+32,Floor)
instance_create(x-64,y+64,Floor)


instance_create(x,y+64,Floor)
instance_create(x-32,y+64,Floor)
instance_create(x+32,y+64,Floor)

instance_create(x,y-64,Floor)
instance_create(x+32,y-64,Floor)
instance_create(x-32,y-64,Floor)
*/

