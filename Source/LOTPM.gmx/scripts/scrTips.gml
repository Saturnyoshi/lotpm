var t, i;
if (random(4) < 1 || !instance_exists(Player)) {
    t = 0;
} else if (random(3) < 1) {
    t = 1;
} else t = choose(3, 4, 5, 6, 7, 8);
if (t == 0) switch (irandom(64)) { // general
    case 0: return "@rHP@s will only drop when damaged";
    case 1: return "@yexplosives@s can destroy walls";
    case 2: return "@wmelee weapons@s can deflect @yprojectiles@s";
    case 3: return "@rHP@s will only drop when damaged";
    case 4: return "@yexplosives@s can destroy walls";
    case 5: return "@wmelee weapons@s can deflect @yprojectiles@s";
    case 6: return "@yshells@s deal more damage from up close";
    case 7: return "assassins can pretend they're dead";
    case 8: return "fish can roll";
    case 9: return "always keep one eye on your @yammo@s";
    case 10: return "robots can digest anything";
    case 11: return "bandits like camping near barrels";
    case 12: return "not today";
    case 13: return "rosebud";
    case 14: return "it's not fair, it's not right";
    case 15: return "watch out for dehydration";
    case 16: return "crystal can shield";
    case 17: return "fear is the mindkiller";
    case 18: return "going in big";
    case 19: return "it's happening";
    case 20: return "no";
    case 21: return "bob and weave";
    case 22: return "airhorn.wav";
    case 23: return "danger zone";
    case 24: return "his legacy";
    case 25: return "what a life";
    case 26: return "remember to take a 15 minute break#for every hour you play!";
    case 27: return "so sweaty";
    case 28: return "just another @pportal@s";
    case 29: return "doing work";
    case 30: return "hold the phone";
    case 31: return "no shame";
    case 32: return "follow your heart";
    case 33: return "practice makes perfect";
    case 34: return "don't give up";
    case 35: return "the @bI.D.P.D.@s despise you";
    case 36: return "try hitting @ygrenades@s for extra range";
    case 37: return "pick your @gmutations@s wisely";
    case 38: return "if only you could talk to the monsters";
    case 39: return "@wsteroids@s can dual wield";
    case 40: return "always wear dry socks";
    case 41: return "@wcrowns@s are loyal";
    case 42: return "@yenergy weapons@s use lots of @yammo@y";
    case 43: return "the elder awaits";
    case 44: return "try not opening @wweapon chests@s";
    case 45: return "fire at things until they die";
    case 46: return "@yammo@s drops depend on your @wweapon types@s";
    case 47: return "don't get hit";
    case 48: return "you get fewer drops when high on @yammo@s";
    case 49: return "doing alright";
    case 50: return "there are other worlds out there";
    case 51: return "is it worth it?";
    case 52: return "it was hard";
    case 53: return "it was worth it";
    case 54: return "thanks for playing lotpm!";
    case 55: return "rated M for mature";
    case 56: return "take a break, if need be";
    case 57: return "are you a boy or a girl?";
    case 58: return "mimics are abound";
    case 59: return "monstro's lung";
    case 60: return "what time is it for you?";
    case 61: return "a figurative bullet hell";
    case 62: return "live a happy life";
    case 63: return "i hope you're ok";
    case 64: return "what are my ultras?";
}
/*if (t == 1 && random(5) < 1) {
    if (UberCont.showtutorial == 1) return choose("everything will be just fine", "don't worry", "the nuclear throne...", "good luck");
    if (UberCont.daily == 1) return choose("such a nice day", "the weather isn't so bad", "don't mess it up", "one shot", "there's always tomorrow", "one day");
    if (UberCont.weekly == 1) return choose("this seems familiar", "what's next?", "free time", "well prepared", "keep trying");
    if (UberCont.hardmode == 1) return choose("heh", "no way", "impossible", choose("", "", "", "", "", "what does SFMT stand for?"), "behind you", "take your time", "...", "it can't be that bad");
    if (UberCont.coop == 1) return choose("friendship", "what a burden", "shouting", "be careful", "help is here", "but who gets the throne?", "two heads");
    return "";
}*/
if (t == 1) switch (Player.area) { // AREA TIPS
    case 0: return choose("it's so dark", "your friends were here", "this can't be true");
    case 1: return choose("in the corner of your eye", "intro", "scorching sun", "watch out for @ymaggots@s", "let's do this", "dust surrounds you", "the wind hurts");
    case 2: return choose("don't drink the water", "the sewers stink", "don't touch the frogs", "don't eat the rat meat", "danger", "water dripping", "sludge everywhere", "so many rats");
    case 3: return choose("shoot bugs on sight", "turrets", "it begins", "rust everywhere", "the sound of birds", "sludge pools", "climb over cars", "portals can blow up cars", "mechanical");
    case 4: return choose("oh no", "spiderwebs everywhere", "don't lose your heart", "reflections on the walls", "skin is crawling", "almost halfway there");
    case 5: return choose("wear a scarf", "revolution", "miss the sun", "civilization", "walk softly", "theres no yeti", "drone");
    case 6: return choose("beep boop", "nerds", "broken buttons");
    case 7: return choose("distant squeaking", "let's do the time warp!", "time locked", "refuge", "where's sheriff isaac", "memories", "tyranny");
    case 8: return choose("souls", "fungi", "can you not?", "spores", "it's so dark", "monster mushes heal");
    case 9: return choose("what was that", "it lurks", "no crowns, just fish", "black lagoon", "it's coming", "eldritch", "visit vault 101");
    case 10: return choose("maniac", "fanatic", "they're coming", "ritualistic sacrifices");
    case 11: return choose("it's their lair", "radiation", "boss rush", "one in three");
    case 100: return choose("awww yes", "become a king", "temporary", "downsides, upsides");
    case 101: return choose("fish", "hold your breath", "it's beautiful down here", "don't move", "relax", "fiji");
    case 102: return choose("hunger...", "it smells nice here", "eugh");
    case 103: return choose("space...", "always wanted to go here", "so much mony", "4 years later...", "follow venuzpatrol");
    case 104: return choose("weewoo", "destroyed", "it's too hard", "opie op");
    case 105: return choose("this is where the magic happens", "lets take a look in the fridge", "get the hell out of here", "now this is real special", "wakkala wayo", "@yyo.@s", "gold across it");
    case 106: return choose("no oh ", "everywhere spiderwebs", "crawling is skin", "there halfway almost");
    case 107: return choose("kajiit has wares", "merchantry", "come again!", "a friendly fight", "for happy children", "liontamer", "growling");
    case 108: return choose("there's something in the trees", "bugs everywhere", "welcome to the jungle", "heart of darkness");
    default: return "";
}
//var p = Player; //, floor(random(instance_number(Player))));
if (t == 2) {
    if (Player.level == 10 && random(5) < 1) switch (Player.race) { // ULTRA TIPS
        case 0: return choose("random @gultra@s, unstoppable");
        case 1: return choose("getting used to this", "just one more day", "somethin-somethin", "fire in the hole");
        case 2: return choose("just a scratch", "stay strong", "tis a flesh wound");
        case 3: return choose("know everything", "show nothing", "keep it inside", "xray vision", "can't hide");
        case 4: return choose("the pain...", "please stop...", "it won't stop...", "save me...");
        case 5: return choose("end end end", "delet this");
        case 6: return choose("\#verifycuz", "airsiren.wav", "\#blessed", "one of these days...", "real thugs hustle", "YV fact: YV IS THE BEST", "4EVER", "GO HARD", "VOTE 2 B COOL", "MURDER NON-STOP", "it's LIT");
        case 7: return choose("let's hope this is correct", "so strong", "calvacade");
        case 8: return choose("6d 61 63 68 69 6e 65 73#77 69 6c 6c#6e 65 76 65 72#65 6e 64", "66 6c 65 73 68#69 73#77 65 61 6b", "73 69 6e 67 75 6c 61 72 69 74 79", "don't panic", "ROM check okay#are you ok too?", "illegal exception");
        case 9: return choose("this is destiny", "just like in the movies", "again we are defeated", "you made one mistake", "make your move");
        case 10: return choose("all together now", "no stopping now", "synchronize");
        case 11: return choose("give in", "absolute maniac", "furious", "absolute", "instincts");
        case 12: return choose("put your money where your#mouth is", "amino acids", "belch", "big children");
        case 13: return choose("alpha radiation", "beta radiation", "gamma radiation", "canary in the mineshaft");
        case 14: return choose("at peace", "zen garden", "primitive", "law of the jungle");
        case 15: return choose("the air is changing", "the light moves", "things are different");
        default: return "";
    } else switch (Player.race) { // NORMAL CHARACTER TIPS
        case 0: return choose("random shifts shapes", "clearly the most powerful", "never the same", "shapeless", "passive: anything", "active: anything", "noone compares");
        case 1: return choose("last day before retirement", "duty calls", "it's ok to eat", "gills on your neck", "like kevin costner", "the taste of mud");
        case 2: return choose("crystal can handle this", "family");
        case 3: return choose("eyes sees everything", "don't blink", "all these thoughts", "eyes can't speak", "telekinesis pushes projectiles away");
        case 4: return choose("it's so cold out here", "melting is tired", "everything hurts", "cough", "brr...", "it's ok to be scared");
        case 5: return choose("kill kill kill", "death death death", "blood blood blood", "no mercy", "photosynthesizing...", "snare is a source of light", "gotta go fast");
        case 6: return choose("yung venuz is so cool", "yung venuz is the best", "so cool", "thanks gun god", "\#verifyvenuz", "2 yung 2 die", "guns for fake necklace", "guns that send textses", "guns that make breakfast", "mony", "guns that straight festive", "guns with 6 senses", "guns that hate texas", "guns that wear vests it", "no @bpopo@s", "pop pop");
        case 7: return choose("time to flex", "study hard", "steroids could do pushups forever", "steroids used to be a scientist", "appreciate @wrevolvers@s", "get shots", "get strong", "read a book");
        case 8: return choose("6b 69 6c 6c#61 6c 6c#68 75 6d 61 6e 73", "72 6f 62 6f 74", "3c 33", "74 61 73 74 79", "@wguns@s for breakfast", "I'm afraid I can't let you do that", "don't forget to eat @wweapons@s");
        case 9: return choose("remember the training", "focus", "go", "never surrender", "amateur hour is over", "again", "getting decapitated reduces max HP", "@rsuper! hot! super! hot!@s", "conglaturaisins");
        case 10: return choose("forget the old days", "change is coming", "allies are a source of light", "a new generation", "it will get better", "spawning new allies @rheals@s old ones", "your first ally costs less @rHP@s", "allies take damage over time", "the scarf is nice");
        case 11: return choose("smash em", "[brute intensifies]", "brutalitiester", "yay! violence!", "charging shots @wslow you down@s", "hold down rmb");
        case 12: return choose("obituaries? i call em menus", "defunt au fromage", "stomach rumbles", "necrophagy is fun!", "my swarm", "eat or be eaten", "grub loves having a bite to eat");
        case 13: if Player.area = 6 and random(3) < 2{ return "home"; } else return choose("together we are strong", "teamwork makes the dreamwork", "they are my brothers", "you can place more turrets#as you level up");
        case 14: return choose("disassociative", "platypus was a mutant#before it was cool", "go berserk!");
        case 15: return choose("@gradiation@s is everywhere", "in the zone", "the horror", "power", "horror's beam powers up over time", "horror's beam destroys projectiles", "enemies absorb the beam's @grads@s", "firing the beam pauses @grad@s attraction");
        default: return "";
    }
}
if (t == 3) {
    if (random(2) < 1) {
        if ((Player.curse == 1 || Player.bcurse == 1) && random(10) < 1) { // FUCKIN CURSED DAUDE
            return choose("@qw @qh @qy", "@qh @qe @ql @ql @qo", "@qi @qs @qe @qe @qy @qo @qu", "@ql @qe @qt @qm @qe @qo @qu @qt", "@qp @ql @qe @qa @qs @qe @qs @qt @qo @qp");
        } else return Player.wep_text[Player.wep]; // WEAPONS GOT SOME RAD TIPS YO
    } else return "";
}
if (t == 4) {
    if (Player.level > 1) { // MUTATION TIPS
        do {
            i = irandom_range(1, Player.maxskill);
        } until (Player.skill_got[i]);
        return Player.skill_tips[i];
    } else return "";
}
if (t == 5) return Player.crown_tips[Player.crown]; // YOURE A KING WITH A CROWN FUCKO
if (t == 6 && Player.my_health < 4) return choose("oh dear", "this isn't going to end well", "good luck", "no no no", "help"); // LOW HEALTH
if (t == 7 && Player.loops > 0) return choose("this will never end", "it's a whole new world", "monsters everywhere", "what's happening", "there's no limit", "we'll reach for the sky", "no valley too deep", "no mountain too high"); // L O O P S
return "";
