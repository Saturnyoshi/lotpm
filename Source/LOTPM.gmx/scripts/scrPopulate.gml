if(!file_exists(working_directory+"\lung\area"+string(Player.area)+"-"+string(Player.subarea)+".ini"))
{

//setting area and spawning some enemies
with Floor
{
if Player.loops = 0 or Player.area = 100
{
spawnarea = 0
if Player.area = 1 spawnarea = 1
if Player.area = 2 spawnarea = 2
if Player.area = 3 spawnarea = 3
if global.hardmode = 1 then {if Player.area = 3 and Player.subarea = 3 then spawnarea = -1}
if Player.area = 4 spawnarea = 4
if Player.area = 5 spawnarea = 5
if Player.area = 6 spawnarea = 6
if Player.area = 7 spawnarea = 7
if Player.area = 7 and Player.subarea = 3 then spawnarea = -1
if Player.area = 8 spawnarea = 8
if Player.area = 9 spawnarea = 9
if Player.area = 10 spawnarea = 10
if Player.area = 11 spawnarea = 11
if Player.area = 100 spawnarea = 100
if Player.area = 101 spawnarea = 101
if Player.area = 102 spawnarea = 102
if Player.area = 103 and Player.subarea != -1 spawnarea = 103
//-1 is yv's crib
if Player.area = 103 and Player.subarea = -1 spawnarea = -1
if Player.area = 104 spawnarea = 104
if Player.area = 106 spawnarea = 106
if Player.area = 107 spawnarea = -1;
if Player.area = 108 spawnarea = 108
}
else spawnarea = choose(1,2,3,4,5,6,7,8,102,103)

//cool loop thingie from lotpm!
if Player.loops > 0
{
if Player.area = 1 spawnarea = choose(1,1,2,4,102)
if Player.area = 2 spawnarea = choose(1,2,3,6,6,8,)
if Player.area = 3 spawnarea = choose(2,3,3,5,5,102,103)
if Player.area = 3 and Player.subarea = 3 then spawnarea = choose(2,3,5,-1,-1,-1,-1,-1,-1,-1)
if global.hardmode = 1 then {if Player.area = 3 and Player.subarea = 3 then spawnarea = -1}
if Player.area = 4 spawnarea = choose(4,4,6,6,2,1,1,5,5,4)
if Player.area = 5 spawnarea = choose(5,5,5,6,7,9,9,103,-1,-1,-1)
if Player.area = 6 spawnarea = choose(1,6,6,7,2,2,8,9,101,102,3,)
if Player.area = 7 spawnarea = choose(7,7,5,9,8,9,6,3)
if Player.area = 7 and Player.subarea = 3 then spawnarea = -1
if Player.area = 8 spawnarea = choose(8,8,8,7,7,6,5,3)
if Player.area = 9 spawnarea = choose(-1,-1,-1,9,9,7,2,1,5,-1,-1,-1,)
if Player.area = 10 spawnarea = 10 //NO FGUINCK
if Player.area = 11 spawnarea = 11 //NO FGUINCK
if Player.area = 100 spawnarea = 100
if Player.area = 102 spawnarea = 102
if Player.area = 103 spawnarea = 103
}

if random(10+Player.hard) < Player.hard and point_distance(x,y,Player.x,Player.y) > 110 and !place_meeting(x,y,RadChest) and !place_meeting(x,y,AmmoChest) and !place_meeting(x,y,WeaponChest) and ((x+16 != Player.x and y+16 != Player.y) or point_distance(x,y,Player.x,Player.y) > 240)
scrPopEnemies()
}



//COOL BONES

with Floor
{
if spawnarea = 1
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
instance_create(x,y,Bones)
instance_create(x,y+16,Bones)
with instance_create(x+32,y,Bones)
image_xscale = -1
with instance_create(x+32,y+16,Bones)
image_xscale = -1
}
}
if spawnarea = 3
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
if random(7) < 1
instance_create(x,y,Bones)
if random(7) < 1
instance_create(x,y+16,Bones)
if random(7) < 1
{
with instance_create(x+32,y,Bones)
image_xscale = -1}
if random(7) < 1{
with instance_create(x+32,y+16,Bones)
image_xscale = -1}
}
with Bones
sprite_index = sprScrapDecal
}
if spawnarea = 5
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
if random(7) < 1
instance_create(x,y,Bones)
if random(7) < 1
instance_create(x,y+16,Bones)
if random(7) < 1
{
with instance_create(x+32,y,Bones)
image_xscale = -1}
if random(7) < 1{
with instance_create(x+32,y+16,Bones)
image_xscale = -1}
}
with Bones
sprite_index = sprIceDecal
}
if spawnarea = 4
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
if random(9) < 1
instance_create(x,y,Bones)
if random(9) < 1
instance_create(x,y+16,Bones)
if random(9) < 1
{
with instance_create(x+32,y,Bones)
image_xscale = -1}
if random(9) < 1{
with instance_create(x+32,y+16,Bones)
image_xscale = -1}
}
with Bones
sprite_index = sprCaveDecal
}
if spawnarea = 2
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y) and random(10) < 1
{
instance_create(x,y+16,Bones)
with instance_create(x+32,y+16,Bones)
image_xscale = -1
}
with Bones
sprite_index = sprSewerDecal
}

if spawnarea = 8
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
if random(7) < 1
instance_create(x,y,Bones)
if random(7) < 1
instance_create(x,y+16,Bones)
if random(7) < 1
{
with instance_create(x+32,y,Bones)
image_xscale = -1}
if random(7) < 1{
with instance_create(x+32,y+16,Bones)
image_xscale = -1}
}
with Bones
{sprite_index = spr8Decal
image_index = random(3)}
}

if spawnarea = 9
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
if random(7) < 1
instance_create(x,y,Bones)
if random(7) < 1
instance_create(x,y+16,Bones)
if random(7) < 1
{
with instance_create(x+32,y,Bones)
image_xscale = -1}
if random(7) < 1{
with instance_create(x+32,y+16,Bones)
image_xscale = -1}
}
with Bones
sprite_index = spr9Decal
}
//populate inv caves
if spawnarea = 106
{
if !place_free(x-32,y) and !place_free(x+32,y) and place_free(x,y)
{
if random(9) < 1
instance_create(x,y,Bones)
if random(9) < 1
instance_create(x,y+16,Bones)
if random(9) < 1
{
with instance_create(x+32,y,Bones)
image_xscale = -1}
if random(9) < 1{
with instance_create(x+32,y+16,Bones)
image_xscale = -1}
}
with Bones
sprite_index = sprInvCaveDecal
}

}


//making sure there are enough enemies and spawning props/lil walls

with Floor
{
if instance_number(enemy) < (3+Player.hard/1.5) and point_distance(x,y,Player.x,Player.y) >110 and !place_meeting(x,y,RadChest) and !place_meeting(x,y,AmmoChest) and !place_meeting(x,y,WeaponChest) and ((x+16 != Player.x and y+16 != Player.y) or point_distance(x,y,Player.x,Player.y) > 240)
scrPopEnemies()
//CROWN OF BLOOD
if instance_exists(Player){
if (Player.crown = 7 and random(8+Player.hard) < Player.hard and point_distance(x,y,Player.x,Player.y) > 110 and !place_meeting(x,y,RadChest) and !place_meeting(x,y,AmmoChest) and !place_meeting(x,y,WeaponChest) and ((x+16 != Player.x and y+16 != Player.y) or point_distance(x,y,Player.x,Player.y) > 240))
scrPopEnemies()}

scrPopProps()
}

with NOWALLSHEREPLEASE
instance_destroy()

//spawning chests
scrPopChests()

//spawn desert boss
if Player.area = 1 and Player.subarea = 3
instance_create(x,y,WantBoss)
//venuz car
if Player.area = 3 and Player.subarea = 1
{
with instance_furthest(10016,10016,Car){
instance_create(x,y,CarVenus)
instance_change(Wind,false)
}
}

if Player.area < 11 or Player.area >= 100
{
with WeaponChest
instance_create(x,y,Bandit)
with RadChest
instance_create(x,y,Bandit)
with AmmoChest
instance_create(x,y,Bandit)
}
else
{
with WeaponChest
instance_create(x,y,Grunt)
with RadChest
instance_create(x,y,Grunt)
with AmmoChest
instance_create(x,y,Grunt)
}

//populate pizza sewers
if Player.area = 102
{

with enemy
instance_destroy()

with instance_furthest(10016,10016,Corpse)
{
repeat(4)
instance_create(x+random(4)-2,y+random(4)-2,Turtle)
instance_create(x,y,Rat)
}
with Rad
instance_destroy()
with AmmoPickup
instance_destroy()
with HPPickup
instance_destroy()
with Corpse
instance_destroy()
}


//PIZZA SEWER ENTRANCE
{
if Player.area = 2
{
with Floor
{
if sprite_index = sprFloor2 and image_index = 1 or image_index = 5
instance_create(x,y,PizzaEntrance)
}
do {with instance_nearest(10016+random(240)-120,10016+random(240)-120,PizzaEntrance) instance_destroy()}
until instance_number(PizzaEntrance) <= 1
}}


//COOL BONES



}
