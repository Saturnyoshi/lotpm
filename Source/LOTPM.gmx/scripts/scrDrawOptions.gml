
//DRAW OPTIONS

if widescreen > 0
widescreen -= 8

txt0 = "#OPTIONS#(WORK IN PROGRESS)########################PRESS [RIGHT CLICK] TO RETURN"
txt1 = "####AUDIO#MUSIC VOLUME#MASTER VOLUME#AMBIENT VOLUME##VISUALS#FULL SCREEN#SCALING#FIT SCREEN#NICE SHADOWS#NICE DARKNESS##CONTROLS#GAMEPAD#GAMEPAD AUTOAIM##OTHER#SCREEN SHAKE#CAPTURE MOUSE#DELETE DATA"
txt2 = "#####"+string(scrAddZero(round(UberCont.opt_musvol*100),2))+"%#"+string(scrAddZero(round(UberCont.opt_sfxvol*100),2))+"%#"+string(scrAddZero(round(UberCont.opt_ambvol*100),2))
+"%###"+string(scrOnOff(UberCont.opt_fulscrn))+"#X"+string(UberCont.opt_scaling)+"#"+string(scrOnOff(UberCont.opt_fitscrn))+"#"+string(scrOnOff(UberCont.opt_niceshd))+" (NOT WORKING)#"+string(scrOnOff(UberCont.opt_nicedrk))+" (NOT WORKING)"
+"###"+string(scrOnOff(UberCont.opt_gamepad))+"#"+string(scrAddZero(round(UberCont.opt_autoaim*100),2))+"%"
+"###"+string(scrAddZero(round(UberCont.opt_shake*100),2))+"%#"+string(scrOnOff(UberCont.opt_mousecp))+"#(NOT WORKING)"

stxt0 = "#OPTIONS"
stxt1 = "####AUDIO#####VISUALS#######CONTROLS####OTHER####"
stxt2 = txt2


with MusVolSlider
event_perform(ev_draw,0)
with SfxVolSlider
event_perform(ev_draw,0)
with AmbVolSlider
event_perform(ev_draw,0)

with FullScreenToggle
event_perform(ev_draw,0)
with ScaleUpDown
event_perform(ev_draw,0)
with FitScreenToggle
event_perform(ev_draw,0)
with GamePadToggle
event_perform(ev_draw,0)
with AutoAimUpDown
event_perform(ev_draw,0)

with ShakeUpDown
event_perform(ev_draw,0)
with MouseCPToggle
event_perform(ev_draw,0)

draw_set_font(fntM)
draw_set_valign(fa_top)
draw_set_halign(fa_center)

draw_set_color(c_black)
draw_text(view_xview+view_wview/2,view_yview+1,string(txt0))
draw_text(view_xview+view_wview/2+1,view_yview+1,string(txt0))
draw_text(view_xview+view_wview/2+1,view_yview,string(txt0))
draw_set_color(c_gray)
draw_text(view_xview+view_wview/2,view_yview,string(txt0))
draw_set_color(c_white)
draw_text(view_xview+view_wview/2,view_yview,string(stxt0))

draw_set_halign(fa_right)
draw_set_color(c_black)
draw_text(view_xview+view_wview/2-8,view_yview+1,string(txt1))
draw_text(view_xview+view_wview/2-7,view_yview+1,string(txt1))
draw_text(view_xview+view_wview/2-7,view_yview,string(txt1))
draw_set_color(c_gray)
draw_text(view_xview+view_wview/2-8,view_yview,string(txt1))
draw_set_color(c_white)
draw_text(view_xview+view_wview/2-8,view_yview,string(stxt1))

draw_set_halign(fa_left)

draw_set_color(c_black)
draw_text(view_xview+view_wview/2+8,view_yview+1,string(txt2))
draw_text(view_xview+view_wview/2+9,view_yview+1,string(txt2))
draw_text(view_xview+view_wview/2+9,view_yview,string(txt2))
draw_set_color(c_gray)
draw_text(view_xview+view_wview/2+8,view_yview,string(txt2))
draw_set_color(c_white)
draw_text(view_xview+view_wview/2+8,view_yview,string(stxt2))
