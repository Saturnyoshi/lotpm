race_name[0] = "[RANDOM]"
race_pass[0] = "???"
race_acti[0] = "???"
race_butt[0] = ""
race_lock[0] = ""
race_have[0] = 1

race_name[1] = "[FISH]"
race_pass[1] = "GETS MORE AMMO"
race_acti[1] = "DODGE ROLL"
race_butt[1] = "WATER BOOST"
race_lock[1] = "UNLOCKED FROM THE START"
race_have[1] = 1
race_swep[1] = 1

race_name[2] = "[CRYSTAL]"
race_pass[2] = "GETS MORE HP"
race_acti[2] = "CRYSTAL SHIELD"
race_butt[2] = "TELEPORTATION"
race_lock[2] = "UNLOCKED FROM THE START"
race_have[2] = 1
race_swep[2] = 1

race_name[3] = "[EYES]"
race_pass[3] = "SEES IN THE DARK"
race_acti[3] = "TELEKINESIS"
race_butt[3] = "STRONGER TELEKINESIS"
race_lock[3] = "REACH SEWERS TO UNLOCK"
race_have[3] = 1
race_swep[3] = 1

race_name[4] = "[MELTING]"
race_pass[4] = "LESS HP, MORE XP"
race_acti[4] = "EXPLOSIVE REVENGE"
race_butt[4] = "BIGGER CORPSE EXPLOSIONS"
race_lock[4] = "DIE TO UNLOCK"
race_have[4] = 1
race_swep[4] = 1

race_name[5] = "[PLANT]"
race_pass[5] = "IS FASTER"
race_acti[5] = "SNARE GROWTH"
race_butt[5] = "SNARE DEALS DAMAGE PER#SECOND"
race_lock[5] = "KILL 100 ENEMIES TO UNLOCK"
race_have[5] = 1
race_swep[5] = 1

race_name[6] = "[Y.V.]"
race_pass[6] = "HIGHER RATE OF FIRE"
race_acti[6] = "POP POP"
race_butt[6] = "BRRRAP"
race_lock[6] = "REACH ??? TO UNLOCK"
race_have[6] = 1
race_swep[6] = 39

race_name[7] = "[STEROIDS]"
race_pass[7] = "START LOADED, LESS ACCURATE#FULLY AUTOMATIC WEAPONS"
race_acti[7] = "DUAL WIELDING"
race_butt[7] = "FIRING BOTH WEAPONS AT ONCE#HAS A CHANCE NOT TO CONSUME AMMO"
race_lock[7] = "REACH LABS TO UNLOCK"
race_have[7] = 1
race_swep[7] = 1

race_name[8] = "[ROBOT]"
race_pass[8] = "FINDS BETTER TECH"
race_acti[8] = "EATS GUNS"
race_butt[8] = "BETTER GUN NUTRITION"
race_lock[8] = "REACH SCRAPYARD TO UNLOCK"
race_have[8] = 1
race_swep[8] = 1


race_name[9] = "[CHICKEN]"
race_pass[9] = "HARD TO KILL"
race_acti[9] = "SUPER HOT"
race_butt[9] = "NORMAL RATE OF FIRE#DURING SLOW MOTION"
race_lock[9] = "???"
race_have[9] = 1
race_swep[9] = 46


race_name[10] = "[REBEL]"
race_pass[10] = "PORTALS HEAL"
race_acti[10] = "ALLIES"
race_butt[10] = "HIGHER ALLY RATE OF FIRE"
race_lock[10] = "REACH SEWERS TO UNLOCK"
race_have[10] = 1
race_swep[10] = 1

race_name[11] = "[BRUTE]"
race_pass[11] = "HITS HARDER, IS SLOWER"
race_acti[11] = "CHARGE SHOT"
race_butt[11] = "FASTER CHARGE#LEVEL 3 SHOTS DEALS MORE DAMAGE"
race_lock[11] = "REACH 4-1 IN HARD MODE TO UNLOCK"
race_have[11] = 1
race_swep[11] = 1

race_name[12] = "[GRUB]"
race_pass[12] = "MAGGOT ARMY"
race_acti[12] = "EATS CORPSES"
race_butt[12] = "EATING CORPSES#SPAWNS MAGGOTS"
race_lock[12] = "UNLOCKED FROM THE START"
race_have[12] = 1
race_swep[12] = 1

race_name[13] = "[TURRET]"
race_pass[13] = "DEALS LESS DAMAGE#PREDICT ATTACKS"
race_acti[13] = "PLACE TURRETS"
race_butt[13] = "MORE TURRET AMMO"
race_lock[13] = "UNLOCKED FROM THE START"
race_have[13] = 1
race_swep[13] = 1

race_name[14] = "[PLATYPUS]"
race_pass[14] = "OVERALL BETTER STATS"
race_acti[14] = "CHANGE FORMS"
race_butt[14] = "LESS BERZERK RAD DRAIN"
race_lock[14] = "UNLOCKED FROM THE START"
race_have[14] = 1
race_swep[14] = 1

race_name[15] = "[HORROR]"
race_pass[15] = "EXTRA MUTATION CHOICE"
race_acti[15] = "RADIATION BEAM"
race_butt[15] = "BEAM CHARGES FASTER#BEAM HEALS"
race_lock[15] = "UNLOCKED FROM THE START"
race_have[15] = 1
race_swep[15] = 1

race_name[16] = "[ANALYST]"
race_pass[16] = "NO FILTHY MUTATIONS#IDPD TECH"
race_acti[16] = "JETPACK"
race_butt[16] = "???"
race_lock[16] = "UNLOCKED FROM THE START"
race_have[16] = 1
race_swep[16] = 1

racemax = 16
