with Top {
    if place_meeting(x,y,Top) || position_meeting(x,y,Floor)
        instance_destroy()
    else if place_meeting(x,y,Wall) {
        instance_destroy()
        instance_create(x,y,TopSmall)
        instance_create(x+16,y,TopSmall)
        instance_create(x,y+16,TopSmall)
        instance_create(x+16,y+16,TopSmall)
    }
}



with TopDecal {
    if place_meeting(x,y,Bones) || place_meeting(x,y,TopDecal) 
        instance_destroy()
    else if place_meeting(x,y,FloorExplo) {
        instance_destroy();
        if topin = sprTopDecalScrapyard && image_index < 1{
            repeat(4)
            instance_create(x+random(6)-3,y+random(6)-3,Explosion)
        }
        
        if topin = sprTopDecalColony && image_index > 0 && image_index < 2{
            repeat(25)
            instance_create(x,y,ToxicGas)
            
            snd_play(sndToxicBoltGas);
        }
    } else if place_meeting(x,y,Floor)
        instance_destroy()
}
